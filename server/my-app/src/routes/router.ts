import * as _ from 'lodash';
import { Router as ExpressRouter, Request, Response, NextFunction } from 'express';
import expressJwt from 'express-jwt';
import expressGraphql from 'express-graphql';
import { Auth } from './auth/auth';
import badgerSchema from '../badger/graphql/badgerSchema';

export class Router {
  public static getRoutes() { return new Router().routes; }
  private routes = ExpressRouter();

  constructor() {
    this.routes.post('/authenticate', Auth.authenticate);

    this.routes.all('/graphql', expressJwt({ secret: process.env.JWT_SECRET }), this.getGraphql);
  }

  private nocache(req: Request, res: Response, next: NextFunction) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '0');
    res.header('Pragma', 'no-cache');
    next();
  }

  private getGraphql(req: Request, res: Response, next: NextFunction) {
    console.log("belekas");
    const jwt = Auth.getJWT(req);
    const graphql = expressGraphql({
      schema: badgerSchema,
      pretty: true,
      graphiql: false,
    });
    console.log("belekas ret");
    return graphql(req, res);
  }
}
