import * as promise from 'bluebird';
import pgPromise, { IDatabase, IMain } from 'pg-promise';

const pgp: IMain = pgPromise({
  promiseLib: promise,
});

const user = process.env.DB_APP_USER;
const password = process.env.DB_APP_PASSWORD;
const host = process.env.DB_HOST;
const connection = `postgres://${user}:${password}@${host}/badger`;
export const db: IDatabase<any> = pgp(connection);
