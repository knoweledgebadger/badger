import * as Sequelize from 'sequelize';

export const spec_kategorijos = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      primaryKey: true,
        autoIncrement: true,
    },
    kategorijos_pavadinimas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
  aprasas: {
      type: Sequelize.STRING,
      allowNull: false,
    },
};
