import * as Sequelize from 'sequelize';

export const aprasymas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    pavadinimas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sukurimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    redagavimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    aprasas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    vidutinis_ivertinimas: {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0,
    },
    redaguotas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
};
