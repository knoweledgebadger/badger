import * as Sequelize from 'sequelize';

export const privilegijos = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    rasyti_temas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    rasyti_straipsnius: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    redaguoti_straipsnius: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    vertinti_vartotoja: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    trinti_komentarus: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    trinti_straipsnius: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    trinti_temas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    kurti_aprasymus: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    trinti_aprasymus: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    redaguoti_aprasymus: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
};
