import * as Sequelize from 'sequelize';

export const role = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    role: {
        type: Sequelize.STRING(20),
        allowNull: false,
    },

};
