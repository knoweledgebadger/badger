import * as Sequelize from 'sequelize';

// vartotojo lenteles sukurimas
export const vartotojas = {
    vardas: {
        type: Sequelize.STRING(20),
        allowNull: false,
    },
    pseudonimas: {
        type: Sequelize.STRING(30),
        allowNull: false,
        primaryKey: true,
    },
    slaptazodis: {
        type: Sequelize.STRING(32),
        allowNull: false,
    },
    el_pastas: {
        type: Sequelize.STRING(40),
        allowNull: false,
    },
    lytis: {
        type: Sequelize.STRING(10),
        allowNull: false,
    },
    sukurimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    bendras_ivertis: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    avataras: {
        type: Sequelize.STRING(50),
        allowNull: false,
    },
    temu_kiekis: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
    komentaru_kiekis: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
    paskutinis_aktyvumas: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    role_id: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
    },
};
