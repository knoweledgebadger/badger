import * as Sequelize from 'sequelize';

export const istorinis_aprasymas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    pavadinimas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    sukurimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    aprasas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    vidutinis_ivertinimas: {
        type: Sequelize.DOUBLE,
        allowNull: false,
    },
    redaguotas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    }
};
