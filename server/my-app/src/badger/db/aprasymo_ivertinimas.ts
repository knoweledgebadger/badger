import * as Sequelize from 'sequelize';

export const aprasymo_ivertinimas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    vartotojas: {
      type: Sequelize.STRING,
        allowNull: false,
    },
  zvaigzduciu_kiekis: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
  komentaras: {
        type: Sequelize.STRING,
        allowNull: true,
    },
  data: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.fn('now'),
    },
};
