import * as Sequelize from 'sequelize';

export const irenginio_kategorija = {
  id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
  pavadinimas: {
      type: Sequelize.STRING,
      allowNull: false,
    },
};
