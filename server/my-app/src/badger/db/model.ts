import { Sequelize } from 'sequelize';
import logger from '../../logger';
import { vartotojas } from './Jurgis/vartotojas';
import { ivertinimas } from './Jurgis/ivertinimas';
import { privilegijos } from './Jurgis/privilegijos';
import { role } from './Jurgis/role';
import { user } from './user';

// Aurimo modeliai
import { laisvasamdybos_profilis } from './Aurimas/laisvasamdybos_profilis';
import { skelbimas } from './Aurimas/skelbimas';
import { zinute } from './Aurimas/zinute';
import { blokuoti_vartotojai } from './Aurimas/blokuoti_vartotojai';
import { ignoruoti_vartotojai } from './Aurimas/ignoruoti_vartotojai';
import { zymes } from './Aurimas/zymes';
// Luko modeliai
import { aprasymas } from './aprasymas';
import { aprasymo_ivertinimas } from './aprasymo_ivertinimas';
import { istorinis_aprasymas } from './istorinis_aprasymas';
import { nuotrauka } from './nuotrauka';
import { paaiskinimas } from './paaiskinimas';
import { spec_kategorijos } from './spec_kategorijos';
import { irenginio_kategorija } from './irenginio_kategorija';

// Lauryno modeliai
import { tema } from './Laurynas/tema';
import { temosAdminTaisykle } from './Laurynas/temosAdminTaisykle';
import { diskusija } from './Laurynas/diskusija';
import { diskusijosIstorinisIrasas } from './Laurynas/diskusijosIstorinisIrasas';
import { komentaras } from './Laurynas/komentaras';
import { diskusijosIvertinimas } from './Laurynas/diskusijosIvertinimas';
import { komentaroIstorinisIrasas } from './Laurynas/komentaroIstorinisIrasas';
import { komentaroIvertinimas } from './Laurynas/komentaroIvertinimas';
import { paieskosIrasas } from './Laurynas/paieskosIrasas';

const defineBadgerModel = (conn: Sequelize, badgerSchema: string, sync?: boolean) => {

  const userModel = conn.define('user', user, { schema: badgerSchema });

  const options = {
    schema: badgerSchema, timestamps: false,
    underscored: true,
    freezeTableName: true,
  };

  const optionsCamel = {
    schema: badgerSchema, timestamps: false,
    freezeTableName: true,
  };

  // Aurimo
  const laisvasamdybosProfilisModel =
    conn.define('laisvasamdybos_profilis', laisvasamdybos_profilis, {
      ...options, name: {
        singular: 'laisvasamdybos_profilis',
        plural: 'laisvasamdybos_profilis',
      },
    });
  const skelbimasModel = conn.define('skelbimas', skelbimas, {
    ...options, name: {
      singular: 'skelbimas',
      plural: 'skelbimas',
    },
  });
  const zinuteModel = conn.define('zinute', zinute, {
    ...options, name: {
      singular: 'zinute',
      plural: 'zinute',
    },
  });
  const blokuotiVartotojaiModel = conn.define('blokuoti_vartotojai', blokuoti_vartotojai, {
    ...options, name: {
      singular: 'blokuoti_vartotojai',
      plural: 'blokuoti_vartotojai',
    },
  });
  const ignoruotiVartotojaiModel = conn.define('ignoruoti_vartotojai', ignoruoti_vartotojai, {
    ...options, name: {
      singular: 'ignoruoti_vartotojai',
      plural: 'ignoruoti_vartotojai',
    },
  });
  const zymesModel = conn.define('zymes', zymes, {
    ...options, name: {
      singular: 'zymes',
      plural: 'zymes',
    },
  });

  //#region JK_DB
  // [JK] Iterpiamos jau sukurtos duomenu bazes lenteles
  const vartotojasModel = conn.define('vartotojas', vartotojas, {
    ...options, name: {
      singular: 'vartotojas',
      plural: 'vartotojai',
    },
  });
  const ivertinimasModel = conn.define('ivertinimas', ivertinimas, {
    ...options, name: {
      singular: 'ivertinimas',
      plural: 'ivertinimai',
    },
  });
  const privilegijosModel = conn.define('privilegijos', privilegijos, {
    ...options, name: {
      singular: 'privilegijos',
      plural: 'privilegijos',
    },
  });
  const roleModel = conn.define('role', role, {
    ...options, name: {
      singular: 'role',
      plural: 'roles',
    },
  });

  // [JK] nustatomi rysiai tarp lenteli
  vartotojasModel.belongsTo(ivertinimasModel);
  ivertinimasModel.hasMany(vartotojasModel, { onDelete: 'cascade' });

  vartotojasModel.belongsTo(privilegijosModel);
  privilegijosModel.hasMany(vartotojasModel, { onDelete: 'cascade' });

  vartotojasModel.belongsTo(roleModel, { foreignKey: "role_id" });
  roleModel.hasOne(vartotojasModel, { onDelete: 'cascade' });

  userModel.belongsTo(vartotojasModel, { foreignKey: "fk_vartotojas" });
  vartotojasModel.hasOne(userModel, { onDelete: 'cascade', foreignKey: "fk_vartotojas" });
  //#endregion JK_DB

  // pavyzdinis model : tema + temos admin taisykle
  const temaModel = conn.define(
    'tema', tema,
    { ...optionsCamel, name: { singular: 'tema', plural: 'tema' } });
  const temosAdminTaisykleModel = conn.define(
    'temosAdminTaisykle',
    temosAdminTaisykle,
    { ...optionsCamel, name: { singular: 'temosAdminTaisykle', plural: 'temosAdminTaisykle' } },
  );
  const diskusijaModel = conn.define(
    'diskusija', diskusija,
    { ...optionsCamel, name: { singular: 'diskusija', plural: 'diskusija' } });
  const diskusijosIstorinisIrasasModel = conn.define(
    'diskusijosIstorinisIrasas',
    diskusijosIstorinisIrasas,
    {
      ...optionsCamel,
      name: { singular: 'diskusijosIstorinisIrasas', plural: 'diskusijosIstorinisIrasas' },
    },
  );
  const komentarasModel = conn.define(
    'komentaras', komentaras,
    { ...optionsCamel, name: { singular: 'komentaras', plural: 'komentaras' } });
  const diskusijosIvertinimasModel = conn.define(
    'diskusijosIvertinimas',
    diskusijosIvertinimas,
    {
      ...optionsCamel,
      name: { singular: 'diskusijosIvertinimas', plural: 'diskusijosIvertinimas' },
    },
  );
  const komentaroIstorinisIrasasModel = conn.define(
    'komentaroIstorinisIrasas', komentaroIstorinisIrasas,
    {
      ...optionsCamel,
      name: { singular: 'komentaroIstorinisIrasas', plural: 'komentaroIstorinisIrasas' },
    });
  const komentaroIvertinimasModel = conn.define(
    'komentaroIvertinimas',
    komentaroIvertinimas,
    {
      ...optionsCamel,
      name: { singular: 'komentaroIvertinimas', plural: 'komentaroIvertinimas' },
    },
  );
  const paieskosIrasasModel = conn.define(
    'paieskosIrasas', paieskosIrasas,
    { ...optionsCamel, name: { singular: 'paieskosIrasas', plural: 'paieskosIrasas' } });

  const aprasymaiModel = conn.define('aprasymas', aprasymas, {
    ...options, name: {
      singular: 'aprasymas',
      plural: 'aprasymai',
    },
  });
  // tslint:disable-next-line
  const aprasymo_ivertinimasModel =
    conn.define('aprasymo_ivertinimas', aprasymo_ivertinimas, options);
  // tslint:disable-next-line
  const istorinis_aprasymasModel = conn.define('istorinis_aprasymas', istorinis_aprasymas, options);
  const nuotraukaModel = conn.define('nuotrauka', nuotrauka, options);
  const paaiskinimasModel = conn.define('paaiskinimas', paaiskinimas, options);
  // tslint:disable-next-line
  const spec_kategorijosModel = conn.define('spec_kategorijos', spec_kategorijos, {
    ...options, name: {
      singular: 'spec_kategorijos',
      plural: 'spec_kategorijos',
    },
  });
  const kategorijuModel = conn.define('irenginio_kategorija', irenginio_kategorija, options);

  spec_kategorijosModel.belongsTo(
    aprasymaiModel,
  );
  aprasymaiModel.hasMany(spec_kategorijosModel, { onDelete: 'cascade' });

  paaiskinimasModel.belongsTo(
    spec_kategorijosModel,
  );
  spec_kategorijosModel.hasMany(paaiskinimasModel, { onDelete: 'cascade' });

  istorinis_aprasymasModel.belongsTo(
    aprasymaiModel,
  );
  aprasymaiModel.hasMany(istorinis_aprasymasModel, { onDelete: 'cascade' });

  nuotraukaModel.belongsTo(
    aprasymaiModel,
  );
  aprasymaiModel.hasMany(nuotraukaModel, { onDelete: 'cascade' });

  aprasymo_ivertinimasModel.belongsTo(
    aprasymaiModel,
  );
  aprasymaiModel.hasMany(aprasymo_ivertinimasModel, { onDelete: 'cascade' });

  aprasymaiModel.belongsTo(
    kategorijuModel,
  );
  kategorijuModel.hasMany(aprasymaiModel, { onDelete: 'cascade' });

  // Lauryno relations

  // tema - taisykle
  temaModel.hasMany(temosAdminTaisykleModel);
  temosAdminTaisykleModel.belongsTo(temaModel, { onDelete: 'Cascade' });

  // tema - diskusija
  temaModel.hasMany(diskusijaModel);
  diskusijaModel.belongsTo(temaModel, { onDelete: 'Cascade' });

  // diskusija - diskusijos ist irasas
  diskusijaModel.hasMany(diskusijosIstorinisIrasasModel);
  diskusijosIstorinisIrasasModel.belongsTo(diskusijaModel, { onDelete: 'Cascade' });
  userModel.hasMany(diskusijaModel);
  diskusijaModel.belongsTo(userModel, { onDelete: 'Cascade' });
  // diskusija - diskusijos ist irasas
  diskusijaModel.hasMany(diskusijosIvertinimasModel);
  diskusijosIvertinimasModel.belongsTo(diskusijaModel, { onDelete: 'Cascade' });
  // diskusija - komentaras 
  diskusijaModel.hasMany(komentarasModel);
  komentarasModel.belongsTo(diskusijaModel, { onDelete: 'Cascade' });

  // komentaras - komentaroIvertinimas
  komentarasModel.hasMany(komentaroIvertinimasModel);
  komentaroIvertinimasModel.belongsTo(komentarasModel, { onDelete: 'Cascade' });

  // komentaras - komentaro ist irasas  
  komentarasModel.hasMany(komentaroIstorinisIrasasModel);
  komentaroIstorinisIrasasModel.belongsTo(komentarasModel, { onDelete: 'Cascade' });

  // paieskosIrasas - vartotojas 
  userModel.hasMany(paieskosIrasasModel);
  paieskosIrasasModel.belongsTo(userModel, { onDelete: 'Cascade' });

  // Aurimo relations

  // vartotojas - laisvasamdybos profilis
  vartotojasModel.hasOne(laisvasamdybosProfilisModel, { foreignKey: 'vartotojo_id' });
  laisvasamdybosProfilisModel.belongsTo(
    vartotojasModel,
    { foreignKey: 'vartotojo_id', onDelete: 'Cascade' });

  // vartotojas - skelbimas
  vartotojasModel.hasMany(skelbimasModel);
  skelbimasModel.belongsTo(vartotojasModel, { foreignKey: 'autorius', onDelete: 'Cascade' });

  // vartotojas - zinute(gavejas)
  vartotojasModel.hasMany(zinuteModel);
  zinuteModel.belongsTo(vartotojasModel, { foreignKey: 'gavejas', onDelete: 'Cascade' });

  // vartotojas - zinute(siuntejas)
  vartotojasModel.hasMany(zinuteModel);
  zinuteModel.belongsTo(vartotojasModel, { foreignKey: 'siuntejas', onDelete: 'Cascade' });

  // vartotojas - blokuoti_vartotojai
  vartotojasModel.hasOne(blokuotiVartotojaiModel, { foreignKey: 'blokuojamo_vartotojo_id' });
  blokuotiVartotojaiModel.belongsTo(
    vartotojasModel, { foreignKey: 'blokuojamo_vartotojo_id', onDelete: 'Cascade' });

  // vartotojas - ignoruoti_vartotojai(ignoruotojo_id)
  vartotojasModel.hasOne(ignoruotiVartotojaiModel, { foreignKey: 'ignoruotojo_id' });
  ignoruotiVartotojaiModel.belongsTo(
    vartotojasModel, { foreignKey: 'ignoruotojo_id', onDelete: 'Cascade' });

  // vartotojas - ignoruoti_vartotojai(ignoruojamo_vartotojo_id)
  vartotojasModel.hasOne(ignoruotiVartotojaiModel, { foreignKey: 'ignoruojamo_vartotojo_id' });
  ignoruotiVartotojaiModel.belongsTo(
    vartotojasModel, { foreignKey: 'ignoruojamo_vartotojo_id', onDelete: 'Cascade' });

  if (sync) return conn.sync({ force: false, alter: false })
    .then(() => logger('DB schema %s synced with sequelize' + badgerSchema));
};

export { defineBadgerModel };
