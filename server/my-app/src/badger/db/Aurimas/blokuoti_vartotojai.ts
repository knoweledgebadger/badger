import * as Sequelize from 'sequelize';

export const blokuoti_vartotojai = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    priezastis: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ar_pasibaigs: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
    pradzios_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    pabaigos_data: {
        type: Sequelize.DATE,
        allowNull: true,
    },
};