import * as Sequelize from 'sequelize';

export const zymes = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    pavadinimas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    aprasymas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
};