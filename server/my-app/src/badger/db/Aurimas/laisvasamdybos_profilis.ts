import * as Sequelize from 'sequelize';

export const laisvasamdybos_profilis = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    sukurimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    redagavimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    portfolio_nuoroda: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    cv_nuoroda: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ar_patvirtintas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
    },
};