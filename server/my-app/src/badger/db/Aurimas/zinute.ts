import * as Sequelize from 'sequelize';

export const zinute = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    tekstas: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ar_viesa: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    },
};