import * as Sequelize from 'sequelize';

export const skelbimas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    sukurimo_data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
    pabaigos_data: {
        type: Sequelize.DATE,
        allowNull: false,
    },
    tekstas: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    ar_patvirtintas: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    },
    aprasymas: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
};