import * as Sequelize from 'sequelize';

export const paaiskinimas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    zodis: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    tekstas: {
        type: Sequelize.STRING,
        allowNull: false,
    }
};
