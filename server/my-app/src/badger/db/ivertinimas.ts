import * as Sequelize from 'sequelize';

export const ivertinimas = {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    pseudonimas: {
        type: Sequelize.STRING(30),
        allowNull: false,
    },
    ivertis: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0,
    },
    data: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.fn('now'),
    },
};
