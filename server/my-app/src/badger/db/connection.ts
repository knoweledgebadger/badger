import sequelize from 'sequelize';
import { defineBadgerModel } from './model';
import logger from '../../logger';

const dbHost = process.env.DB_HOST;
const dbInstance = process.env.DB_INSTANCE;
const ownerUser = process.env.DB_OWNER_USER;
const ownerPassword = process.env.DB_OWNER_PASSWORD;
const appUser = process.env.DB_APP_USER;
const appPassword = process.env.DB_APP_PASSWORD;
const owenerConnection = `postgres://${ownerUser}:${ownerPassword}@${dbHost}/${dbInstance}`;
const appConnection = `postgres://${appUser}:${appPassword}@${dbHost}/${dbInstance}`;
const badgerSchema = 'badger';
const force = process.env.FORCE_DB_SCHEMA_CREATION === 'true';

const options: sequelize.Options = {
  logging: (sql: {}) => logger('', sql),
};
export const ownerConn = new sequelize(owenerConnection, options);
const conn = new sequelize(appConnection, options);

ownerConn.authenticate()
  .then(() => createSchema(ownerConn, badgerSchema, force))
  .then(() => defineBadgerModel(ownerConn, badgerSchema, true))
  .then(() => createUser(ownerConn, appUser))
  .then(() => grantPrivileges(ownerConn, appUser, badgerSchema))
  .then(() => logger('DB synced for owner'))
  .then(() => defineBadgerModel(conn, badgerSchema))
  .then(() => logger('DB synced for app'))
  .catch((err: Error) => logger('Failed syncing DB!', err));

const grantPrivileges = (conn: sequelize.Sequelize, user: string, schema: string) => {
  return conn.query(`REVOKE ALL ON ALL TABLES IN SCHEMA ${schema} FROM PUBLIC`)
    .then(() => conn.query(
      `GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA ${schema} TO ${user}`))
    .then(() => conn.query(`GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA ${schema} TO ${user}`))
    .then(() => conn.query(`GRANT USAGE ON SCHEMA ${schema} TO ${user}`));
};

const createUser = (conn: sequelize.Sequelize, user: string) => {
  return conn.query(`SELECT 1 FROM pg_roles WHERE rolname='${appUser}'`).then((result) => {
    const userExists = result[0][0] ? true : false;
    if (!userExists) {
      return conn.query(`create user ${appUser} with login password '${appPassword}'`);
    }
    return;
  });
};

const createSchema = (conn: sequelize.Sequelize, schema: string, force?: boolean) => {
  if (force) return conn.dropSchema(schema, {}).then(() => conn.createSchema(schema, {}));

  const sql = `SELECT schema_name FROM information_schema.schemata WHERE schema_name='${schema}'`;
  return conn.query(sql).then((result) => {
    const schemaExists = result[0][0] ? true : false;
    if (!schemaExists) {
      return conn.createSchema(schema, {});
    }
    return;
  });
};
export default conn;
