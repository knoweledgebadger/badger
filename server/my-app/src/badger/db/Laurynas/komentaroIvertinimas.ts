import * as Sequelize from 'sequelize';

export const komentaroIvertinimas = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  arTeigiamas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  ivertinimoData: {
    type: Sequelize.STRING,
    allowNull: false,
  },
};
