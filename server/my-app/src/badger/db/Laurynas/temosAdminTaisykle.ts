import * as Sequelize from 'sequelize';

export const temosAdminTaisykle = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  pavadinimas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  aprasymas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  sukurimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
};
