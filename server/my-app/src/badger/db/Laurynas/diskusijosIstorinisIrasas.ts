import * as Sequelize from 'sequelize';

export const diskusijosIstorinisIrasas = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  aprasymas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  tekstas: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  sukurimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
};
