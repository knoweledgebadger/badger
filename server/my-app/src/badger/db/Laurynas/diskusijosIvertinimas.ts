import * as Sequelize from 'sequelize';

export const diskusijosIvertinimas = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  tekstas: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  aprasymas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  keitimoPriezastis: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  keitimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
};
