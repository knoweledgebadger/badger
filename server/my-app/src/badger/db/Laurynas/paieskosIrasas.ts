import * as Sequelize from 'sequelize';

export const paieskosIrasas = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  tekstas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  sukurimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
};
