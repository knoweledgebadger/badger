import * as Sequelize from 'sequelize';

export const diskusija = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  tekstas: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  sukurimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
  keitimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
  aprasymas: {
    type: Sequelize.STRING,
    allowNull: false,
  },
};
