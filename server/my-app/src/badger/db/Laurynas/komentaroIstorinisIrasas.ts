import * as Sequelize from 'sequelize';

export const komentaroIstorinisIrasas = {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  tekstas: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  keitimoData: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: Sequelize.fn('now'),
  },
};
