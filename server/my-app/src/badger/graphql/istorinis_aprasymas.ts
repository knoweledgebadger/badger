import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

export const istoriniaiAprasymaiType = `
    input IstorinioAprasymoInput {
        id: Int
        pavadinimas: String
        sukurimo_data: Date
        redagavimo_data: Date
        aprasas: String
    }

    type IstorinisAprasymas {
        id: Int
        pavadinimas: String
        sukurimo_data: Date
        redagavimo_data: Date
        aprasas: String
        vidutinis_ivertinimas: String
        redaguotas: Boolean
    }
`;

export const istroiniaiAprasymaiQuery = `
  aprasymai: [IstorinisAprasymas]
`;

export const istoriniaiAprasymaiMutation = `
    kurtiIstoriniAprasyma(aprasymas: IstorinioAprasymoInput!): IstorinisAprasymas
    istrintiIstoriniAprasyma(id: Int!): IstorinisAprasymas
`;

export const aprasymaiResolvers = {
  Query: {
    aprasymai: (obj: {}, args: {}, context: Request) => {
      return connection.models.istorinis_aprasymas.findAll();
    },
  },
  Mutation: {
    kurtiIstoriniAprasyma(value: {}, { istorinis_aprasymas }: any, context: Request) {
      return connection.models.istorinis_aprasymas.create({
        pavadinimas: istorinis_aprasymas.pavadinimas,
        sukurimo_data: istorinis_aprasymas.sukurimo_data,
        redagavimo_data: istorinis_aprasymas.redagavimo_data,
        aprasas: istorinis_aprasymas.aprasas,
        vidutinis_ivertinimas: istorinis_aprasymas.vidutinis_ivertinimas,
        redaguotas: istorinis_aprasymas.redaguotas,
      });
    },
    istrintiIstoriniAprasyma(value: any, { id }: any) {
      return connection.models.istorinis_aprasymas.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
  },
};
