import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const roleType = `
    type Role {
      id: Int
      role: String
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Tema" masyvas
export const roleQuery = `
  role : [Role]
`;


// instrukcijos ką daryti kiekvieno query/mutation metu
export const roleResolvers = {
  Query: {
    role: (obj: {}, { }: any, context: Request) => {
      return connection.models.role.findAll();
    },
  },
};
