import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

export const kategorijaType = `
    type Kategorija {
        id: Int
        pavadinimas: String
    }
`;

export const kategorijosQuery = `
  kategorijos: [Kategorija]
`;

export const kategorijosResolvers = {
  Query: {
    kategorijos: (obj: {}, args: {}, context: Request) => {
      return connection.models.irenginio_kategorija.findAll();
    },
  },
};
