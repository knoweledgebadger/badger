import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';
import * as Sequelize from 'sequelize';
// import * as moment from 'moment';
// tslint:disable-next-line
//import { Moment } from 'moment';

export const aprasymaiType = `
    input AprasymoInput {
        id: Int
        pavadinimas: String
        sukurimo_data: Date
        redagavimo_data: Date
        aprasas: String
        vidutinis_ivertinimas: Float
        redaguotas: Boolean
        spec_kategorijos: [SpecKategorijosInput]
        irenginio_kategorija_id: Int
        ivertinimas: [AprasymoIvertinimasInput]
    }

    input SpecKategorijosInput {
      id: Int
      kategorijos_pavadinimas: String
      aprasas: String
      paaiskinimas: [PaaiskinimasInput]
    }

    type Aprasymas {
        id: Int
        pavadinimas: String
        sukurimo_data: Date
        redagavimo_data: Date
        aprasas: String
        vidutinis_ivertinimas: Float
        redaguotas: Boolean
        spec_kategorijos: [SpecKategorijos]
        irenginio_kategorija_id: Int
        ivertinimas: [AprasymoIvertinimas]
    }

    

    type SpecKategorijos {
      id: Int
      kategorijos_pavadinimas: String
      aprasas: String
      paaiskinimas: [Paaiskinimas]
    }

    type Paaiskinimas
    {
      id: Int
      zodis: String
      tekstas: String
    }
    
    input PaaiskinimasInput
    {
      id: Int
      zodis: String
      tekstas: String
    }
    type AprasymoIvertinimas {
      id: Int
      vartotojas: String
      zvaigzduciu_kiekis: Int
      komentaras: String
      data: Date
    }
    input AprasymoIvertinimasInput {
      id: Int
      vartotojas: String
      zvaigzduciu_kiekis: Int
      komentaras: String
      data: Date
    }
    type AprasymasResult {
      success: Boolean
      data: Aprasymas
    }
`;

export const aprasymaiQuery = `
  aprasymai: [Aprasymas]
  aprasymas (id: Int!): Aprasymas
`;

export const aprasymaiMutation = `
    kurtiAprasyma(aprasymas: AprasymoInput!): Aprasymas
    redaguotiAprasyma(aprasymas: AprasymoInput!): Aprasymas
    istrintiAprasyma(id: Int!): Aprasymas
    vertintiAprasyma(aprasymo_ivertinimas: AprasymoIvertinimasInput!, aprasymo_id: Int!):
     AprasymoIvertinimas
`;

export const aprasymaiResolvers = {
  Query: {
    aprasymai: (obj: {}, args: {}, context: Request) => {
      return connection.models.aprasymas.findAll({
        include: [
          {
            model: connection.models.spec_kategorijos,
            include: [connection.models.paaiskinimas],
          },
        ],
        order: [['pavadinimas', 'ASC']],
      });
    },
    aprasymas: (obj: {}, { id }: any, context: Request) => {
      return connection.models.aprasymas.find({
        include: [
          {
            model: connection.models.aprasymo_ivertinimas,
          },
          connection.models.user,
        ],
        order: [['pavadinimas', 'ASC']],
      }).then((result) => {
        return result;
      });
    },
  },
  Mutation: {
    kurtiAprasyma(value: {}, { aprasymas }: any, context: Request) {
      return connection.models.aprasymas.create({
        pavadinimas: aprasymas.pavadinimas,
        aprasas: aprasymas.aprasas,
        irenginio_kategorija_id: aprasymas.irenginio_kategorija_id,
      }).then((created: any) => {
        if (aprasymas.spec_kategorijos) {
          aprasymas.spec_kategorijos.forEach((skategorija: any) => {
            connection.models.spec_kategorijos.create({
              kategorijos_pavadinimas: skategorija.kategorijos_pavadinimas,
              aprasas: skategorija.aprasas,
              aprasymas_id: created.id,
            }).then((created2: any) => {
              // console.log(skategorija);
              if (skategorija.paaiskinimas)
                skategorija.paaiskinimas.forEach((paaisk: any) => {
                  connection.models.paaiskinimas.create({
                    zodis: paaisk.zodis,
                    tekstas: paaisk.tekstas,
                    spec_kategorijos_id: created2.id,
                  });
                });
            });
          });
        }
        return created;
      });
    },

    /*
    // pvz sudetingiausias mutation:
    updateTema(value: any, { tema }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.tema.find({ where: { id: tema.id } }).then((item: any) => {
        // paupdatina rastos temos atributus
        return item.updateAttributes(tema).then((updated: any) => {
          // sunaikina senas taisykles
          connection.models.temosAdminTaisykle.destroy({
            where: { temaId: updated.id },
          });
          // jei yra nauju administravimo taisykliu
          if (tema.temosAdminTaisykle)
            // su kiekvienas (foreach daro kazka)
            tema.temosAdminTaisykle.forEach((taisykle: any) => {
              // sukuria taisykel naudojant jos duomenis ir paupdatintos temos id kaip fkey
              connection.models.temosAdminTaisykle.create({
                pavadinimas: taisykle.pavadinimas,
                aprasymas: taisykle.aprasymas,
                temaId: updated.id,
              });
            });
          // returnina paupdatintą  temą
          return updated;
        });
      });
    },
    deleteTema(value: any, { id }: any) {
      return connection.models.tema.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
    */

    /* editKomentaras(value: any, { komentaras }: any) {
       // suranda temą duombazeje pagal jos id
       return connection.models.komentaras.find({
         where: {
           id: komentaras.id,
         },
       }).then((item: any) => {
         connection.models.komentaroIstorinisIrasas.create({
           tekstas: item.tekstas,
           komentarasId: item.id,
         });
         // paupdatina rastos diskusijos atributus
         return item.updateAttributes({
           ...komentaras,
           keitimoData: Date.now(),
         }).then((updated: any) => updated);
       });
     },
     */
    redaguotiAprasyma(value: any, { aprasymas }: any) {
      return connection.models.aprasymas.find(
        {
          where: { id: aprasymas.id },
        }).
        then((item: any) => {
          connection.models.istorinis_aprasymas.create({
            pavadinimas: item.pavadinimas,
            aprasas: item.aprasas,
            sukurimo_data: item.sukurimo_data,
            vidutinis_ivertinimas: item.vidutinis_ivertinimas,
            redaguotas: item.redaguotas,
            irenginio_kategorija_id: item.irenginio_kategorija_id,
            aprasymas_id: item.id,
          });
          return item.updateAttributes({
            ...aprasymas,
            redaguotas: true,
            redagavimo_data: Date.now(),
          }).
            then((updated: any) => {
              // sunaikina senas taisykles
              connection.models.spec_kategorijos.destroy({
                where: { aprasymas_id: updated.id },
              });
              // jei yra nauju administravimo taisykliu
              if (aprasymas.spec_kategorijos) {
                // su kiekvienas (foreach daro kazka)
                aprasymas.spec_kategorijos.forEach((skategorija: any) => {
                  // sukuria taisykel naudojant jos duomenis ir paupdatintos temos id kaip fkey
                  connection.models.spec_kategorijos.create({
                    kategorijos_pavadinimas: skategorija.kategorijos_pavadinimas,
                    aprasas: skategorija.aprasas,
                    aprasymas_id: updated.id,
                  }).then((updated2: any) => {
                    // sunaikina senas taisykles
                    connection.models.paaiskinimas.destroy({
                      where: { spec_kategorijos_id: updated2.id },
                    });
                    // jei yra nauju administravimo taisykliu
                    if (skategorija.paaiskinimas)
                      // su kiekvienas (foreach daro kazka)
                      skategorija.paaiskinimas.forEach((paaisk: any) => {
                        // sukuria taisykel naudojant jos duomenis ir
                        // paupdatintos temos id kaip fkey
                        connection.models.paaiskinimas.create({
                          zodis: paaisk.zodis,
                          tekstas: paaisk.tekstas,
                          spec_kategorijos_id: updated2.id,
                        });
                      });
                  });
                });
              }
              // returnina paupdatintą  temą
              return updated;
            });
        });
    },
    istrintiAprasyma(value: any, { id }: any) {
      return connection.models.aprasymas.destroy({ where: { id } }).then((deleted: any) => deleted);
    },

    /*createKomentaras(value: {}, { komentaras, diskusijosId }: any, context: Request) {
      return connection.models.komentaras.create({
        diskusijaId: diskusijosId,
        tekstas: komentaras.tekstas,
      }).then((created: any) => created);
    },*/

    vertintiAprasyma(value: {}, { aprasymo_ivertinimas, aprasymo_id }: any, context: Request) {
      return connection.models.aprasymo_ivertinimas.create({
        id: aprasymo_id,
        zvaigzduciu_kiekis: aprasymo_ivertinimas.zvaigzduciu_kiekis,
        komentaras: aprasymo_ivertinimas.komentaras,
        data: Date.now(),
        aprasymas_id: aprasymo_ivertinimas.aprasymas_id,
        // ata
      }).then((created: any) => created);
    },

    /*vertintiAprasyma(value: {}, { aprasymoID, stars, comment }: any, context: Request) {
      const jwt = Auth.getJWT(context);
      let condition = { id: aprasymoID };
      const permissions = _.get<string[]>(jwt, 'permissions');
      const requireOldPassword = permissions != null && (permissions.indexOf('ADMIN') < 0);
      if (requireOldPassword) condition = _.assign(condition, { password: oldPassword });
      const options = { where: condition, returning: true };
 
      return connection.models.user.update({ password: newPassword }, options)
        .then((result) => {
          return { success: result[0] === 1, data: _.get(result[1][0], 'dataValues') };
        });
    },*/
  },
};
