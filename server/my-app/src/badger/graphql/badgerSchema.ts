import graphqlDate from 'graphql-date';
import { makeExecutableSchema } from 'graphql-tools';
import { merge } from 'lodash';
import * as log4js from 'log4js';
import { userType, userResolvers, userQuery, userMutation } from './user';

import { roleType, roleResolvers, roleQuery } from './role';
import { privilegijosType, privilegijosResolvers, privilegijosQuery, privilegijosMutation } from './privilegijos';

import { temaType, temaResolvers, temaQuery, temaMutation } from './Laurynas/tema';
import {
  diskusijaType,
  diskusijaResolvers,
  diskusijaQuery,
  diskusijaMutation,
} from './Laurynas/diskusija';

import {
  komentarasType,
  komentarasResolvers,
  komentarasQuery,
  komentarasMutation,
} from './Laurynas/komentaras';

import {
  vartotojasType,
  vartotojasResolvers, vartotojasQuery,
  vartotojasMutation,
} from './vartotojas';
import { aprasymaiType, aprasymaiResolvers, aprasymaiQuery, aprasymaiMutation } from './aprasymas';
import { kategorijaType, kategorijosResolvers, kategorijosQuery } from './irenginio_kategorija';
const root = `
    scalar Date
    type Query {
        ${userQuery}
        ${temaQuery}
        ${vartotojasQuery}
        ${diskusijaQuery}
        ${komentarasQuery}        
        ${aprasymaiQuery}
        ${roleQuery}
        ${privilegijosQuery}

        ${kategorijosQuery}
        
    }
    type Mutation {
        ${userMutation}
        ${diskusijaMutation}
        ${komentarasMutation}
        ${temaMutation}
        ${aprasymaiMutation}
        ${vartotojasMutation}
        ${privilegijosMutation}
    }
    schema {
        query: Query
        mutation: Mutation
    }
`;

const typeDefs = [
  userType,
  temaType,
  vartotojasType,
  aprasymaiType,
  kategorijaType,
  root,
  userType,
  temaType,
  diskusijaType,
  roleType,
  privilegijosType,
  komentarasType,
];

const rootResolvers = {};

const resolvers: {} = merge(
  { Date: graphqlDate },
  rootResolvers,
  userResolvers,
  temaResolvers,
  aprasymaiResolvers,
  diskusijaResolvers,
  komentarasResolvers,
  vartotojasResolvers,
  roleResolvers,
  privilegijosResolvers,
  kategorijosResolvers,
);

const logger = { log: (e: Error) => log4js.getLogger('api').error(e.stack) };

export default makeExecutableSchema({
  typeDefs,
  resolvers,
  logger,
});
