import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../../badger/db/connection';
import { Auth } from '../../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const temaType = `
    input TemaInput {
        id: Int
        pavadinimas: String
        aprasymas: String
        temosAdminTaisykle: [TemosAdminTaisykleInput]
    }
    input TemosAdminTaisykleInput {
      id: Int
      pavadinimas: String
      aprasymas: String
    }

    type Tema {
        id: Int
        pavadinimas: String
        aprasymas: String
        temosAdminTaisykle: [TemosAdminTaisykle]
    }

    type TemosAdminTaisykle {
      id: Int
      pavadinimas: String
      aprasymas: String
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Tema" masyvas
export const temaQuery = `
  temos: [Tema]
  tema (id: Int!): Tema
`;

// mutationų pavadinimai ir ką jie grąžina 
// pvz createTema sukuria tema iš temaInput ir grąžina Tema
export const temaMutation = `
    createTema(tema: TemaInput!): Tema
    updateTema(tema: TemaInput!): Tema
    deleteTema(id: Int!): Tema
`;
// instrukcijos ką daryti kiekvieno query/mutation metu
export const temaResolvers = {
  Query: {
    temos: (obj: {}, args: {}, context: Request) => {
      return connection.models.tema.findAll({ include: [connection.models.temosAdminTaisykle] });
    },
    tema: (obj: {}, { id }: any, context: Request) => {
      return connection.models.tema.find({ where: { id } });
    },
  },
  Mutation: {
    createTema(value: {}, { tema }: any, context: Request) {
      return connection.models.tema.create({
        pavadinimas: tema.pavadinimas,
        aprasymas: tema.aprasymas,
      }).then((created: any) => {
        if (tema.temosAdminTaisykle)
          tema.temosAdminTaisykle.forEach((taisykle: any) => {
            connection.models.temosAdminTaisykle.create({
              pavadinimas: taisykle.pavadinimas,
              aprasymas: taisykle.aprasymas,
              temaId: created.id,
            });
          });
        return created;
      });
    },
    // pvz sudetingiausias mutation:
    updateTema(value: any, { tema }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.tema.find({ where: { id: tema.id } }).then((item: any) => {
        // paupdatina rastos temos atributus
        return item.updateAttributes(tema).then((updated: any) => {
          // sunaikina senas taisykles
          connection.models.temosAdminTaisykle.destroy({
            where: { temaId: updated.id },
          });
          // jei yra nauju administravimo taisykliu
          if (tema.temosAdminTaisykle)
            // su kiekvienas (foreach daro kazka)
            tema.temosAdminTaisykle.forEach((taisykle: any) => {
              // sukuria taisykel naudojant jos duomenis ir paupdatintos temos id kaip fkey
              connection.models.temosAdminTaisykle.create({
                pavadinimas: taisykle.pavadinimas,
                aprasymas: taisykle.aprasymas,
                temaId: updated.id,
              });
            });
          // returnina paupdatintą  temą
          return updated;
        });
      });
    },
    deleteTema(value: any, { id }: any) {
      return connection.models.tema.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
  },
};
