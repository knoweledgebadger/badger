import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../../badger/db/connection';
import { Auth } from '../../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const komentarasType = `
    input DiskusijosKomentarasInput {
      id: Int
      tekstas: String
      userId: Int
    }

    type DiskusijosKomentaras {
      id: Int
      tekstas: String
      userId: Int
      user: User
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Diskusija" masyvas
export const komentarasQuery = `
  
`;

// mutationų pavadinimai ir ką jie grąžina 
// pvz createDiskusija sukuria komentaras iš komentarasInput ir grąžina Diskusija
export const komentarasMutation = `
    createKomentaras(komentaras: DiskusijosKomentarasInput!, diskusijosId: Int!):
     DiskusijosKomentaras
    editKomentaras(komentaras: DiskusijosKomentarasInput!): DiskusijosKomentaras
`;
// instrukcijos ką daryti kiekvieno query/mutation metu
export const komentarasResolvers = {
  Query: {

  },
  Mutation: {
    createKomentaras(value: {}, { komentaras, diskusijosId }: any, context: Request) {
      return connection.models.komentaras.create({
        diskusijaId: diskusijosId,
        tekstas: komentaras.tekstas,
      }).then((created: any) => created);
    },
    // pvz sudetingiausias mutation:
    editKomentaras(value: any, { komentaras }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.komentaras.find({
        where: {
          id: komentaras.id,
        },
      }).then((item: any) => {
        connection.models.komentaroIstorinisIrasas.create({
          tekstas: item.tekstas,
          komentarasId: item.id,
        });
        // paupdatina rastos diskusijos atributus
        return item.updateAttributes({
          ...komentaras,
          keitimoData: Date.now(),
        }).then((updated: any) => updated);
      });
    },

  },
};
