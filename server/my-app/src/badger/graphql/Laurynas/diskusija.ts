import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../../badger/db/connection';
import { Auth } from '../../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const diskusijaType = `
    input DiskusijaInput {
      id: Int
      tekstas: String
      aprasymas: String
      sukurimoData: String
      userId: Int
      temaId: Int
      komentaras: [DiskusijosKomentarasInput]
    }

    type Diskusija {
      id: Int
      tekstas: String
      aprasymas: String
      sukurimoData: String
      user: User
      userId: Int
      temaId: Int
      komentaras: [DiskusijosKomentaras]
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Diskusija" masyvas
export const diskusijaQuery = `
  diskusijos(paieska: String): [Diskusija]
  diskusija (id: Int!): Diskusija
`;

// mutationų pavadinimai ir ką jie grąžina 
// pvz createDiskusija sukuria diskusija iš diskusijaInput ir grąžina Diskusija
export const diskusijaMutation = `
    createDiskusija(diskusija: DiskusijaInput!): Diskusija
    updateDiskusija(diskusija: DiskusijaInput!): Diskusija
    deleteDiskusija(id: Int!): Diskusija
`;
// instrukcijos ką daryti kiekvieno query/mutation metu
export const diskusijaResolvers = {
  Query: {
    diskusijos: (obj: {}, { paieska }: any, context: Request) => {
      if (paieska && paieska.length > 1) {
        connection.models.paieskosIrasas.create({ tekstas: paieska });
        return connection.models.diskusija.findAll({
          where: {
            $or: [
              { aprasymas: { $iLike: '%' + paieska + '%' } },
              { tekstas: { $iLike: '%' + paieska + '%' } },
            ],
          },
        });
      }
      return connection.models.diskusija.findAll({
      });
    },
    diskusija: (obj: {}, { id }: any, context: Request) => {

      return connection.models.diskusija.find({
        where: { id },
        include: [
          {
            model: connection.models.komentaras,
          },
          connection.models.user,
        ],
      }).then((result) => {
        return result;
      });

    },
  },
  Mutation: {
    createDiskusija(value: {}, { diskusija }: any, context: Request) {
      return connection.models.diskusija.create({
        id: diskusija.id,
        tekstas: diskusija.tekstas,
        aprasymas: diskusija.aprasymas,
        userId: diskusija.userId,
        temaId: diskusija.temaId,
      }).then((created: any) => created);
    },
    // pvz sudetingiausias mutation:
    updateDiskusija(value: any, { diskusija }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.diskusija.find({ where: { id: diskusija.id } }).then((item: any) => {
        connection.models.diskusijosIstorinisIrasas.create({
          tekstas: item.tekstas,
          aprasymas: item.aprasymas,
          diskusijaId: item.id,
        });
        // paupdatina rastos diskusijos atributus
        return item.updateAttributes({
          ...diskusija,
          keitimoData: Date.now(),
        }).then((updated: any) => updated);
      });
    },
    deleteDiskusija(value: any, { id }: any) {
      return connection.models.diskusija.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
  },
};
