import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

export const userType = `
    input UserInput {
        id: Int
        name: String
        surname: String
        role: String
        email: String
        username: String
        password: String
        language: String
        vartotojas: VartotojasInput
        fk_vartotojas: String
    }

    type User {
        id: Int
        name: String
        surname: String
        role: String
        email: String
        username: String
        language: String
        vartotojas: Vartotojas
        fk_vartotojas: String
    }

    type UserResult {
      success: Boolean
      data: User
    }
`;

export const userQuery = `
    users: [User]
`;

export const userMutation = `
    createUser(user: UserInput!): User
    updateUser(user: UserInput!): User
    deleteUser(id: Int!): User
    changePassword(userId: Int!, oldPassword: String, newPassword: String!): UserResult
`;

export const userResolvers = {
  Query: {
    users: (obj: {}, args: {}, context: Request) => {
      return connection.models.user.findAll({ include: [connection.models.vartotojas] });
    },
  },
  Mutation: {
    createUser(value: {}, { user }: any, context: Request) {
      return connection.models.user.create({
        name: user.name,
        surname: user.surname,
        role: user.role,
        email: user.email,
        username: user.username,
        password: user.password,
        language: user.language,
      });
    },
    updateUser(value: any, { user }: any) {
      return connection.models.user.find({ where: { id: user.id } }).then((item: any) => {
        return item.updateAttributes(user).then((updated: any) => {
          return connection.models.vartotojas.find({ where: { pseudonimas: user.vartotojas.pseudonimas } }).then((item: any) => {
            return item.updateAttributes(user.vartotojas).then((updated: any) => updated);
          });
        });
      });
    },
    deleteUser(value: any, { id }: any) {
      return connection.models.user.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
    changePassword(value: {}, { userId, oldPassword, newPassword }: any, context: Request) {
      const jwt = Auth.getJWT(context);
      let condition = { id: userId };
      const permissions = _.get<string[]>(jwt, 'permissions');
      const requireOldPassword = permissions != null && (permissions.indexOf('ADMIN') < 0);
      if (requireOldPassword) condition = _.assign(condition, { password: oldPassword });
      const options = { where: condition, returning: true };

      return connection.models.user.update({ password: newPassword }, options)
        .then((result) => {
          return { success: result[0] === 1, data: _.get(result[1][0], 'dataValues') };
        });
    },
  },
};
