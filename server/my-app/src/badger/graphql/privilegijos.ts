import * as _ from 'lodash';
import { Request } from 'express';
import connection, { ownerConn } from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const privilegijosType = `
    input PrivilegijosInput {
      id: Boolean
      rasyti_temas: Boolean
      rasyti_straipsnius: Boolean
      redaguoti_straipsnius: Boolean
      vertinti_vartotoja: Boolean
      trinti_komentarus: Boolean
      trinti_straipsnius: Boolean
      trinti_temas: Boolean
      kurti_aprasymus: Boolean
      trinti_aprasymus: Boolean
      redaguoti_aprasymus: Boolean
    }

    type Privilegijos {
      id: Boolean
      rasyti_temas: Boolean
      rasyti_straipsnius: Boolean
      redaguoti_straipsnius: Boolean
      vertinti_vartotoja: Boolean
      trinti_komentarus: Boolean
      trinti_straipsnius: Boolean
      trinti_temas: Boolean
      kurti_aprasymus: Boolean
      trinti_aprasymus: Boolean
      redaguoti_aprasymus: Boolean
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Tema" masyvas
export const privilegijosQuery = `
  privilegijos (id: Int!): Privilegijos
`;

export const privilegijosMutation = `
createPrivilegijos(privilegijos: PrivilegijosInput!): Privilegijos
editPrivilegijos(privilegijos: PrivilegijosInput!): Privilegijos
`;

// instrukcijos ką daryti kiekvieno query/mutation metu
export const privilegijosResolvers = {
  Query: {
    privilegijos: (obj: {}, { }: any, context: Request) => {
      return connection.models.privilegijos.find();
    },
  },
  Mutation: {
    // pvz sudetingiausias mutation:
    editPrivilegijos(value: any, { privilegijos }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.privilegijos.find({ where: { id: privilegijos.id } }).then((item: any) => {
        // paupdatina rastos temos atributus
        return item.updateAttributes(privilegijos).then((updated: any) => updated);
      });
    },
    createPrivilegijos(value: {}, { privilegijos }: any) {
      return ownerConn.models.privilegijos.create({
        id: privilegijos.id,
        rasyti_temas: privilegijos.rasyti_temas,
        rasyti_straipsnius: privilegijos.rasyti_straipsnius,
        redaguoti_straipsnius: privilegijos.redaguoti_straipsnius,
        vertinti_vartotoja: privilegijos.vertinti_vartotoja,
        trinti_komentarus: privilegijos.trinti_komentarus,
        trinti_straipsnius: privilegijos.trinti_straipsnius,
        trinti_temas: privilegijos.trinti_temas,
        kurti_aprasymus: privilegijos.kurti_aprasymus,
        trinti_aprasymus: privilegijos.trinti_aprasymus,
        redaguoti_aprasymus: privilegijos.redaguoti_aprasymus,
      }).then((created: any) => created);
    },
  },
};
