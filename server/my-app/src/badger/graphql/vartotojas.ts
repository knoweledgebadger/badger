import * as _ from 'lodash';
import { Request } from 'express';
import connection, { ownerConn } from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

// tipai querinimui ir ra6ymui iš frontend
export const vartotojasType = `
    input VartotojasInput {
      vardas: String
      pseudonimas: String
      slaptazodis: String
      el_pastas: String
      lytis: String
      sukurimo_data: Date
      bendras_ivertis: Int
      avataras: String
      role_id: Int
      privilegijos_id: Int
      privilegijos: PrivilegijosInput
    }

    type Vartotojas {
      vardas: String
      pseudonimas: String
      slaptazodis: String
      el_pastas: String
      lytis: String
      sukurimo_data: Date
      bendras_ivertis: Int
      avataras: String
      role_id: Int
      galutinis_ivert: Float
      privilegijos_id: Int
      privilegijos: Privilegijos
    }
`;
// queriu pavadinimai ir ka jie grazina siuo atveju type "Tema" masyvas
export const vartotojasQuery = `
  vartotojai: [Vartotojas]
  vartotojas (id: String!): Vartotojas
`;

// mutationų pavadinimai ir ką jie grąžina 
// pvz createTema sukuria tema iš temaInput ir grąžina Tema
export const vartotojasMutation = `
    createVartotojas(vartotojas: VartotojasInput!): Vartotojas
    editVartotojas(vartotojas: VartotojasInput!): Vartotojas
`;
// instrukcijos ką daryti kiekvieno query/mutation metu
export const vartotojasResolvers = {
  Query: {
    vartotojai: (obj: {}, args: {}, context: Request) => {
      return connection.models.vartotojas.findAll({ include: [connection.models.role, connection.models.privilegijos] });
    },
    vartotojas: (obj: {}, { pseudonimas }: any, context: Request) => {
      return connection.models.vartotojas.find({ where: { pseudonimas } });
    },
  },
  Mutation: {
    // pvz sudetingiausias mutation:
    editVartotojas(value: any, { vartotojas }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.vartotojas.find({ where: { pseudonimas: vartotojas.pseudonimas } }).then((item: any) => {
        // paupdatina rastos temos atributus
        return item.updateAttributes(vartotojas).then((updated: any) => {
          console.log(vartotojas.privilegijos);
          console.log(vartotojas.privilehijos_id);
          if (vartotojas.privilegijos_id) {
            connection.models.privilegijos.find({ where: { id: vartotojas.privilegijos_id } }).then((item: any) => {
              // paupdatina rastos temos atributus
              item.updateAttributes({ ...vartotojas.privilegijos });
            });
          } else {
            connection.models.privilegijos.create({ ...vartotojas.privilegijos }).then((created: any) => {
              return connection.models.vartotojas.find({ where: { pseudonimas: vartotojas.pseudonimas } }).then((item: any) => {
                // paupdatina rastos temos atributus
                return item.updateAttributes({ ...vartotojas, privilegijos_id: created.id }).then((updated: any) => { });
              });
            });
          }
          return updated;
        });
      });
    },
    createVartotojas(value: {}, { vartotojas }: any) {
      return ownerConn.models.vartotojas.create({
        vardas: vartotojas.vardas,
        pseudonimas: vartotojas.pseudonimas,
        slaptazodis: vartotojas.slaptazodis,
        el_pastas: vartotojas.el_pastas,
        lytis: vartotojas.lytis,
        sukurimo_data: vartotojas.sukurimo_data,
        bendras_ivertis: vartotojas.bendras_ivertis,
        avataras: vartotojas.avataras,
        role_id: vartotojas.role_id
      }).then((created: any) => created);
    },
  },
};
