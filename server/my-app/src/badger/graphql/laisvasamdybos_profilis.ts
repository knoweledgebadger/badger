import * as _ from 'lodash';
import { Request } from 'express';
import connection from '../../badger/db/connection';
import { Auth } from '../../routes/auth/auth';

export const laisvasamdybosProfilisType = `
    input LaisvasamdybosProfilisInput {
        id: Int
        portfolio_nuoroda: String
        cv_nuoroda: String
    }

    type LaisvasamdybosProfilis {
        id: Int
        portfolio_nuoroda: String
        cv_nuoroda: String
    }
`;

export const laisvasamdybosProfilisQuery = `
  laisvasamdybosProfiliai: [LaisvasamdybosProfilis]
  laisvasamdybosProfilis (id: Int!): LaisvasamdybosProfilis
`;

export const laisvasamdybosProfilisMutation = `
    createLaisvasamdybosProfilis(laisvasamdybosProfilis: LaisvasamdybosProfilisInput!): LaisvasamdybosProfilis
    updateLaisvasamdybosProfilis(laisvasamdybosProfilis: LaisvasamdybosProfilisInput!): LaisvasamdybosProfilis
    deleteLaisvasamdybosProfilis(id: Int!): LaisvasamdybosProfilis
`;

export const laisvasamdybosProfilisResolvers = {
  Query: {
    laisvasamdybosProfiliai: (obj: {}, args: {}, context: Request) => {
      return connection.models.laisvasamdybosProfilis.findAll();
    },
    laisvasamdybosProfilis: (obj: {}, { id }: any, context: Request) => {
      return connection.models.laisvasamdybosProfilis.find({ where: { id } });
    },
  },
  Mutation: {
    createLaisvasamdybosProfilis(value: {}, { laisvasamdybosProfilis }: any, context: Request) {
      return connection.models.laisvasamdybosProfilis.create({
        portfolio_nuoroda: laisvasamdybosProfilis.portfolio_nuoroda,
        cv_nuoroda: laisvasamdybosProfilis.cv_nuoroda,
      });
    },
    updateLaisvasamdybosProfilis(value: any, { laisvasamdybosProfilis }: any) {
      // suranda temą duombazeje pagal jos id
      return connection.models.laisvasamdybosProfilis.find({ where: { id: laisvasamdybosProfilis.id } }).then((item: any) => {
        // paupdatina rastos temos atributus
        return item.updateAttributes(laisvasamdybosProfilis);
      });
    },
    deleteLaisvasamdybosProfilis(value: any, { id }: any) {
      return connection.models.laisvasamdybosProfilis.destroy({ where: { id } }).then((deleted: any) => deleted);
    },
  },
};
