import { Server } from './app';
import  logger  from './logger';
const app = Server.getApp();

app.listen(3000, () => {
  logger('Badgerrrr api server listening on port 3000!');
});

