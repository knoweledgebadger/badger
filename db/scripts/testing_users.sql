INSERT INTO badger.users (name, surname, email, role, username, password, language,  fk_vartotojas, "createdAt", "updatedAt")
VALUES('Laurynas', 'Bičkauskas', 'laurynas.bickauskas@gmail.lt','ADMIN', 'laubic', 'Laubic-1', 'LT', 'laubic', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO badger.users (name, surname, email, role, username, password, language,  fk_vartotojas, "createdAt", "updatedAt")
VALUES('Jurgis', 'Kišūnas', 'jurgis.kisunas@gmail.lt','USER', 'jurkis', 'Jurkist-1', 'LT', 'jurkis', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO badger.users (name, surname, email, role, username, password, language,  fk_vartotojas, "createdAt", "updatedAt")
VALUES('Aurimas', 'Butkus', 'aurimas.butkus@gmail.com','ADMIN', 'aurbut', 'Aurbut-1', 'LT', 'aurbut', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO badger.users (name, surname, email, role, username, password, language,  fk_vartotojas, "createdAt", "updatedAt")
VALUES('Lukas', 'Savostas', 'lukas.savostas@gmail.lt','ADMIN', 'luksav', 'Luksav-1', 'LT', 'luksav', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

