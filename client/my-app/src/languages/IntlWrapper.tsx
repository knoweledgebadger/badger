import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import * as jwtDecode from 'jwt-decode';

import { State } from '../rootReducer';
import translate from './translate';
import Auth from '../Auth';

interface IntlWrapperProps {
  locale: string;
  // tslint:disable-next-line
  messages: any;
}

class IntlWrapper extends React.Component<IntlWrapperProps & RouteComponentProps<{}>> {
  render() {
    const { children, locale, messages } = this.props;

    return (
      <IntlProvider locale={locale} messages={messages}>
        {children}
      </IntlProvider>
    );
  }
}

function mapStateToProps(state: State) {
  // const token = state.sessionReducer.token;
  const token = Auth.getToken();
  const oldLang = !localStorage.getItem('language') ? 'LT' : localStorage.getItem('language');
  const lang = !token ? oldLang : jwtDecode<{ language: string }>(token).language;
  localStorage.setItem('language', lang!);
  const locale: string = lang!.toLowerCase();
  const messages = translate(lang!);
  return { locale, messages };
}

export default connect(mapStateToProps)(IntlWrapper as any);
