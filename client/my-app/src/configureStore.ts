import { createStore, applyMiddleware } from 'redux';
const thunk = require('redux-thunk').default;
const devtools = require('redux-devtools-extension/developmentOnly');
import rootReducer, { resetState } from './rootReducer';
import Auth from './Auth';
import { sessionExpired } from './modules/login/sessionReducer';
import ReactApollo from './ReactApollo';

// tslint:disable-next-line
export function configureStore(history: any) {
  // tslint:disable-next-line
  const jwtMiddleware = (store: any) => {
    // tslint:disable-next-line
    return (next: any) => (action: any) => {
      if (typeof action === 'function' && Auth.isTokenExpired()) {
        Auth.deauthenticateUser();
        store.dispatch(resetState());
        store.dispatch(sessionExpired());
        return;
      }
      return next(action);
    };
  };

  const apollo = ReactApollo.getApolloClient();
  const composeEnhancers = devtools.composeWithDevTools({});
  const enhancer = composeEnhancers(
    applyMiddleware(apollo.middleware(), history, jwtMiddleware, thunk),
  );

  return createStore(rootReducer, enhancer);
}
