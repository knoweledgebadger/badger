import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field, BaseFieldProps } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import { black } from 'material-ui/styles/colors';
import { default as ClearIcon } from 'material-ui/svg-icons/content/clear';
import { IconButton } from 'material-ui';

interface FormFieldProps {
  name: string;
  component?: React.ReactNode;
  hint?: string;
  type?: string;
  fullWidth?: boolean;
  label?: string;
  colClassName?: string;
  disabled?: boolean;
  underlineDisabledStyle?: {};
  style?: React.CSSProperties;
  onChange?: Function;
  inputStyle?: {};
  value?: {};
  bold?: boolean;
  onNewRequest?: Function;
  limitMaxSearchResults?: boolean;
  noHint?: boolean;
  multiLine?: boolean;
  floatingLabelFixed?: boolean;
  floatingLabelShrinkStyle?: React.CSSProperties;
  floatingLabelStyle?: React.CSSProperties;
  resetSearchText?: boolean;
  includeEquipmentName?: boolean;
  withRef?: boolean;
  objectId?: number;
  clear?: () => void;
  filter?: {};
  rows?: number;
  uzrasas?: string;
}

class FormField extends React.Component<BaseFieldProps & FormFieldProps, {}> {
  static defaultProps = {
    component: TextField,
    fullWidth: true,
    type: 'text',
    colClassName: 'col-sm-12',
    disabled: false,
    floatingLabelFixed: false,
  };

  private field;

  constructor(props) {
    super(props);
    this.getRenderedComponent = this.getRenderedComponent.bind(this);
  }

  getRenderedComponent() {
    return this.field.getRenderedComponent();
  }

  render() {
    const {
    label, children, colClassName, floatingLabelFixed, hint, style, clear, normalize, rows,
      disabled, underlineDisabledStyle, inputStyle, multiLine, bold, ...rest } = this.props;
    const floatingLabelText = label ? <FormattedMessage id={label} /> : undefined;
    const hintText = hint ? <FormattedMessage id={hint} /> : undefined;
    const textareaStyle = disabled ? {
      color: black, fontWeight: bold ? 'bold' : undefined,
      cursor: 'default',
    } : inputStyle;

    const textFieldInputWidth = clear ? { width: 'calc(100% - 48px)' } : { width: '100%' };
    const textFieldInputStyle = {
      ...textareaStyle,
      ...textFieldInputWidth,
    };

    return (
      <div className={colClassName}>
        <div className={'row'}>
          <div className="col-12" style={{ position: 'relative', display: 'inline-block' }}>
            {clear && <IconButton
              onClick={clear}
              style={{ position: 'absolute', right: 15, top: 22, zIndex: 1, padding: 16 }}
              iconStyle={{ width: 16, height: 16, color: 'rgba(0, 0, 0, 0.3' }}
              disabled={disabled}
            >
              <ClearIcon />
            </IconButton>
            }
            <Field
              floatingLabelText={floatingLabelText}
              hintText={hintText}
              floatingLabelFixed={disabled ? true : floatingLabelFixed}
              disabled={disabled}
              multiLine={multiLine}
              style={disabled ? { cursor: 'default' } : style}
              inputStyle={textFieldInputStyle}
              textareaStyle={textareaStyle}
              underlineDisabledStyle={disabled ? { display: 'none' } : underlineDisabledStyle}
              normalize={normalize}
              rows={rows}
              ref={c => this.field = c}
              {...rest}>
              {children}
            </Field>
          </div>
        </div>
      </div >
    );
  }
}

export default FormField;
