import * as React from 'react';
import FormTitle from './FormTitle';

interface FormTitleWithButtonsProps {
  title: string;
  formatTitle?: boolean;
}

const formTitleWithButtons: React.StatelessComponent<FormTitleWithButtonsProps> = (props) => {
  const { title, children, formatTitle } = props;
  return (
    <FormTitle title={title} formatTitle={formatTitle}>
      <div className="float-right form-group">
        <div className="btn-group">
          {children}
        </div>
      </div>
    </FormTitle>
  );
};

formTitleWithButtons.defaultProps = {
  formatTitle: true,
};

export default formTitleWithButtons;
