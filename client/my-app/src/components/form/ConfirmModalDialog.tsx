import * as React from 'react';
import { injectIntl, InjectedIntlProps } from 'react-intl';
import FlatButton from 'material-ui/FlatButton';
import ClosableModal from './ClosableModal';

export interface ConfirmModalDialogProps {
  show: boolean;
  onClose: () => void;
  onConfirm: () => void;
  cancelMessageKey: string;
  confirmMessageKey: string;
  dialogMessageKey: string;
}

const confirmModalDialog = (props: ConfirmModalDialogProps & InjectedIntlProps) => {

  const actions = [
    (
      <FlatButton
        label={props.intl!.formatMessage({ id: props.cancelMessageKey })}
        primary={true}
        onTouchTap={props.onClose}
      />
    ),
    (
      <FlatButton
        label={props.intl!.formatMessage({ id: props.confirmMessageKey })}
        primary={true}
        keyboardFocused={true}
        onTouchTap={props.onConfirm}
      />
    ),
  ];

  return (
    <ClosableModal
      open={props.show}
      onHide={props.onClose}
      actions={actions}
      title={props.dialogMessageKey} />
  );
};

export default injectIntl(confirmModalDialog);
