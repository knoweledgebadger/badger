import * as React from 'react';
import * as moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { TimePicker } from 'redux-form-material-ui';

const timePicker: React.StatelessComponent<any> = (props) => {
  return (
    <TimePicker 
      format={'24hr'}
      value={moment(props.input.value).toDate()}
      cancelLabel={<FormattedMessage id="cancel" />}
      okLabel={<FormattedMessage id="ok" />}
      autoOk={true}
      {...props}
    />
  );
};

export default timePicker;
