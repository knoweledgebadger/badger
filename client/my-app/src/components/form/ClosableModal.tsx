import * as React from 'react';
import Dialog from 'material-ui/Dialog';
import { injectIntl, InjectedIntlProps } from 'react-intl';

export interface ClosableModalProps {
  onHide: Function;
  open: boolean;
  title: string;
  // tslint:disable-next-line
  actions?: any[];
}

class ClosableModal extends React.Component<ClosableModalProps & InjectedIntlProps, {}> {
  constructor() {
    super();
    this.onExit = this.onExit.bind(this);
  }

  onExit() {
    if (location.hash) {
      $(window).off('hashchange');
      history.replaceState({}, document.title, window.location.pathname + window.location.search);
    }
  }

  onEnter() {
    const uniqueId = Math.round(new Date().getTime() + (Math.random() * 100)).toString();
    window.location.hash = uniqueId;
    $(window).on('hashchange', (event: any) => {
      if (location.hash !== `#${uniqueId}`) {
        this.props.onHide();
        $(this).off(event);
      }
    });
  }

  render() {
    if (this.props.open) {
      this.onEnter.bind(this);
    }
    return (
      <Dialog
        title={this.props.intl!.formatMessage({ id: this.props.title })}
        actions={this.props.actions}
        modal={true}
        autoScrollBodyContent={false}
        onRequestClose={this.onExit}
        open={this.props.open}>
        {this.props.children}
      </Dialog>
    );
  }
}

export default injectIntl(ClosableModal);
