import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field, BaseFieldProps } from 'redux-form';
import { grey400 } from 'material-ui/styles/colors';
import { Toggle } from 'redux-form-material-ui';

interface FormFieldProps {
  name: string;
  type?: string;
  label: string;
}

const toggleField: React.StatelessComponent<BaseFieldProps & FormFieldProps> = (props) => {
  const { label, name } = props;
  return (
    <div>
      <div className="col-md-3" style={{ color: grey400 }}>
        <FormattedMessage id={label} />
      </div>
      <div className="row">
        <div className="col-md-3" style={{ marginBottom: 14 }}>
          <Field
            name={name}
            component={Toggle}
            style={{ left: 14 }}
          />
        </div>
      </div>
    </div>
  );
};

toggleField.defaultProps = {
  component: Toggle,
  type: 'toggle',
};

export default toggleField;
