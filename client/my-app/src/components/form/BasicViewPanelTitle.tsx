import * as React from 'react';
import { default as NavigationCancel } from 'material-ui/svg-icons/navigation/cancel';
import FormTitleWithButtons from './FormTitleWithButtons';
import FloatingButtonWithToolTip from '../material/FloatingButtonWithToolTip';

interface BasicFormTitleProps {
  onCancel: () => void;
  title: string;
  cancelTooltip?: string;
  usePlainMessage?: boolean;
}

const basicFormTitle: React.StatelessComponent<BasicFormTitleProps> = (props) => {
  const { onCancel, title, cancelTooltip, usePlainMessage } = props;
  return (
    <FormTitleWithButtons formatTitle={!usePlainMessage} title={title}>
      <FloatingButtonWithToolTip
        tooltipId={cancelTooltip}
        onMouseDown={onCancel}>
        <NavigationCancel />
      </FloatingButtonWithToolTip>
    </FormTitleWithButtons>
  );

};
basicFormTitle.defaultProps = {
  cancelTooltip: 'button.cancel.tooltip',
};

export default basicFormTitle;
