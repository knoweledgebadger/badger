import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import Box from '../material/Box';

export interface FormBoxProps {
  header?: string;
}

const formBox: React.StatelessComponent<FormBoxProps> = (props) => {
  const { header, children } = props;

  return (
    <Box
      header={(header) ? <FormattedMessage id={header} /> : ''}
      divider={false}
      body={
        <div className="row">
          {children}
        </div>
        }
    />
  );
};

export default formBox;
