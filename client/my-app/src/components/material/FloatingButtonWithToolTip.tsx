import * as React from 'react';
import { injectIntl, InjectedIntlProps } from 'react-intl';
import Tooltip from 'material-ui/internal/Tooltip';
import FloatingActionButton from 'material-ui/FloatingActionButton';

export interface FloatingButtonWithToolTipProps {
  onMouseDown?: () => void;
  onTouchTap?: () => void;
  tooltipId?: string;
  secondary?: boolean;
  style?: React.CSSProperties;
}

interface FloatingButtonWithToolTipState {
  showToolTip: boolean;
}

class FloatingButtonWithToolTip extends
  React.Component<FloatingButtonWithToolTipProps & InjectedIntlProps,
  FloatingButtonWithToolTipState> {

  constructor(props: FloatingButtonWithToolTipProps & InjectedIntlProps) {
    super(props);
    this.state = {
      showToolTip: false,
    };
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseEnter() {
    this.setState({
      showToolTip: true,
    });
  }

  onMouseLeave() {
    this.setState({
      showToolTip: false,
    });
  }

  render() {
    const { style } = this.props;
    return (
      <div>
        <Tooltip
          show={this.state.showToolTip}
          label={this.props.tooltipId ? this.props.intl!.formatMessage(
            { id: this.props.tooltipId }) : ''}
          horizontalPosition="left" verticalPosition="top" touch={true} />
        <FloatingActionButton secondary={this.props.secondary} mini={true}
          style={style}
          onMouseDown={this.props.onMouseDown || this.props.onTouchTap}
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}>
          {this.props.children}
        </FloatingActionButton>
      </div>
    );
  }
}

export default injectIntl(FloatingButtonWithToolTip);
