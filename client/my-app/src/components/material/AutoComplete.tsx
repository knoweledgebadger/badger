import * as React from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import { FlatButton } from 'material-ui';

export interface AutoCompleteProps {
  fullWidth?: boolean;
  openOnFocus: boolean;
  // tslint:disable-next-line
  onNewRequest: (value: any, index?: number) => void;
  // tslint:disable-next-line
  dataSource: any[];
  dataSourceConfig: { text: string, value: string };
  hintText: React.ReactNode;
  name: string;
  resetSearchText?: boolean;
  clearSearchText?: boolean;
  // tslint:disable-next-line
  initialValue?: any;
  limitMaxSearchResults?: boolean;
}

export interface AutoCompleteState {
  searchText: string;
  // tslint:disable-next-line
  initialValue?: any;
  searchTextCleared?: boolean;
}

class AutoCompleteWrapper extends React.Component<AutoCompleteProps, AutoCompleteState> {
  static muiName = 'AutoComplete';

  constructor(props: AutoCompleteProps) {
    super();
    this.state = {
      searchText: '',
      initialValue: props.initialValue,
    };
    this.handleUpdateInput = this.handleUpdateInput.bind(this);
    this.handleNewRequest = this.handleNewRequest.bind(this);
    this.clearSearchField = this.clearSearchField.bind(this);
  }

  componentDidUpdate() {
    if (this.state.initialValue && this.props.dataSource.length > 0) {
      this.setState({
        searchText: this.props.dataSource
          // tslint:disable-next-line
          .find((item: any) => { return item.id === this.state.initialValue; })[this.props.dataSourceConfig.text],
        initialValue: null,
      });
    }
  }

  handleUpdateInput(searchText: string) {
    this.setState({
      searchText,
    });
  }

  // tslint:disable-next-line
  handleNewRequest(value: any, index?: number) {
    this.setState({
      searchText: this.props.resetSearchText ? '' : value[this.props.dataSourceConfig.text],
    });
    this.props.onNewRequest(value, index);
  }

  // tslint:disable-next-line
  clearSearchField() {
    this.setState({
      searchText: '',
    });
    this.props.onNewRequest(null);
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-10">
            <AutoComplete
              name={this.props.name}
              hintText={this.props.hintText}
              filter={AutoComplete.caseInsensitiveFilter}
              dataSource={this.props.dataSource}
              dataSourceConfig={this.props.dataSourceConfig}
              searchText={this.state.searchText}
              onUpdateInput={this.handleUpdateInput}
              onNewRequest={this.handleNewRequest}
              fullWidth={this.props.fullWidth}
              openOnFocus={this.props.openOnFocus}
              listStyle={{ width: '100%' }}
              textFieldStyle={{ minWidth: '100%', width: '100%' }}
              style={{ minWidth: '100%', width: '100%' }}
              maxSearchResults={this.props.limitMaxSearchResults ? 6 : 100} />
          </div>
          <div className="col-2">
            <FlatButton
              onClick={() => this.clearSearchField()}
              label="x"
              style={
                {
                  minWidth: 'initial',
                  height: 'initial',
                  left: '-35px',
                  top: '5px',
                }
              }
              primary={true} />
          </div>
        </div>
      </div>
    );
  }
}

export default AutoCompleteWrapper;
