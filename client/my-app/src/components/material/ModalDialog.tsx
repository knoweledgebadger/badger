import * as React from 'react';
import Dialog from 'material-ui/Dialog';
import ModalDialogTitle from './ModalDialogTitle';

export interface ModalDialogProps {
  open: boolean;
  title: string;
  onClose: () => void;
}

const modalDialog: React.StatelessComponent<ModalDialogProps> = (props) => {

  const { open, onClose, title, children } = props;
  return (
    <Dialog
      autoScrollBodyContent={false}
      repositionOnUpdate={false}
      autoDetectWindowHeight={false}
      contentStyle={{
        width: '100%',
        maxWidth: '750px',
        maxHeight: '100% !important',
      }}
      bodyStyle={{
        maxHeight: '100% !important',
      }}
      style={{
        paddingTop: '5px',
        marginTop: '-65px',
        bottom: '5px',
        overflowY: 'scroll',
        height: 'auto',
      }}
      open={open}
      modal={true}
      onRequestClose={onClose}>
      <ModalDialogTitle title={title} close={onClose} />
      {children}
    </Dialog>
  );
};

export default modalDialog;
