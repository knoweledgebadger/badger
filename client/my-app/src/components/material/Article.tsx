import * as React from 'react';

export interface ArticleProps {
  headerTitle?: React.ReactNode;
  hideTitle?: boolean;
  className?: string;
}

const article: React.StatelessComponent<ArticleProps> = (props) => {
  const { className, headerTitle, hideTitle, children } = props;

  return (
    <div className={`article ${className}`}>
      {!hideTitle && <div className="article-title">{headerTitle}</div>}
      {children}
    </div>
  );
};

article.defaultProps = {
  className: '',
};

export default article;
