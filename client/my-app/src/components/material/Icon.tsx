import * as React from 'react';

export interface IconProps {
  name: string;
  className?: string;
}

const icon: React.StatelessComponent<IconProps> = (props) => {
  const { name, className } = props;

  return (
    <i className={`material-icons ${className}`}>{name}</i>
  );
};

export default icon;
