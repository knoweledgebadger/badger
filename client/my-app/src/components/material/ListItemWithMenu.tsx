import * as React from 'react';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { TouchTapEventHandler } from 'material-ui';
import { default as ContentEdit } from 'material-ui/svg-icons/editor/mode-edit';
import { default as ContentDelete } from 'material-ui/svg-icons/action/delete';
import { default as ContentCopy } from 'material-ui/svg-icons/av/library-books';
import { default as MoreVertIcon } from 'material-ui/svg-icons/navigation/more-vert';

export interface TankListItemProps {
  onTouchTap?: TouchTapEventHandler;
  primaryText: React.ReactNode;
  secondaryText?: React.ReactNode;
  onEditClick: (event) => void;
  onRemoveClick: (event) => void;
  onCopyClick?: (event) => void;
  copyButton?: boolean;
  hideDelete?: boolean;
  hideAllButtons?: boolean;
}

const listItem: React.StatelessComponent<TankListItemProps> = (props) => {
  const {
    onTouchTap, primaryText, onEditClick, onRemoveClick,
    secondaryText, copyButton, onCopyClick, hideDelete, hideAllButtons } = props;

  return (
    <ListItem
      onTouchTap={onTouchTap}
      primaryText={primaryText}
      secondaryText={secondaryText}
      rightIconButton={
        hideAllButtons ? <div /> :
          <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }>
            <MenuItem>
              <IconButton
                iconStyle={{ width: '16px', height: '16px' }}
                onClick={onEditClick}>
                <ContentEdit />
              </IconButton>
            </MenuItem>
            {!hideDelete && <MenuItem>
              <IconButton
                iconStyle={{ width: '16px', height: '16px' }}
                onClick={onRemoveClick}>
                <ContentDelete />
              </IconButton>
            </MenuItem>}
            {copyButton && <MenuItem>
              <IconButton
                iconStyle={{ width: '16px', height: '16px' }}
                onClick={onCopyClick}>
                <ContentCopy />
              </IconButton>
            </MenuItem>}
          </IconMenu>
      } />
  );
};

export default listItem;
