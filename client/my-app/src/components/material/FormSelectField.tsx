import * as React from 'react';
import MenuItem from 'material-ui/MenuItem';
import { Validator } from 'redux-form';
import SelectField from '../validation/SelectField';
import FormField from '../../components/form/FormField';
import { FormattedMessage } from 'react-intl';

interface SelectFieldProps {
  value?: string;
  label?: string;
  options: {}[];
  valueField: string;
  textField: string;
  fullWidth?: boolean;
  name: string;
  onFieldSelect?: (value: any) => void;
  validate?: Validator | Validator[];
  colClassName?: string;
  floatingLabelFixed?: boolean;
  style?: React.CSSProperties;
  disabled?: boolean;
  useFieldAsMessageId?: boolean;
  hint?: string;
}

const selectField: React.StatelessComponent<SelectFieldProps> = (props) => {
  const { label, options, valueField, textField, fullWidth, name, useFieldAsMessageId,
    validate, colClassName, onFieldSelect, floatingLabelFixed, disabled, hint } = props;
  return (
    <FormField
      name={name}
      label={label}
      component={SelectField}
      fullWidth={fullWidth}
      disabled={disabled}
      validate={validate}
      colClassName={colClassName || 'col-sm-12'}
      floatingLabelFixed={floatingLabelFixed}
      style={props.style}
      hint={hint}
    >
      {(options && options.length) >= 1 ? options.map((option, index) => {
        return <MenuItem
          onTouchTap={() => onFieldSelect && onFieldSelect(option[valueField])}
          key={index} value={option[valueField]}
          primaryText={useFieldAsMessageId ?
            <FormattedMessage id={option[textField]} /> : option[textField]}
        />;
      }) :
        <MenuItem
          onTouchTap={() => { }}
          key={-1}
          value=""
          primaryText=""
        />}
    </FormField>
  );

};

export default selectField;
