import * as React from 'react';
import { default as ContentAdd } from 'material-ui/svg-icons/content/add';
import FormTitleWithButtons from '../form/FormTitleWithButtons';
import FloatingButtonWithToolTip from './FloatingButtonWithToolTip';

interface ItemsListTitleProps {
  onNewClick: () => void;
  title: string;
  createTooltip: string;
  hideCreateButton?: boolean;

}

const itemsListTitle: React.StatelessComponent<ItemsListTitleProps> = (props) => {
  const { onNewClick, title, createTooltip, hideCreateButton } = props;
  return (
    <FormTitleWithButtons title={title}>
      {!hideCreateButton && <FloatingButtonWithToolTip
        style={{ display: hideCreateButton ? 'none' : undefined }}
        tooltipId={createTooltip}
        secondary={true}
        onMouseDown={onNewClick}>
        <ContentAdd />}
      </FloatingButtonWithToolTip>}
    </FormTitleWithButtons>
  );
};

export default itemsListTitle;
