import * as React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

export interface CenterCircularProgressProps {
  mode?: 'determinate' | 'indeterminate';
}

const centerCircularProgress: React.StatelessComponent<CenterCircularProgressProps> = (props) => {
  const { mode } = props;
  return (
    <div style={{ width: '0%', margin: 'auto' }}>
      <CircularProgress mode={mode} />
    </div>
  );
};

centerCircularProgress.defaultProps = {
  mode: 'indeterminate',
};

export default centerCircularProgress;
