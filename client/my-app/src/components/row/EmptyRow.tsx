import * as React from 'react';

export interface EmptyRowProps {
}

export const EmptyRow: React.StatelessComponent<EmptyRowProps> = props => {
    return (
        <div className="row form-group" />
    );
};
