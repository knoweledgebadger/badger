import * as React from 'react';
import * as moment from 'moment';
import { FormattedMessage } from 'react-intl';
import { DatePicker as MaterialDatePicker } from 'material-ui';
import { DatePicker as ReduxFormMaterialUIDatePicker } from 'redux-form-material-ui';

export interface DatePickerProps {
  onChange: (date: moment.Moment) => void;
  date: string | moment.Moment;
  formatDate?: (date: Date) => string;
  style?: React.CSSProperties;
  textFieldStyle?: React.CSSProperties;
  hintText?: React.ReactNode;
  floatingLabelText?: React.ReactNode;
  name?: string;
  allowWeekends?: boolean;
  fullWidth?: boolean;
}
const disableWeekends = (day: Date) => {
  const weekday = moment(day).weekday();
  return weekday === 5 || weekday === 6;
};

export const datePickerOld: React.StatelessComponent<DatePickerProps> = (props) => {
  const { date, onChange, formatDate, style, textFieldStyle, hintText, floatingLabelText,
    name, allowWeekends, fullWidth } = props;

  const handleChange = (event: Event, selectedDate: Date) => {
    onChange(moment(selectedDate));
  };

  return (
    <MaterialDatePicker
      DateTimeFormat={global.Intl.DateTimeFormat}
      hintText={hintText ? hintText : 'datePicker'}
      cancelLabel={<FormattedMessage id="cancel" />}
      locale="lt"
      shouldDisableDate={(day: Date) => (allowWeekends) ? false : disableWeekends(day)}
      autoOk={true}
      value={moment(date).toDate()}
      onChange={handleChange}
      formatDate={formatDate}
      style={style}
      textFieldStyle={textFieldStyle}
      floatingLabelText={floatingLabelText}
      name={name}
      fullWidth={fullWidth}
    />
  );
};

// tslint:disable-next-line
export const DatePicker: React.StatelessComponent<any> = props => {
  const { hintText, value, allowWeekends, fullWidth, ...rest } = props;
  const componentProps = {
    DateTimeFormat: global.Intl.DateTimeFormat,
    hintText: hintText ? <FormattedMessage id={hintText} /> : 'datePicker',
    cancelLabel: <FormattedMessage id="cancel" />,
    locale: 'lt',
    shouldDisableDate: (day: Date) => (allowWeekends) ? false : disableWeekends(day),
    autoOk: true,
    value: moment(props.input.value).toDate(),
    fullWidth: fullWidth || false,
    ...rest,
  };
  return (
    <ReduxFormMaterialUIDatePicker  {...componentProps} />
  );
};

export default datePickerOld;
