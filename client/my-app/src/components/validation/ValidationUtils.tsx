import * as React from 'react';
import { FormattedMessage } from 'react-intl';

export const required = (value: string) => {
  return (!value ? <FormattedMessage id="required" /> : undefined);
};
