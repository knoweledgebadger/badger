import * as React from 'react';
import { default as EchartsForReact } from 'echarts-for-react';

export interface EchartProps {
  option: {};
  style: React.CSSProperties;
  zoomActive?: boolean;
}

class Echart extends React.Component<EchartProps, {}> {
  private echart: any;

  componentDidMount() {
    if (this.props.zoomActive) {
      this.echart.getEchartsInstance().dispatchAction({
        type: 'takeGlobalCursor',
        key: 'dataZoomSelect',
        dataZoomSelectActive: true,
      });
    }
  }

  render() {
    const { option, style } = this.props;
    return (
      <EchartsForReact
        option={option}
        ref={(c: any) => { this.echart = c; }}
        style={style}
      />
    );
  }
}

export default Echart;
