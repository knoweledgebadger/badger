import * as jwtDecode from 'jwt-decode';
import * as moment from 'moment';

class Auth {

  static authenticateUser(token: string) {
    localStorage.setItem('token', token);
  }

  static isUserAuthenticated() {
    const expired = this.isTokenExpired();
    if (expired) {
      this.deauthenticateUser();
    }
    return !expired && this.getToken();
  }

  static isTokenExpired() {
    const token = this.getJWT();
    if (token !== null && token.exp) {
      return moment.duration(moment(token.exp * 1000).diff(moment())).asMilliseconds() < 5000;
    } else {
      return false;
    }
  }

  static deauthenticateUser() {
    localStorage.removeItem('token');
  }

  static getToken() {
    return localStorage.getItem('token');
  }

  static getJWT(): JWTFields {
    const token = this.getToken();
    if (token !== null) {
      return jwtDecode(token);
    } else {
      return {};
    }
  }

  static saveUsername(username: string) {
    localStorage.setItem('un', username);
  }

  static savePassword(password: string) {
    localStorage.setItem('pwd', password);
  }

  static getUsername() {
    const un = localStorage.getItem('un');
    return un ? un : '';
  }

  static getPassword() {
    const pwd = localStorage.getItem('pwd');
    return pwd ? pwd : '';
  }

  static isAdmin() {
    const permissions = this.getJWT().permissions;
    return permissions != null && (permissions.indexOf('ADMIN') >= 0);
  }

}

interface JWTFields {
  id?: number;
  exp?: number;
  language?: string;
  permissions?: string;
  name?: string;
  surname?: string;
  clientId?: number;
  namespace?: string;
}

export default Auth;
