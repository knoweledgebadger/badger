import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { User } from '../Model';
import { updateUser, usersQuery } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../../modules/main/snackbarReducer';
import UserForm from '../components/UserForm';
import BasicFormLayout from '../../../modules/main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  refetch: Function;
  updateUser: (user: User) => Promise<User>;
  user: User;
  users: User[];
}

export interface UserEditPanelProps extends RouteComponentProps<{ id: string }> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & UserEditPanelProps;

class UserEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.updateUser = this.updateUser.bind(this);
    this.getPath = this.getPath.bind(this);
  }

  updateUser(user: User) {
    const cleanObject = _.cloneDeep(user);
    delete (cleanObject as any).__typename;
    delete cleanObject.createdAt;
    delete cleanObject.updatedAt;
    delete (cleanObject.vartotojas as any).__typename;

    this.props.updateUser(cleanObject).then(() => {
      this.props.displaySnackbar('user.updated', 'success');
      this.props.refetch();
      this.props.push(this.getPath());
    });
  }

  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/users/${this.props.user.id}`;
  }

  render() {
    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('UserForm')}
            onCancel={() => this.props.push(this.getPath())}
            title="user.modify.title"
          />}
        body={
          <UserForm
            onSubmit={this.updateUser}
            initialValues={this.props.user}
            users={this.props.users}
            hidePassword={true}
          />}
      />
    );
  }
}

const findUser = (id: number, users: User[]) => {
  if (id && users && users.length > 0) {
    return Object.assign({}, users.find((user: User) => user.id === id));
  } else {
    return undefined;
  }
};

const mapStateToProps = (state: State, ownProps: UserEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withData = graphql<{ users: User[] }, ComponentProps>(usersQuery, {
  props: (props) => {
    const { data, ownProps } = props;
    const { refetch, loading, users } = data!;
    const { id } = ownProps.match.params;
    return {
      refetch,
      loading,
      users,
      user: findUser(parseInt(id, 10), users),
    };
  },
});

const withMutations = graphql<{}, ComponentProps>(updateUser, {
  props: (props) => {
    const { mutate } = props;
    return {
      updateUser: (user: User) => mutate!({ variables: { user } }),
    };
  },
});

const userEditPanel: React.ComponentClass<UserEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(UserEditPanel)));

export default userEditPanel;
