import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import * as _ from 'lodash';
import { usersQuery, deleteUser } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { User } from '../Model';
import UsersFilter from '../components/UsersFilter';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

interface StateProps {
  filter: string;
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}

interface GraphqlProps {
  refetch: Function;
  deleteUser: (id: number) => Promise<User>;
  users: User[];
  loading: boolean;
}

export interface UsersPanelProps extends RouteComponentProps<{}> { }

type ComponentProps = StateProps & DispatchProps & UsersPanelProps & GraphqlProps;

class UsersPanel extends React.Component<ComponentProps, {}> {

  constructor() {
    super();
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }

  onDelete(user: User) {
    this.props.deleteUser(user.id!).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('user.removed', 'success');
    });
  }

  onEdit(user: User) {
    this.props.push(`/settings/users/${user.id}/edit`, { prevPath: this.props.location.pathname });
  }

  render() {
    const { push, users, loading } = this.props;
    return (
      <FilterFormLayout
        title={
          <ItemsListTitle
            onNewClick={() => push('/settings/users/new')}
            title="users.title"
            createTooltip="users.create.tooltip"
          />}
        filter={<UsersFilter />}
        body={(
          <ItemsList
            items={users}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            onClick={(user: User) => push(`/settings/users/${user.id}`)}
            deleteMessage="user.delete.confirm"
            primaryTextProperty={['name', 'surname']}
            loading={loading}
          />
        )}
      />
    );
  }
}

const filterUsers = (users: User[], filter: string) => {
  if (users) {
    const filtered = users.filter((user: User) => {
      return (new RegExp(filter!, 'i').test(user.name)) ||
        (new RegExp(filter!, 'i').test(user.surname!));
    });
    return _.sortBy(filtered, (user: User) => user.name.toLowerCase());
  }
  return [];
};

const mapStateToProps = (state: State, ownProps: UsersPanelProps) => {
  return {
    filter: _.get<string>(state.form, 'UsersFilter.values.userFullname'),
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(deleteUser, {
  props: (props) => {
    const { mutate } = props;
    return {
      deleteUser: (id: number) => mutate!({ variables: { id } }),
    };
  },
});

const withData = graphql<{ users: User[] }, ComponentProps>(usersQuery, {
  props: (props) => {
    const { data, ownProps } = props;
    const { refetch, loading, users } = data!;
    const filter = ownProps.filter;
    return {
      refetch,
      loading,
      users: filterUsers(users, filter),
    };
  },
});

const usersPanel: React.ComponentClass<UsersPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(UsersPanel)));

export default usersPanel;
