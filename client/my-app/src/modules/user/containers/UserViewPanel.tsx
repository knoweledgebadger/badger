import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { User } from '../Model';
import { State } from '../../../rootReducer';
import { usersQuery } from '../graphql';
import BasicFormLayout from '../../../modules/main/components/BasicFormLayout';
import UserView from '../components/UserView';
import UserViewTitle from '../components/UserViewTitle';
import Auth from '../../../Auth';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
}

interface GraphqlProps {
  user: User;
}

export interface UserViewPanelProps extends RouteComponentProps<{ id: string }> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & UserViewPanelProps;

class UserViewPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
  }

  onEdit() {
    const { push, user } = this.props;
    push(`/settings/users/${user.id}/edit`, { prevPath: this.props.location.pathname });
  }

  onChangePassword() {
    const { push, user } = this.props;
    push(`/settings/users/${user.id}/changePassword`, { prevPath: this.props.location.pathname });
  }

  render() {
    const { user } = this.props;
    return (
      <BasicFormLayout
        title={
          <UserViewTitle
            hideEditButton={!Auth.isAdmin()}
            onEdit={this.onEdit}
            onChangePassword={this.onChangePassword}
            title={_.get(user, 'fk_vartotojas')}
          />}
        body={<UserView user={user} />}
      />
    );
  }
}

const findUser = (id: number, users: User[]) => {
  if (id && users && users.length > 0) {
    return Object.assign({}, users.find((user: User) => user.id === id));
  } else {
    return undefined;
  }
};

const mapStateToProps = (state: State, ownProps: UserViewPanelProps) => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
  };
};

const withData = graphql<{ users: User[] }, ComponentProps>(usersQuery, {
  props: (props) => {
    const { data, ownProps } = props;
    const { refetch, loading, users } = data!;
    const { id } = ownProps.match.params;
    return {
      refetch,
      loading,
      users,
      user: findUser(parseInt(id, 10), users),
    };
  },
});

const userViewPanel: React.ComponentClass<UserViewPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(UserViewPanel));

export default userViewPanel;
