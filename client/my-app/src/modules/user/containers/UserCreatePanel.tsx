import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { User } from '../Model';
import { createUser, usersQuery } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../../modules/main/snackbarReducer';
import UserForm from '../components/UserForm';
import BasicFormLayout from '../../../modules/main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  refetch: Function;
  createUser: (user: User) => Promise<User>;
  users: User[];
}

export interface UserCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & UserCreatePanelProps;

class UserCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.createUser = this.createUser.bind(this);
  }

  createUser(user: User) {
    this.props.createUser(user).then(() => {
      this.props.displaySnackbar('user.created', 'success');
      this.props.refetch();
      this.props.push('/settings/users');
    });
  }

  render() {
    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('UserForm')}
            onCancel={() => this.props.push('/settings/users')}
            title="user.modify.title"
          />}
        body={<UserForm onSubmit={this.createUser} users={this.props.users} />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: UserCreatePanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withData = graphql<{ users: User[] }, ComponentProps>(usersQuery, {
  props: (props) => {
    const { data } = props;
    const { refetch, loading, users } = data!;
    return {
      refetch,
      loading,
      users,
    };
  },
});

const withMutations = graphql<{}, ComponentProps>(createUser, {
  props: (props) => {
    const { mutate } = props;
    return {
      createUser: (user: User) => mutate!({
        variables: { user },
      }),
    };
  },
});

const userCreatePanel: React.ComponentClass<UserCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(UserCreatePanel)));

export default userCreatePanel;
