import * as React from 'react';
import { default as EditorModeEdit } from 'material-ui/svg-icons/editor/mode-edit';
import { default as ActionLock } from 'material-ui/svg-icons/action/lock';
import FormTitleWithButtons from '../../../components/form/FormTitleWithButtons';
import FloatingButtonWithToolTip from '../../../components/material/FloatingButtonWithToolTip';

interface UserViewTitleProps {
  onEdit: () => void;
  onChangePassword: () => void;
  title: string;
  hideEditButton?: boolean;
}

const userViewTitle: React.StatelessComponent<UserViewTitleProps> = (props) => {
  const { onEdit, onChangePassword, title, hideEditButton } = props;
  return (
    <FormTitleWithButtons title={title} formatTitle={false}>
      {!hideEditButton &&
        <FloatingButtonWithToolTip
          tooltipId="user.view.edit"
          onMouseDown={onEdit}>
          <EditorModeEdit />
        </FloatingButtonWithToolTip>}
      <span className="space" />
      <FloatingButtonWithToolTip
        tooltipId="user.view.change.password"
        secondary={true}
        onMouseDown={onChangePassword}>
        <ActionLock />
      </FloatingButtonWithToolTip>
    </FormTitleWithButtons>
  );

};

userViewTitle.defaultProps = {
  hideEditButton: false,
};

export default userViewTitle;
