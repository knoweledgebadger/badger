import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Validator } from 'redux-form';
import FormSelectField from '../../../components/material/FormSelectField';

export interface RoleSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
}

const roleSelectField: React.StatelessComponent<RoleSelectFieldProps> = (props) => {
  const { name, validate, value } = props;
  return (
    <FormSelectField
      value={value}
      options={[
        { value: 'USER', label: <FormattedMessage id="user.role.USER" /> },
        { value: 'ADMIN', label: <FormattedMessage id="user.role.ADMIN" /> },
      ]}
      textField="label"
      valueField="value"
      label="user.role"
      fullWidth={true}
      name={name}
      validate={validate}
    />
  );
};

export default roleSelectField;
