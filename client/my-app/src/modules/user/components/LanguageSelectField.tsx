import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { Validator } from 'redux-form';
import FormSelectField from '../../../components/material/FormSelectField';

export interface LanguageSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
  style?: React.CSSProperties;
}

const languageSelectField: React.StatelessComponent<LanguageSelectFieldProps> = (props) => {
  const { name, validate, value } = props;
  return (
    <FormSelectField
      value={value}
      options={[
        { value: 'EE', label: <FormattedMessage id="user.language.EE" /> },
        { value: 'EN', label: <FormattedMessage id="user.language.EN" /> },
        { value: 'ES', label: <FormattedMessage id="user.language.ES" /> },
        { value: 'LT', label: <FormattedMessage id="user.language.LT" /> },
        { value: 'LV', label: <FormattedMessage id="user.language.LV" /> },
        { value: 'PL', label: <FormattedMessage id="user.language.PL" /> },
        { value: 'RU', label: <FormattedMessage id="user.language.RU" /> },
      ]}
      textField="label"
      valueField="value"
      label="user.language.select"
      fullWidth={true}
      name={name}
      validate={validate}
      style={props.style}
    />
  );
};

export default languageSelectField;
