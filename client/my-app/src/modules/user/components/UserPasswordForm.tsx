import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { required } from '../../../components/validation/ValidationUtils';
import FormField from '../../../components/form/FormField';
import { Password } from '../Model';

export interface UserPasswordFormProps {
  onSubmit: (password: Password) => void;
  oldPasswordRequired?: boolean;
}

export interface UserPasswordFormModel {
  oldPassword: string;
  newPassword1: string;
  newPassword2: string;
}

type ComponentProps =
  UserPasswordFormProps & FormProps<UserPasswordFormModel, UserPasswordFormProps, State>;

class UserPasswordForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
    this.passwordsMatch = this.passwordsMatch.bind(this);
  }

  passwordsMatch(value: string, allValues: Object[]) {
    return (value !== allValues['newPassword1']
      ? <FormattedMessage id="user.password.change.mismatch" />
      : undefined);
  }

  render() {
    const { handleSubmit, oldPasswordRequired } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        {oldPasswordRequired &&
          <FormField
            name="oldPassword"
            type="password"
            label="user.password.old"
            validate={required}
          />}
        <FormField
          name="newPassword1"
          type="password"
          label="user.password.new1"
          validate={required}
        />
        <FormField
          name="newPassword2"
          type="password"
          label="user.password.new2"
          validate={[required, this.passwordsMatch]}
        />
      </form>
    );
  }
}

export default reduxForm<UserPasswordFormModel, UserPasswordFormProps, State>({
  form: 'UserPasswordForm',
})(UserPasswordForm);
