import { gql } from 'react-apollo';

export const usersQuery = gql`
  query users {
      users {
          id
          name
          surname
          role
          email
          username
          language
          fk_vartotojas
          vartotojas {
            vardas
            pseudonimas
            slaptazodis
            el_pastas
            lytis
            sukurimo_data
            bendras_ivertis
            avataras
            role_id
          }
      }
  }
`;

export const createUser = gql`
  mutation createUser($user: UserInput!) {
    createUser(user: $user) {
      id
      name
      surname
      role
      email
      username
      language
      fk_vartotojas
      vartotojas {
        vardas
        pseudonimas
        slaptazodis
        el_pastas
        lytis
        sukurimo_data
        bendras_ivertis
        avataras
        role_id
      }
    }
  }
`;

export const deleteUser = gql`
  mutation deleteUser($id: Int!) {
    deleteUser(id: $id) {
      id
    }
  }
`;

export const updateUser = gql`
  mutation updateUser($user: UserInput!) {
    updateUser(user: $user) {
      id
      name
      surname
      role
      email
      username
      language
      fk_vartotojas
      vartotojas {
        vardas
        pseudonimas
        slaptazodis
        el_pastas
        lytis
        sukurimo_data
        bendras_ivertis
        avataras
        role_id
      }
    }
  }
`;

export const changePassword = gql`
  mutation changePassword($userId: Int!, $oldPassword: String, $newPassword: String!) {
    changePassword(userId: $userId, oldPassword: $oldPassword, newPassword: $newPassword) {
      success
    }
  }
`;
