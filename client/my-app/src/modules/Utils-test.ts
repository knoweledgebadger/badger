import * as moment from 'moment';
import { Utils } from './Utils';
import { expect } from 'chai';
import 'mocha';

describe('Hello function', () => {
  it('should return hello world', () => {
    const result = Utils.changeFieldDateType({ test: moment() }, ['test']);
    expect(result).to.not.equal('');
  });
});
