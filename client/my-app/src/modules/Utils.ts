import * as moment from 'moment';

export class Utils {
  // tslint:disable-next-line
  static changeFieldsDateType(array: any[], fields: string[]) {
    array.forEach((o) => {
      this.changeFieldDateType(o, fields);
    });
    return array;
  }

  // tslint:disable-next-line
  static changeFieldDateType(obj: any, fields: string[]) {
    fields.forEach((field) => {
      obj[field] = moment(obj[field]);
    });
    return obj;
  }
}
