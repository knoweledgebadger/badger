import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Diskusija } from '../model';
import { updateDiskusija } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import DiskusijaForm from '../components/DiskusijaForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  updateDiskusija: (diskusija: Diskusija) => Promise<Diskusija>;
  diskusija: Diskusija;
}

export interface DiskusijaEditPanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & DiskusijaEditPanelProps;

class DiskusijaEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.updateDiskusija = this.updateDiskusija.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  updateDiskusija(diskusija: Diskusija) {
    const cleanObject = _.cloneDeep(diskusija);
    delete (cleanObject as any).__typename;
    console.log(cleanObject);
    return this.props.updateDiskusija(cleanObject).then(() => {
      this.props.displaySnackbar('diskusija.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.editFailed', 'danger') });
  }
  getPath() {
    return `/forum`;
  }

  render() {
    const { diskusija } = this.props;
    console.log('edit panel', diskusija);

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('DiskusijaForm')}
            onCancel={this.handleclose}
            title="diskusija.edit.title"
          />}
        body={
          <DiskusijaForm
            onSubmit={this.updateDiskusija}
            initialValues={_.get<Diskusija>(this.props.location.state, 'diskusija')}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: DiskusijaEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(updateDiskusija, {
  props: (props) => {
    const { mutate } = props;
    return {
      updateDiskusija: (diskusija: Diskusija) => mutate!({
        variables: { diskusija },
      }),
    };
  },
});

const diskusijaEditPanel: React.ComponentClass<DiskusijaEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(DiskusijaEditPanel));

export default diskusijaEditPanel;
