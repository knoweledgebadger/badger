// paimportinti reikilingi dalykai
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import { diskusijosQuery, deleteDiskusija } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Diskusija } from '../model';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';
import Paieska from '../components/Paieska';

// propsai routinimui ir žinutes atvaizdavimui
interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}
// propsai querinimui / mutationams
interface GraphqlProps {
  refetch: Function;
  deleteDiskusija: (id: number) => Promise<Diskusija>;
  diskusijos: Diskusija[];
  loading: boolean;
}
// propsas padariantis kad komeponentas būtų route komponentas
export interface TemosPanelProps extends RouteComponentProps<{}> { }
// sujungiami i viena visi propsai
type ComponentProps = DispatchProps & TemosPanelProps & GraphqlProps;

//  temu peržiuros panel komponentas su sujungtais propsais
class TemosPanel extends React.Component<ComponentProps, {}> {
  // konstruktorius
  constructor() {
    super();
    // užbindimani viduje naudojami metodai, tam kad butu galima naudoti pačio komponento variablus
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onView = this.onView.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }
  // kas vyks pasaudus diskusijos delete mygtuka
  onDelete(diskusija: Diskusija) {
    // naudojamas iš graphql gautas metoda deleteDiskusija
    this.props.deleteDiskusija(diskusija.id!).then(() => {
      // pasibaigus trinimui refetchinama
      this.props.refetch();
      // parodoma žinutė , kad diskusija sekmingai ištrinta
      this.props.displaySnackbar('diskusija.removed', 'success');
      // jei nepavyksta ištrinti diskusijos žinutė kad nepavyko
    }).catch(error => this.props.displaySnackbar('diskusija.remove.failed', 'danger'));
  }
  // ka daryti jei paspaudžiamas edit mygtukas arba paspaudžiama ant diskusijos elemento
  onEdit(diskusija: Diskusija) {
    // pushinamas edit panelis su  jam nurodyta diskusija, diskusijos id
    this.props.push(`/diskusija/${diskusija.id}/edit`, { diskusija });
  }
  onView(diskusija: Diskusija) {
    // pushinamas edit panelis su  jam nurodyta diskusija, diskusijos id
    this.props.push(`/diskusija/${diskusija.id}/view`, { diskusija });
  }
  onSearch(paieska: any) {
    this.props.refetch({ paieska: paieska.tekstas, fetchPolicy: 'network-only' });
  }
  // render metodas kvieciamas kaskart pasikeitus propsas, pvz kai užkrauna diskusijas
  render() {
    const { push, diskusijos, loading } = this.props;
    // returninamas elementas
    return (
      <FilterFormLayout
        title={
          <div className="col-12">
            <ItemsListTitle
              onNewClick={() => push('/diskusija/new')}
              title="diskusija.title"
              createTooltip="diskusija.create.tooltip"
            />
            <Paieska onSubmit={this.onSearch} />
          </div>}
        filter={<div />}
        body={(
          <ItemsList
            items={diskusijos}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            onClick={this.onView}
            deleteMessage="diskusija.delete.confirm"
            primaryTextProperty={['aprasymas']}
            secondaryTextProperty={['tekstas']}
            loading={loading}
          />
        )}
      />
    );
  }
}
// šiuo atveju nenaudojamas redux metodas naudingas sudetingesniuose psl
const mapStateToProps = (state: State, ownProps: TemosPanelProps) => {
  return {
  };
};
// sumapinami push ir žinutės atvaizdavimo metodai
const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};
// aprašomas mutationas su metodu, ir kas deletinama
const withMutations = graphql<{}, ComponentProps>(deleteDiskusija, {
  props: (props) => {
    const { mutate } = props;
    return {
      deleteDiskusija: (id: number) => mutate!({ variables: { id } }),
    };
  },
});
// aprašomas queris partraukiantis visas diskusijas ir į ką viską sudėti , šiuo atveju
// return metodas į props numes fukciją refetch, boola loading, ir duomenis diskusijos
// optionsai network-only reikalingi kad neužsicashintu listas ir pvz sukurus nauja diskusija
// ji iskart atsivaizduotu liste
const withData = graphql<{ diskusijos: Diskusija[] }, ComponentProps>(diskusijosQuery, {
  options: {
    fetchPolicy: 'network-only',
    variables: { search: undefined },
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, diskusijos } = data!;
    return {
      refetch,
      loading,
      diskusijos,
    };
  },
});

// viskas sujungiama į vieną komponentą
const diskusijosPanel: React.ComponentClass<TemosPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(TemosPanel)));
// exportinamas komponentas
export default diskusijosPanel;
