import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Diskusija } from '../model';
import { createDiskusija } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import DiskusijaForm from '../components/DiskusijaForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';
import Auth from '../../../Auth';
interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  createDiskusija: (diskusija: Diskusija) => Promise<Diskusija>;
}

export interface DiskusijaCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & DiskusijaCreatePanelProps;

class DiskusijaCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.createDiskusija = this.createDiskusija.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  createDiskusija(diskusija: Diskusija) {
    console.log(diskusija);
    return this.props.createDiskusija(diskusija).then(() => {
      this.props.displaySnackbar('diskusija.created', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.createFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/forum`;
  }

  render() {

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('DiskusijaForm')}
            onCancel={this.handleclose}
            title="diskusija.create.title"
          />}
        body={
          <DiskusijaForm
            initialValues={{ userId: Auth.getJWT().id }}
            onSubmit={this.createDiskusija}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: DiskusijaCreatePanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(createDiskusija, {
  props: (props) => {
    const { mutate } = props;
    return {
      createDiskusija: (diskusija: Diskusija) => mutate!({
        variables: { diskusija },
      }),
    };
  },
});

const diskusijaCreatePanel: React.ComponentClass<DiskusijaCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(DiskusijaCreatePanel));

export default diskusijaCreatePanel;
