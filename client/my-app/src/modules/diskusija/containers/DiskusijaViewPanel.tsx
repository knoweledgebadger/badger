import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Diskusija, DiskusijosKomentaras } from '../model';
import { getDiskusionData, editKomentaras, createKomentaras } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicViewPanelTitle from '../../../components/form/BasicViewPanelTitle';
import KomentaraiList from '../components/KomentaraiList';
import Auth from '../../../Auth';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  updateDiskusija: (diskusija: Diskusija) => Promise<Diskusija>;
  editKomentaras: (komentaras: DiskusijosKomentaras) => Promise<DiskusijosKomentaras>;
  createKomentaras:
  (komentaras: DiskusijosKomentaras, diskusijosId: number,
  ) => Promise<DiskusijosKomentaras>;
  refetch: Function;
  diskusija: Diskusija;
}

export interface DiskusijaEditPanelProps extends RouteComponentProps<{ id: number }> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & DiskusijaEditPanelProps;

class DiskusijaEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.updateDiskusija = this.updateDiskusija.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
    this.createKomentaras = this.createKomentaras.bind(this);
    this.editKomentaras = this.editKomentaras.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  updateDiskusija(diskusija: Diskusija) {
    const cleanObject = _.cloneDeep(diskusija);
    delete (cleanObject as any).__typename;
    return this.props.updateDiskusija(cleanObject).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('diskusija.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.editFailed', 'danger') });
  }
  editKomentaras(komentaras: DiskusijosKomentaras) {
    const cleanObject = _.cloneDeep(komentaras);
    delete (cleanObject as any).__typename;
    return this.props.editKomentaras(cleanObject).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('komentaras.edited', 'success');
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.editFailed', 'danger') });
  }
  createKomentaras(komentaras: DiskusijosKomentaras) {
    const cleanObject = _.cloneDeep(komentaras);
    delete (cleanObject as any).__typename;
    return this.props.createKomentaras(cleanObject, this.props.diskusija.id!).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('komentaras.created', 'success');
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.editFailed', 'danger') });
  }
  getPath() {
    return `/forum`;
  }

  render() {
    const { diskusija } = this.props;
    console.log(diskusija);
    return (
      <BasicFormLayout
        title={
          <BasicViewPanelTitle
            usePlainMessage={true}
            onCancel={this.handleclose}
            title={diskusija ? diskusija.aprasymas! : 'undefined'}
          />}
        body={!diskusija ? <div /> :
          <div>
            <div className="box box-default">
              {diskusija ? 'Kūrėjas : ' + diskusija.user.username : undefined}
            </div>

            {diskusija ? diskusija.tekstas : undefined}
            <div className="box-divider" style={{ marginTop: 15, marginBottom: 15 }} />
            <KomentaraiList
              onEdit={this.editKomentaras}
              onCreate={this.createKomentaras}
              komentarai={diskusija.komentaras!}
              dabartinioVartotojoId={Auth.getJWT().id!}
              isMod={Auth.isAdmin()} />
          </div>
        }
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: DiskusijaEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};
const withEdit = graphql<{}, ComponentProps>(editKomentaras, {
  props: (props) => {
    const { mutate } = props;
    return {
      editKomentaras: (komentaras: DiskusijosKomentaras) => mutate!({
        variables: { komentaras },
      }),
    };
  },
});
const withCreate = graphql<{}, ComponentProps>(createKomentaras, {
  props: (props) => {
    const { mutate } = props;
    return {
      createKomentaras: (komentaras: DiskusijosKomentaras, diskusijosId: number) => mutate!({
        variables: { komentaras, diskusijosId },
      }),
    };
  },
});
const withData = graphql<{ diskusija: Diskusija }, ComponentProps>(getDiskusionData, {
  options: (ownProps) => {
    return {
      fetchPolicy: 'network-only',
      variables: { id: ownProps.match.params.id },
    };
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, diskusija } = data!;
    return {
      refetch,
      loading,
      diskusija,
    };
  },
});

const diskusijaEditPanel: React.ComponentClass<DiskusijaEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withCreate(withEdit(withData(DiskusijaEditPanel))));

export default diskusijaEditPanel;
