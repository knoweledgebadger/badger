// fronte naudojamas modelis (TypeScript)
import { User } from '../user/Model';
export interface Diskusija {
  id?: number;
  tekstas?: string;
  aprasymas?: string;
  sukurimoData?: string;
  userId?: number;
  user: User;
  temaId?: number;
  komentaras?: DiskusijosKomentaras[];
}

export interface DiskusijosKomentaras {
  id?: number;
  tekstas?: string;
  data?: string;
  user?: User;
}
