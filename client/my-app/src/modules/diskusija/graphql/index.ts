import { gql } from 'react-apollo';
// fronte naudojami queriai ir ką partraukti
const deleteDiskusija = gql`
    mutation deleteDiskusija($id: Int!) {
      deleteDiskusija(id: $id) {
        id
      }
    }
`;

const createDiskusija = gql`
    mutation createDiskusija($diskusija: DiskusijaInput!) {
      createDiskusija(diskusija: $diskusija) {
        id
      }
    }
`;

const diskusijosQuery = gql`
    query diskusijos($paieska: String) {
        diskusijos(paieska: $paieska) {
            id
            tekstas
            aprasymas
            sukurimoData
            userId
            temaId
            komentaras {
                id
                tekstas
                userId
            }
        }
    }
`;

const updateDiskusija = gql`
mutation updateDiskusija($diskusija: DiskusijaInput!) {
    updateDiskusija(diskusija: $diskusija) {
        id
        tekstas
        aprasymas
        sukurimoData
        userId
        temaId
  }
}
`;
const editKomentaras = gql`
mutation editKomentaras($komentaras: DiskusijosKomentarasInput!) {
    editKomentaras(komentaras: $komentaras) {
        id
  }
}
`;
const createKomentaras = gql`
mutation createKomentaras($komentaras: DiskusijosKomentarasInput!, $diskusijosId: Int!) {
    createKomentaras(komentaras: $komentaras, diskusijosId: $diskusijosId) {
        id
  }
}
`;

const getDiskusionData = gql`
query diskusija($id: Int!) {
    diskusija(id: $id) {
        id
        tekstas
        aprasymas
        sukurimoData
        user {
            username
        }
        komentaras {
            id
            tekstas
            user {
                username
            }
        }
  }
}
`;

export {
    diskusijosQuery, updateDiskusija, createDiskusija, deleteDiskusija, getDiskusionData,
    editKomentaras, createKomentaras,
};
