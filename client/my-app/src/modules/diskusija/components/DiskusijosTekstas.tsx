import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Diskusija } from '../model';
import { getDiskusionData } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicViewPanelTitle from '../../../components/form/BasicViewPanelTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  updateDiskusija: (diskusija: Diskusija) => Promise<Diskusija>;
  diskusija: Diskusija;
}

export interface DiskusijaEditPanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & DiskusijaEditPanelProps;

class DiskusijaEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.updateDiskusija = this.updateDiskusija.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  updateDiskusija(diskusija: Diskusija) {
    const cleanObject = _.cloneDeep(diskusija);
    delete (cleanObject as any).__typename;
    return this.props.updateDiskusija(cleanObject).then(() => {
      this.props.displaySnackbar('diskusija.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('diskusija.editFailed', 'danger') });
  }
  getPath() {
    return `/forum`;
  }

  render() {
    const diskusija = _.get<Diskusija>(this.props.location.state, 'diskusija');
    return (
      <BasicFormLayout
        title={
          <BasicViewPanelTitle
            usePlainMessage={true}
            onCancel={this.handleclose}
            title={diskusija ? diskusija.aprasymas! : 'undefined'}
          />}
        body={
          <div className="box box-default">
            {diskusija.id}
            {diskusija ? diskusija.tekstas : undefined}
          </div>
        }
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: DiskusijaEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withData = graphql<{ diskusija: Diskusija }, ComponentProps>(getDiskusionData, {
  options: (ownProps) => {
    return {
      fetchPolicy: 'network-only',
      variables: { id: ownProps.diskusija.id },
    };
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, diskusija } = data!;
    console.log(diskusija);
    return {
      refetch,
      loading,
      diskusija,
    };
  },
});

const diskusijaEditPanel: React.ComponentClass<DiskusijaEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(DiskusijaEditPanel));

export default diskusijaEditPanel;
