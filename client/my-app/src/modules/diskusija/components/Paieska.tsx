import * as React from 'react';
import { injectIntl, InjectedIntlProps } from 'react-intl';
import { TextField } from 'redux-form-material-ui';
import FloatingActionButton from 'material-ui/FloatingActionButton';
// tslint:disable-next-line
import Search from 'material-ui/svg-icons/action/search';
import { State } from '../../../rootReducer';

import FormField from '../../../components/form/FormField';
import { reduxForm, FormProps } from 'redux-form';
import { required } from '../../../components/validation/ValidationUtils';

interface SingleKomentarasFormProps {
  // tslint:disable-next-line
  onSubmit: (event: any) => void;
}

interface SingleKomentarasFormState {
}

export interface SingleKomentarasFormModel {
  tekstas?: string;
}
type ComponentProps = SingleKomentarasFormProps & InjectedIntlProps &
  FormProps<SingleKomentarasFormModel, SingleKomentarasFormProps, State>;
class Paieska extends React.Component<ComponentProps, SingleKomentarasFormState> {
  constructor(props: ComponentProps) {
    super(props);

  }
  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div className="row form-group" style={{ paddingTop: 50, paddingBottom: 13 }}>
          <FormField
            component={TextField}
            label={'diskusija.paieska'}
            name="tekstas"
            fullWidth={true}
            validate={required}
            colClassName={'col-4'}
          />
          <div className="col-2" style={{ top: 15 }}>
            <FloatingActionButton
              onMouseDown={this.props.handleSubmit}
              secondary={false}
              mini={true}>
              <Search />
            </FloatingActionButton>
          </div>
        </div>
      </form>
    );
  }
}
export default reduxForm<SingleKomentarasFormModel,
  SingleKomentarasFormProps>({ form: 'KomentarasForm' })(injectIntl(Paieska));
