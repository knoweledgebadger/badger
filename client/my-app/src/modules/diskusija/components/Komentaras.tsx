import * as React from 'react';
import { injectIntl, InjectedIntlProps } from 'react-intl';
import { TextField } from 'redux-form-material-ui';
import FloatingActionButton from 'material-ui/FloatingActionButton';
// tslint:disable-next-line
import ContentDone from 'material-ui/svg-icons/action/done';
// tslint:disable-next-line
import ContentClear from 'material-ui/svg-icons/content/clear';
import { State } from '../../../rootReducer';

import FormField from '../../../components/form/FormField';
import { reduxForm, FormProps } from 'redux-form';
import { required } from '../../../components/validation/ValidationUtils';

interface SingleKomentarasFormProps {
  // tslint:disable-next-line
  onSubmit: (event: any) => void;
  onCancel: (event: any) => void;
}

interface SingleKomentarasFormState {
  editingId?: number;
}

export interface SingleKomentarasFormModel {
  tekstas?: string;
}
type ComponentProps = SingleKomentarasFormProps & InjectedIntlProps &
  FormProps<SingleKomentarasFormModel, SingleKomentarasFormProps, State>;
class SingleKomentarasForm extends React.Component<ComponentProps, SingleKomentarasFormState> {
  constructor(props: ComponentProps) {
    super(props);

  }
  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div className="row form-group" style={{ paddingTop: 10, paddingBottom: 13 }}>
          <div className="col-10" >
            <div className="col-12">
              <FormField
                component={TextField}
                label={'komentaras.enter'}
                name="tekstas"
                fullWidth={true}
                validate={required}
              />
            </div>
          </div>
          <div className="col-2">
            <div className="btn-toolbar float-right">
              <FloatingActionButton
                onMouseDown={this.props.handleSubmit}
                secondary={true}
                mini={true}>
                <ContentDone />
              </FloatingActionButton>
              <span className="space" />
              <FloatingActionButton
                type="reset"
                mini={true}
                onMouseDown={this.props.onCancel}>
                <ContentClear />
              </FloatingActionButton>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
export default reduxForm<SingleKomentarasFormModel,
  SingleKomentarasFormProps>({ form: 'KomentarasForm' })(injectIntl(SingleKomentarasForm));
