import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { Diskusija } from '../model';
import FormField from '../../../components/form/FormField';
import TemaSelectField from '../../tema/components/TemaSelectField';

export interface DiskusijaFormProps {
  onSubmit: (tema: Diskusija) => void;
  isViewOnly?: boolean;
}

export interface DiskusijaFormModel {
  id?: number;
  tekstas?: string;
  aprasymas?: string;
  temaId?: number;
  userId?: number;
}

type ComponentProps = DiskusijaFormProps & FormProps<DiskusijaFormModel, DiskusijaFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isViewOnly } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit}>
        <TemaSelectField
          disabled={isViewOnly}
          name="temaId" />
        <FormField disabled={isViewOnly} name="aprasymas" label="diskusija.aprasymas" />
        <FormField
          disabled={isViewOnly}
          floatingLabelFixed={true}
          name="tekstas" label="diskusija.tekstas"
          multiLine={true}
          rows={5} />

      </form>
    );
  }
}

export default compose(
  reduxForm<DiskusijaFormModel, DiskusijaFormProps, State>({ form: 'DiskusijaForm' }),
)(TransportForm);
