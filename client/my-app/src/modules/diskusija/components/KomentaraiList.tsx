import * as React from 'react';
import * as _ from 'lodash';
import { injectIntl, InjectedIntlProps, FormattedMessage } from 'react-intl';
import FloatingButtonWithToolTip from '../../../components/material/FloatingButtonWithToolTip';
import IconButton from 'material-ui/IconButton';
// tslint:disable-next-line
import ContentAdd from 'material-ui/svg-icons/content/add';
import { ListItem, List } from 'material-ui/List';

import { DiskusijosKomentaras } from '../model';
import { User } from '../../user/Model';
// tslint:disable-next-line
import EditIcon from 'material-ui/svg-icons/image/edit';

import Komentaras from './Komentaras';

interface KomentarasFormProps {
  komentarai: DiskusijosKomentaras[];
  dabartinioVartotojoId: number;
  isMod: boolean;
  onEdit: (komentaras: DiskusijosKomentaras) => void;
  onCreate: (komentaras: DiskusijosKomentaras) => void;
}

interface KomentarasFormState {
  creating: boolean;
  editingId?: number;
}

class KomentarasForm extends
  React.Component<KomentarasFormProps & InjectedIntlProps, KomentarasFormState> {
  constructor(props: KomentarasFormProps & InjectedIntlProps) {
    super(props);
    this.state = { creating: false };
    this.handleEdit = this.handleEdit.bind(this);
    this.handleAccept = this.handleAccept.bind(this);
    this.onCreateClick = this.onCreateClick.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.canEditComment = this.canEditComment.bind(this);
  }

  componentWillReceiveProps(nextProps: KomentarasFormProps) {
    this.setState({ creating: false });
  }

  // tslint:disable-next-line
  handleEdit(event: any, item: DiskusijosKomentaras) {
    console.log('handleEDit');
    this.setState({ editingId: item.id });
  }

  // tslint:disable-next-line
  handleAccept(komentaras: any) {

    const { editingId } = this.state;
    if (komentaras.tekstas && komentaras.tekstas.length >= 1) {
      if (editingId) {
        this.props.onEdit({ id: editingId, tekstas: komentaras.tekstas });
        this.setState({ creating: false, editingId: undefined });
      } else {
        this.props.onCreate({ tekstas: komentaras.tekstas });
        this.setState({ creating: false, editingId: undefined });
      }
    }

  }

  onCreateClick() {
    this.setState({ creating: true });
  }

  // tslint:disable-next-line
  onCancel(event: any) {
    event.preventDefault();
    this.setState({ creating: false, editingId: undefined });
  }

  renderUser(user: User) {
    return <p>{user.name + ' ' + user.surname}</p>;
  }
  canEditComment = (komentaras: DiskusijosKomentaras) => {
    const { isMod, dabartinioVartotojoId } = this.props;
    return isMod || (_.get<number>(komentaras, 'user.id') === dabartinioVartotojoId);
  }
  render() {
    const { intl, komentarai } = this.props;
    const { creating, editingId } = this.state;
    const element = <div className="box-body">
      <FormattedMessage id="komentaras.title" />
      <List>
        {komentarai.map((komentaras: DiskusijosKomentaras, index: number) => {
          return (<div key={'id' + komentaras.id} className="box box-default col-12" >
            {!(editingId === komentarai[index].id) ? <ListItem
              key={'id' + komentaras.id}
              disabled={true}
              primaryText={
                <div style={{ wordBreak: 'break-word' }}>
                  {komentaras.tekstas}
                </div>}
              secondaryText={
                <div style={{ wordBreak: 'break-word' }}>
                  {intl!.formatMessage({ id: 'komentaras.kūrėjas' })
                    + _.get<string>(komentaras, 'user.username')}
                </div>}
              rightIconButton={
                this.canEditComment(komentaras) ?
                  <IconButton
                    style={{ marginRight: 5 }}
                    iconStyle={{ width: '20px', height: '20px' }}
                    tooltip={<FormattedMessage id="komentaras.edit" />}
                    // tslint:disable-next-line
                    onTouchTap={((event: any) => this.handleEdit(event, komentaras)).bind(this)}>
                    <EditIcon />
                  </IconButton> : <div />
              }>
            </ListItem> :
              <Komentaras
                onSubmit={this.handleAccept}
                onCancel={this.onCancel}
                initialValues={{ tekstas: komentarai[index].tekstas }} />}
          </div>);

        })}
      </List >
      {(!editingId)
        && <div>{(!creating && !editingId) ? (
          <div className="btn-group float-right">
            <div className="col-3">
              <FloatingButtonWithToolTip
                secondary={true}
                tooltipId={'komentaras.create'}
                onMouseDown={this.onCreateClick}>
                <ContentAdd />
              </FloatingButtonWithToolTip>
            </div>
          </div>) : (
            <Komentaras
              onSubmit={this.handleAccept}
              onCancel={this.onCancel} />
          )
        }
        </div>
      }
    </div >;
    return element;
  }
}

export default injectIntl(KomentarasForm);
