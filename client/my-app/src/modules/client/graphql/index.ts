import { gql } from 'react-apollo';

export const clientQuery = gql`
  query client($id: Int!) {
    client(id: $id) {
      id
      name
      namespace
    }
  }
`;

export const createClient = gql`
  mutation createClient($client: ClientInput!) {
    createClient(client: $client) {
      id
      name
      namespace
    }
  }
`;

export const deleteClient = gql`
  mutation deleteClient($id: Int!) {
    deleteClient(id: $id) {
      id
    }
  }
`;

export const updateClient = gql`
  mutation updateClient($client: ClientInput!) {
    updateClient(client: $client) {
      id
      name
      namespace
    }
  }
`;
