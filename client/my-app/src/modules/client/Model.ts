export interface Client {
  id?: number;
  name: string;
  namespace: string;
}
