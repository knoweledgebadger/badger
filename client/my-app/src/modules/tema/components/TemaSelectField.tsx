import * as React from 'react';
import { graphql } from 'react-apollo';
import { Validator } from 'redux-form';
import { temosQuery } from '../graphql';
import FormSelectField from '../../../components/material/FormSelectField';
import { Tema } from '../model';

export interface TemaSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
  colClassName?: string;
  onFieldSelect?: (value: Tema) => void;
  hideLabel?: boolean;
  floatingLabelFixed?: boolean;
  valueField?: string;
  onlyProducts?: boolean;
  disabled?: boolean;
  style?: any;
}

interface GraphqlProps {
  temos: Tema[];
}

type ComponentProps = TemaSelectFieldProps & GraphqlProps;

class TemaSelectField extends React.Component<ComponentProps, {}> {
  static defaultProps = {
    onlyProducts: false,
    colClassName: 'col-sm-12',
    label: 'tema',
  };
  render() {
    const {
      value, temos, name, validate, colClassName, disabled,
      onFieldSelect, hideLabel, floatingLabelFixed, valueField } = this.props;
    const label = hideLabel ? undefined : 'tema.select';
    return (
      <FormSelectField
        value={value}
        options={temos}
        onFieldSelect={onFieldSelect}
        disabled={disabled}
        style={{ cursor: 'default' }}
        textField="pavadinimas"
        valueField={valueField || 'id'}
        label={label}
        fullWidth={true}
        name={name}
        validate={validate}
        colClassName={colClassName}
        floatingLabelFixed={floatingLabelFixed}
      />
    );
  }
}

const withData = graphql<{ temos: Tema[] }, TemaSelectFieldProps>(
  temosQuery,
  {
    options: (props) => {
      return {
        variables: { onlyProducts: props.onlyProducts },
        fetchPolicy: 'network-only',
      };
    },
    props: ({ data: { temos }, ownProps }: any) => {
      return {
        temos,
      };
    },
  });

export default withData(TemaSelectField);
