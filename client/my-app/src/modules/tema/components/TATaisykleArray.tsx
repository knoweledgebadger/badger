import * as React from 'react';
import { FieldArray } from 'redux-form';
import TATaisykleList from './TATaisykleList';

export interface SegmentFieldsArrayProps {
  identificationNo?: string;
}

class SegmentFieldsArray extends React.Component<SegmentFieldsArrayProps, {}> {
  render() {
    const { identificationNo } = this.props;
    return (
      <FieldArray
        name="temosAdminTaisykle"
        component={TATaisykleList}
        identificationNo={identificationNo}
      />
    );
  }
}

export default SegmentFieldsArray;
