import * as React from 'react';
import FormField from '../../../components/form/FormField';

interface SegmentProps {
  index: number;
  name: string;
}

const tATaisykle: React.StatelessComponent<SegmentProps> = (props) => {
  const { index, name } = props;
  return (
    <div className="row">
      <div className="col-xl-5 col-lg-6 col-md-8 col-10">
        <div className="row">
          <FormField
            name={`${name}.pavadinimas`}
            colClassName="col-6"
            label={index === 0 ? 'tema.taisykle.pav' : undefined}
            floatingLabelFixed={true}
          />
          <FormField
            colClassName="col-6"
            name={`${name}.aprasymas`}
            label={index === 0 ? 'tema.taisykle.ap' : undefined}
            floatingLabelFixed={true}
          />
        </div>
      </div>
    </div>
  );
};
export default tATaisykle;
