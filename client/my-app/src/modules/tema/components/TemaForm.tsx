import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { Tema, TemosAdminTaisykle } from '../model';
import TATaisykleArray from './TATaisykleArray';
import FormField from '../../../components/form/FormField';

export interface TemaFormProps {
  onSubmit: (tema: Tema) => void;
}

export interface TemaFormModel {
  id?: number;
  pavadinimas?: string;
  aprasymas?: string;
  temosAdminTaisykle?: TemosAdminTaisykle[];
}

type ComponentProps = TemaFormProps & FormProps<TemaFormModel, TemaFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormField name="pavadinimas" label="tema.pavadinimas" />
        <FormField name="aprasymas" label="tema.aprasymas" />
        <TATaisykleArray />
      </form>
    );
  }
}

export default compose(
  reduxForm<TemaFormModel, TemaFormProps, State>({ form: 'TemaForm' }),
)(TransportForm);
