import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { TemosAdminTaisykle } from '../model';
import { WrappedFieldArrayProps } from 'redux-form';
import { IconButton } from 'material-ui';
import { default as ContentAdd } from 'material-ui/svg-icons/content/add';
import TATaisykle from './TATaisykle';

interface TATaisykleProps extends WrappedFieldArrayProps<TemosAdminTaisykle> {
}

const tATaisykleList: React.StatelessComponent<TATaisykleProps> = (props) => {
  const { fields } = props;
  return (
    <div className="col-12">
      <div />
      <FormattedMessage id="tema.temosAdminTaisykles" />
      <div />
      {fields && fields.map((name, index) => {
        return (
          <TATaisykle
            key={index}
            index={index}
            name={name}
          />
        );
      })}

      <IconButton
        tooltip={<FormattedMessage id="tema.taisykle.add" />}
        tooltipPosition="top-left"
        onTouchTap={() => fields.push({})}
      >
        <ContentAdd />
      </IconButton>
    </div>
  );
};

export default tATaisykleList;
