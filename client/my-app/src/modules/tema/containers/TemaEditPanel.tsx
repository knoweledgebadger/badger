import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Tema } from '../model';
import { updateTema } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import TemaForm from '../components/TemaForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  updateTema: (tema: Tema) => Promise<Tema>;
  tema: Tema;
}

export interface TemaEditPanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & TemaEditPanelProps;

class TemaEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.updateTema = this.updateTema.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  updateTema(tema: Tema) {
    const cleanObject = _.cloneDeep(tema);
    delete (cleanObject as any).__typename;
    if (cleanObject.temosAdminTaisykle)
      cleanObject.temosAdminTaisykle.map((tais) => {
        delete (tais as any).__typename;
        return tais;
      });
    console.log(cleanObject);
    return this.props.updateTema(cleanObject).then(() => {
      this.props.displaySnackbar('tema.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('tema.editFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/tema`;
  }

  render() {
    const { tema } = this.props;
    console.log('edit panel', tema);

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('TemaForm')}
            onCancel={this.handleclose}
            title="tema.edit.title"
          />}
        body={
          <TemaForm
            onSubmit={this.updateTema}
            initialValues={_.get<Tema>(this.props.location.state, 'tema')}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: TemaEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(updateTema, {
  props: (props) => {
    const { mutate } = props;
    return {
      updateTema: (tema: Tema) => mutate!({
        variables: { tema },
      }),
    };
  },
});

const temaEditPanel: React.ComponentClass<TemaEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(TemaEditPanel));

export default temaEditPanel;
