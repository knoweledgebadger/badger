import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Tema } from '../model';
import { createTema } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import TemaForm from '../components/TemaForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  createTema: (tema: Tema) => Promise<Tema>;
}

export interface TemaCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & TemaCreatePanelProps;

class TemaCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.createTema = this.createTema.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  createTema(tema: Tema) {
    return this.props.createTema(tema).then(() => {
      this.props.displaySnackbar('tema.created', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('tema.createFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/tema`;
  }

  render() {

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('TemaForm')}
            onCancel={this.handleclose}
            title="tema.create.title"
          />}
        body={
          <TemaForm
            onSubmit={this.createTema}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: TemaCreatePanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(createTema, {
  props: (props) => {
    const { mutate } = props;
    return {
      createTema: (tema: Tema) => mutate!({
        variables: { tema },
      }),
    };
  },
});

const temaCreatePanel: React.ComponentClass<TemaCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(TemaCreatePanel));

export default temaCreatePanel;
