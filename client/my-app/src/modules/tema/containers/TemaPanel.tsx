// paimportinti reikilingi dalykai
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import { temosQuery, deleteTema } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Tema } from '../model';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

// propsai routinimui ir žinutes atvaizdavimui
interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}
// propsai querinimui / mutationams
interface GraphqlProps {
  refetch: Function;
  deleteTema: (id: number) => Promise<Tema>;
  temos: Tema[];
  loading: boolean;
}
// propsas padariantis kad komeponentas būtų route komponentas
export interface TemosPanelProps extends RouteComponentProps<{}> { }
// sujungiami i viena visi propsai
type ComponentProps = DispatchProps & TemosPanelProps & GraphqlProps;

//  temu peržiuros panel komponentas su sujungtais propsais
class TemosPanel extends React.Component<ComponentProps, {}> {
  // konstruktorius
  constructor() {
    super();
    // užbindimani viduje naudojami metodai, tam kad butu galima naudoti pačio komponento variablus
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }
  // kas vyks pasaudus temos delete mygtuka
  onDelete(tema: Tema) {
    // naudojamas iš graphql gautas metoda deleteTema
    this.props.deleteTema(tema.id!).then(() => {
      // pasibaigus trinimui refetchinama
      this.props.refetch();
      // parodoma žinutė , kad tema sekmingai ištrinta
      this.props.displaySnackbar('tema.removed', 'success');
      // jei nepavyksta ištrinti temos žinutė kad nepavyko
    }).catch(error => this.props.displaySnackbar('tema.remove.failed', 'danger'));
  }
  // ka daryti jei paspaudžiamas edit mygtukas arba paspaudžiama ant temos elemento
  onEdit(tema: Tema) {
    // pushinamas edit panelis su  jam nurodyta tema, temos id
    this.props.push(`/settings/tema/${tema.id}/edit`, { tema });
  }
  // render metodas kvieciamas kaskart pasikeitus propsas, pvz kai užkrauna temas
  render() {
    // tam kad visur nerašyti this.props.push arba thsi.props.temos tiesiog persimetama į konstatas
    const { push, temos, loading } = this.props;
    // returninamas elementas

    return (
      <FilterFormLayout
        title={
          <ItemsListTitle
            onNewClick={() => push('/settings/tema/new')}
            title="tema.title"
            createTooltip="tema.create.tooltip"
          />}
        filter={<div />}
        body={(
          <ItemsList
            items={temos}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            onClick={this.onEdit}
            deleteMessage="tema.delete.confirm"
            primaryTextProperty={['pavadinimas']}
            loading={loading}
          />
        )}
      />
    );
  }
}
// šiuo atveju nenaudojamas redux metodas naudingas sudetingesniuose psl
const mapStateToProps = (state: State, ownProps: TemosPanelProps) => {
  return {
  };
};
// sumapinami push ir žinutės atvaizdavimo metodai
const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};
// aprašomas mutationas su metodu, ir kas deletinama
const withMutations = graphql<{}, ComponentProps>(deleteTema, {
  props: (props) => {
    const { mutate } = props;
    return {
      deleteTema: (id: number) => mutate!({ variables: { id } }),
    };
  },
});
// aprašomas queris partraukiantis visas temas ir į ką viską sudėti , šiuo atveju
// return metodas į props numes fukciją refetch, boola loading, ir duomenis temos
// optionsai network-only reikalingi kad neužsicashintu listas ir pvz sukurus nauja tema 
// ji iskart atsivaizduotu liste
const withData = graphql<{ temos: Tema[] }, ComponentProps>(temosQuery, {
  options: {
    fetchPolicy: 'network-only',
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, temos } = data!;
    return {
      refetch,
      loading,
      temos,
    };
  },
});

// viskas sujungiama į vieną komponentą
const temosPanel: React.ComponentClass<TemosPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(TemosPanel)));
// exportinamas komponentas
export default temosPanel;
