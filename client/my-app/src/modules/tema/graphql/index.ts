import { gql } from 'react-apollo';
// fronte naudojami queriai ir ką partraukti
const deleteTema = gql`
    mutation deleteTema($id: Int!) {
      deleteTema(id: $id) {
        id
      }
    }
`;

const createTema = gql`
    mutation createTema($tema: TemaInput!) {
      createTema(tema: $tema) {
        id
        pavadinimas
        aprasymas
        temosAdminTaisykle {
            id
            pavadinimas
            aprasymas
        }
      }
    }
`;

const temosQuery = gql`
    query temos {
        temos {
            id
            pavadinimas
            aprasymas
            temosAdminTaisykle {
                id
                pavadinimas
                aprasymas
            }
        }
    }
`;

const updateTema = gql`
mutation updateTema($tema: TemaInput!) {
    updateTema(tema: $tema) {
    id
    pavadinimas
    aprasymas
    temosAdminTaisykle {
        id
        pavadinimas
        aprasymas
    }
  }
}
`;

export {
    temosQuery, updateTema, createTema, deleteTema,
};
