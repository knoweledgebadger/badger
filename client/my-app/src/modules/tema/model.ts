// fronte naudojamas modelis (TypeScript)

export interface Tema {
  id?: number;
  pavadinimas?: string;
  aprasymas?: string;
  temosAdminTaisykle?: TemosAdminTaisykle[];
}

export interface TemosAdminTaisykle {
  id?: number;
  pavadinimas?: string;
  aprasymas?: string;
}
