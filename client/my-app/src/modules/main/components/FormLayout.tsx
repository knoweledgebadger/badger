import * as React from 'react';
import * as QueueAnim from 'rc-queue-anim';
import Article from '../../../components/material/Article';

export interface BasicFormLayoutProps {
  title?: React.ReactNode;
}

const formLayout: React.StatelessComponent<BasicFormLayoutProps> = (props) => {
  const { title, children } = props;

  return (
    <Article hideTitle={true}>
      <QueueAnim type="bottom" className="ui-animate">
        <div className="row" key={1}>
          <div className="col-12">
            <div className="article-title">
              {title}
            </div>
          </div>
        </div>
        <div className="row" key={2}>
          <div className="col-lg-12">
            {children}
          </div>
        </div>
      </QueueAnim>
    </Article>
  );
};

export default formLayout;
