import * as React from 'react';
import Snackbar from 'material-ui/Snackbar';
import { FormattedMessage } from 'react-intl';
import Box from '../../../components/material/Box';

export interface SnackbarProps {
  open: boolean;
  message: string;
  onRequestClose?: (reason: string) => void;
  bg: 'danger' | 'success' | 'info';
}

const snackbar: React.StatelessComponent<SnackbarProps> = (props) => {
  const { open, message, onRequestClose, bg } = props;
  return (
    <Snackbar
      open={open}
      message={(
        <Box
          className={`bg-color-${bg}`}
          body={message && <FormattedMessage id={message} />}
          bodyStyle={{ paddingTop: '0px' }}
        />
      )}
      autoHideDuration={4000}
      bodyStyle={{ paddingLeft: '0px', paddingRight: '0px', backgroundColor: '' }}
      onRequestClose={onRequestClose}
    />
  );
};

export default snackbar;
