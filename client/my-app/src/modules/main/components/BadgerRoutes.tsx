import * as React from 'react';
import { Route, Switch } from 'react-router';
import UsersPanel from '../../user/containers/UsersPanel';
import UserCreatePanel from '../../user/containers/UserCreatePanel';
import UserEditPanel from '../../user/containers/UserEditPanel';
import UserViewPanel from '../../user/containers/UserViewPanel';
import UserPasswordPanel from '../../user/containers/UserPasswordPanel';

import TemaPanel from '../../tema/containers/TemaPanel';
import TemaCreatePanel from '../../tema/containers/TemaCreatePanel';
import TemaEditPanel from '../../tema/containers/TemaEditPanel';
import DiskusijosPanel from '../../diskusija/containers/DiskusijosPanel';
import DiskusijaEditPanel from '../../diskusija/containers/DiskusijaEditPanel';
import DiskusijaCreatePanel from '../../diskusija/containers/DiskusijaCreatePanel';
import DiskusijaViewPanel from '../../diskusija/containers/DiskusijaViewPanel';

import InfoPanel from '../../aprasymas/containers/InfoPanel';
import InfoCreatePanel from '../../aprasymas/containers/InfoCreatePanel';
import InfoEditPanel from '../../aprasymas/containers/InfoEditPanel';
import InfosPanel from '../../aprasymas/containers/InfosPanel';
import InfoViewPanel from '../../aprasymas/containers/InfoViewPanel';

import VartotojasPanel from '../../vartotojas/containers/VartotojasPanel';
import VartotojasEditPanel from '../../vartotojas/containers/VartotojasEditPanel';
import VartotojasCreatePanel from '../../vartotojas/containers/VartotojasCreatePanel';

interface BadgerRoutesProps {
  matchUrl: string;
}

const badgerRoutes: React.StatelessComponent<BadgerRoutesProps> = (props) => {
  const { matchUrl } = props;
  return (
    <Switch>
      <Route path={matchUrl + 'settings/users/:id/edit'} component={UserEditPanel} />
      <Route path={matchUrl + 'settings/users/:id/changePassword'} component={UserPasswordPanel} />
      <Route path={matchUrl + 'settings/users/new'} component={UserCreatePanel} />
      <Route path={matchUrl + 'settings/users/:id'} component={UserViewPanel} />
      <Route path={matchUrl + 'settings/users'} component={UsersPanel} />

      <Route path={matchUrl + 'settings/tema/:id/edit'} component={TemaEditPanel} />
      <Route path={matchUrl + 'settings/tema/new'} component={TemaCreatePanel} />
      <Route path={matchUrl + 'settings/tema'} component={TemaPanel} />

      <Route path={matchUrl + 'settings/aprasymas/:id/edit'} component={InfoEditPanel} />
      <Route path={matchUrl + 'settings/aprasymas/new'} component={InfoCreatePanel} />
      <Route path={matchUrl + 'settings/aprasymas'} component={InfoPanel} />

      <Route path={matchUrl + 'diskusija/:id/edit'} component={DiskusijaEditPanel} />
      <Route path={matchUrl + 'diskusija/:id/view'} component={DiskusijaViewPanel} />
      <Route path={matchUrl + 'diskusija/new'} component={DiskusijaCreatePanel} />
      <Route path={matchUrl + 'forum'} component={DiskusijosPanel} />

      <Route path={matchUrl + 'settings/vartotojas/new'} component={VartotojasCreatePanel} />
      <Route path={matchUrl + 'settings/vartotojas/:id/edit'} component={VartotojasEditPanel} />
      <Route path={matchUrl + 'settings/vartotojas'} component={VartotojasPanel} />

      <Route path={matchUrl + 'aprasymai/:id/view'} component={InfoViewPanel} />
      <Route path={matchUrl + 'aprasymai'} component={InfosPanel} />
    </Switch>
  );
};

export default badgerRoutes;
