import * as React from 'react';
import * as QueueAnim from 'rc-queue-anim';
import Article from '../../../components/material/Article';
import Box from '../../../components/material/Box';

export interface FilterFormLayoutProps {
  body?: React.ReactNode;
  filter?: React.ReactNode;
  title?: React.ReactNode;
}

const filterFormLayout: React.StatelessComponent<FilterFormLayoutProps> = (props) => {
  const { body, title, filter } = props;

  return (
    <Article hideTitle={true}>
      <QueueAnim type="bottom" className="ui-animate">
        <div className="row" key={1}>
          <div className="col-12">
            <div className="article-title">
              {title}
            </div>
          </div>
        </div>
        <div className="row" key={2}>
          <div className="col-lg-12">
            {filter}
          </div>
        </div>
        <div className="row" key={3}>
          <div className="col-lg-12">
            <Box body={body} />
          </div>
        </div>
      </QueueAnim>
    </Article>
  );
};

export default filterFormLayout;
