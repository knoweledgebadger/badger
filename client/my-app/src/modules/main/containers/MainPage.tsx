import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import SideBar from '../../sidenav/containers/SideBar';
import { bindActionCreators, Dispatch } from 'redux';
import { hideSnackbar, HideSnackbar } from '../snackbarReducer';
import { State } from '../../../rootReducer';
import { User } from '../../user/Model';
import Auth from '../../../Auth';
import AppHeader from '../../header/containers/AppHeader';
import Snackbar from '../components/Snackbar';
import MainRoutes from '../components/MainRoutes';

interface StateProps {
  currentUser?: User;
  navCollapsed: boolean;
  snackbarOpen: boolean;
  snackbarMessage: string;
  snackbarBg: 'danger' | 'success' | 'info';
}

interface DispatchProps {
  hideSnackbar: HideSnackbar;
  push: typeof push;
}

interface GraphqlProps {
  refetch: Function;
  loading: boolean;
}

export interface MainPageProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & MainPageProps & GraphqlProps;

class MainPageComponent extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if (!Auth.isUserAuthenticated()) {
      this.props.push('/login', { nextPathname: this.props.location.pathname });
    }
  }

  render() {
    const collapsed = this.props.navCollapsed ? 'nav-collapsed' : '';
    const { match, location } = this.props;
    const brand = <FormattedMessage id="appName" />;
    return (
      <div id="app-inner">
        <div className="preloaderbar hide"><span className="bar" /></div>
        <div className={`full-height fixed-header sidebar-md nav-behind theme-light ${collapsed}`}>
          <Snackbar
            open={this.props.snackbarOpen}
            message={this.props.snackbarMessage}
            onRequestClose={this.props.hideSnackbar}
            bg={this.props.snackbarBg}
          />
          <div className="main-app-container">
            <SideBar
              onBrand={() => { }}
              path={location.pathname}
              brand={brand} />

            <section id="page-container" className="app-page-container">
              <AppHeader brand={brand} onBrand={() => { }} />

              <div className="app-content-wrapper">
                <div className="app-content">
                  <div className="full-height">
                    <div className={'container-fluid no-breadcumbs page-dashboard chapter'}>
                      <MainRoutes matchUrl={match.url} />
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div >
    );
  }
}

const mapStateToProps = (state: State, ownProps: MainPageProps): StateProps => {
  return {
    navCollapsed: state.sideNavReducer.navCollapsed,
    snackbarOpen: state.snackbarReducer.open,
    snackbarMessage: state.snackbarReducer.message,
    snackbarBg: state.snackbarReducer.bg,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
  bindActionCreators({ hideSnackbar, push }, dispatch);

const mainPage: React.ComponentClass<MainPageProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainPageComponent);

export default mainPage;
