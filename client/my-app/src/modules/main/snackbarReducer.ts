// Actions
export const DISPLAY_SNACKBAR = 'DISPLAY_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';

// Reducer
interface SnackbarAction {
  type: string;
  message?: string;
  bg?: 'danger' | 'success' | 'info';
}

export interface SnackbarReducerState {
  open: boolean;
  message: string;
  bg: 'danger' | 'success' | 'info';
}

const initialState: SnackbarReducerState = {
  open: false,
  message: '',
  bg: 'info',
};

export const snackbarReducer =
  (state: SnackbarReducerState = initialState, action: SnackbarAction): SnackbarReducerState => {
    switch (action.type) {
      case DISPLAY_SNACKBAR:
        return {
          message: action.message!,
          open: true,
          bg: action.bg ? action.bg : 'info',
        };
      case HIDE_SNACKBAR:
        return {
          message: '',
          open: false,
          bg: 'info',
        };
      default:
        return state;
    }
  };

// Action Creators
export interface DisplaySnackbar {
  (message: string, bg?: 'danger' | 'success' | 'info'): (dispatch: Function) => void;
}

export const displaySnackbar: DisplaySnackbar = (message, bg) => {
  return (dispatch: Function) => {
    dispatch({ message, bg, type: DISPLAY_SNACKBAR });
  };
};

export interface HideSnackbar {
  (): (dispatch: Function) => void;
}

export const hideSnackbar: HideSnackbar = () => {
  return (dispatch: Function) => {
    dispatch({ type: HIDE_SNACKBAR });
  };
};
