import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { logout, Logout } from '../../login/sessionReducer';
import { resetState, ResetState, State } from '../../../rootReducer';
import { Header } from '../components/Header';
import HeaderMenu from '../components/HeaderMenu';
import { HeaderMenuItem } from '../components/HeaderMenuItem';
import Auth from '../../../Auth';

interface StateProps {
}

interface DispatchProps {
  logout: Logout;
  resetState: ResetState;
}

interface AppHeaderProps {
  onBrand: () => void;
  brand: JSX.Element;
}

class AppHeaderComponent
  extends React.Component<StateProps & DispatchProps & AppHeaderProps, {}> {
  constructor() {
    super();
    this.logout = this.logout.bind(this);
  }

  logout() {
    this.props.resetState();
    this.props.logout();
  }

  render() {
    const jwt = Auth.getJWT();
    const fullName = jwt.name + ' ' + jwt.surname;
    return (
      <Header brand={this.props.brand} onBrand={this.props.onBrand}>
        <HeaderMenu label={fullName}>
          <HeaderMenuItem
            iconName="person_outline"
            message="user.profile"
            link={`/settings/users/${jwt.id}`}
          />
          <HeaderMenuItem iconName="forward" message="logout" onTouchTap={this.logout} />
        </HeaderMenu>
      </Header>
    );
  }
}

const mapStateToProps = (state: State, ownProps: AppHeaderProps): StateProps => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
  bindActionCreators({ logout, resetState }, dispatch);

const appHeader: React.ComponentClass<AppHeaderProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppHeaderComponent);

export default appHeader;
