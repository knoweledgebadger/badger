// fronte naudojamas modelis (TypeScript)

export interface Vartotojas {
  vardas?: string;
  pseudonimas?: string;
  el_pastas?: string;
  lytis?: string;
  sukurimo_data?: string;
  bendras_ivertis?: number;
  avataras?: string;
  role_id?: number;
}

export interface Role {
  id?: number;
  role?: string;
}