import { gql } from 'react-apollo';

const createVartotojas = gql`
    mutation createVartotojas($vartotojas: VartotojasInput!) {
      createVartotojas(vartotojas: $vartotojas) {
        vardas
        pseudonimas
        slaptazodis
        el_pastas
        lytis
        sukurimo_data
        bendras_ivertis
        avataras
        role_id
        privilegijos_id
        privilegijos{
            rasyti_temas
            rasyti_straipsnius
            redaguoti_straipsnius
            vertinti_vartotoja
            trinti_komentarus
            trinti_straipsnius
            trinti_temas
            kurti_aprasymus
            trinti_aprasymus
            redaguoti_aprasymus
        }
      }
    }
`;

const vartotojaiQuery = gql`
    query vartotojai {
        vartotojai {
            vardas
            pseudonimas
            slaptazodis
            el_pastas
            lytis
            sukurimo_data
            bendras_ivertis
            avataras
            role_id
            privilegijos_id
            privilegijos{
                rasyti_temas
                rasyti_straipsnius
                redaguoti_straipsnius
                vertinti_vartotoja
                trinti_komentarus
                trinti_straipsnius
                trinti_temas
                kurti_aprasymus
                trinti_aprasymus
                redaguoti_aprasymus
            }
        }
    }
`;

const rolesQuery = gql`
query role {
    role {
        id
        role
    }
}
`;

const editVartotojas = gql`
mutation editVartotojas($vartotojas: VartotojasInput!) {
  editVartotojas(vartotojas: $vartotojas) {
    vardas
    pseudonimas
    slaptazodis
    el_pastas
    lytis
    sukurimo_data
    bendras_ivertis
    avataras
    role_id
    privilegijos_id
    privilegijos{
        rasyti_temas
        rasyti_straipsnius
        redaguoti_straipsnius
        vertinti_vartotoja
        trinti_komentarus
        trinti_straipsnius
        trinti_temas
        kurti_aprasymus
        trinti_aprasymus
        redaguoti_aprasymus
    }
  }
}
`;

export {
    vartotojaiQuery, editVartotojas, createVartotojas, rolesQuery
};
