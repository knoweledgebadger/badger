// paimportinti reikilingi dalykai
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import { vartotojaiQuery } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Vartotojas } from '../model';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

// propsai routinimui ir žinutes atvaizdavimui
interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}
// propsai querinimui / mutationams
interface GraphqlProps {
  refetch: Function;
  vartotojai: Vartotojas[];
  loading: boolean;
}
// propsas padariantis kad komeponentas būtų route komponentas
export interface VartotojaiPanelProps extends RouteComponentProps<{}> { }
// sujungiami i viena visi propsai
type ComponentProps = DispatchProps & VartotojaiPanelProps & GraphqlProps;

//  temu peržiuros panel komponentas su sujungtais propsais
class VartotojaiPanel extends React.Component<ComponentProps, {}> {
  // konstruktorius
  constructor() {
    super();
    // užbindimani viduje naudojami metodai, tam kad butu galima naudoti pačio komponento variablus
    this.onEdit = this.onEdit.bind(this);
  }
  // ka daryti jei paspaudžiamas edit mygtukas arba paspaudžiama ant vartotojai elemento
  onEdit(vartotojas: Vartotojas) {
    // pushinamas edit panelis su  jam nurodyta vartotojas, vartotojai id
    this.props.push(`/settings/vartotojas/${vartotojas.pseudonimas}/edit`, { vartotojas });
  }
  // render metodas kvieciamas kaskart pasikeitus propsas, pvz kai užkrauna vartotojass
  render() {
    // tam kad visur nerašyti this.props.push arba thsi.props.vartotojai tiesiog persimetama į konstatas
    const { push, vartotojai, loading } = this.props;
    // returninamas elementas

    return (
      <FilterFormLayout
        title={
          <ItemsListTitle
            hideCreateButton={true}
            onNewClick={() => push('/settings/vartotojas/new')}
            title="Vartotojai"
            createTooltip="vartotojas.create.tooltip"
          />}
        filter={<div />}
        body={(
          <ItemsList
            items={vartotojai}
            onEdit={this.onEdit}
            onClick={this.onEdit}
            hideDelete={true}
            deleteMessage="vartotojas.delete.confirm"
            primaryTextProperty={['pseudonimas']}
            loading={loading}
          />
        )}
      />
    );
  }
}
// šiuo atveju nenaudojamas redux metodas naudingas sudetingesniuose psl
const mapStateToProps = (state: State, ownProps: VartotojaiPanelProps) => {
  return {
  };
};
// sumapinami push ir žinutės atvaizdavimo metodai
const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};
// aprašomas queris partraukiantis visas vartotojass ir į ką viską sudėti , šiuo atveju
// return metodas į props numes fukciją refetch, boola loading, ir duomenis vartotojai
// optionsai network-only reikalingi kad neužsicashintu listas ir pvz sukurus nauja vartotojas 
// ji iskart atsivaizduotu liste
const withData = graphql<{ vartotojai: Vartotojas[] }, ComponentProps>(vartotojaiQuery, {
  options: {
    fetchPolicy: 'network-only',
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, vartotojai } = data!;
    return {
      refetch,
      loading,
      vartotojai,
    };
  },
});

// viskas sujungiama į vieną komponentą
const vartotojaiPanel: React.ComponentClass<VartotojaiPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(VartotojaiPanel));
// exportinamas komponentas
export default vartotojaiPanel;
