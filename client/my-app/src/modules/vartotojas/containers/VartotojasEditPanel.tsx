import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Vartotojas } from '../model';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import VartotojasEditForm from '../components/VartotojasEditForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';
import { editVartotojas } from '../graphql';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  editVartotojas: (vartotojas: Vartotojas) => Promise<Vartotojas>;
  vartotojas: Vartotojas;
}

export interface VartotojasEditPanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & VartotojasEditPanelProps;

class VartotojasEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.editVartotojas = this.editVartotojas.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  editVartotojas(vartotojas: any) {
    const cleanObject = _.cloneDeep(vartotojas);
    delete (cleanObject as any).__typename;
    delete (cleanObject.privilegijos as any).__typename;
    console.log(cleanObject);
    return this.props.editVartotojas(cleanObject).then(() => {
      this.props.displaySnackbar('Sėkmingai atlikti vartotojo pakeitimai', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('vartotojas.editFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/vartotojas`;
  }

  render() {
    const { vartotojas } = this.props;
    console.log('edit panel', vartotojas);

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('VartotojasForm')}
            onCancel={this.handleclose}
            title="Vartotojo redagavimas"
          />}
        body={
          <VartotojasEditForm
            onSubmit={this.editVartotojas}
            initialValues={_.get<Vartotojas>(this.props.location.state, 'vartotojas')}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: VartotojasEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(editVartotojas, {
  props: (props) => {
    const { mutate } = props;
    return {
      editVartotojas: (vartotojas: Vartotojas) => mutate!({
        variables: { vartotojas },
      }),
    };
  },
});

const vartotojasEditPanel: React.ComponentClass<VartotojasEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(VartotojasEditPanel));

export default vartotojasEditPanel;
