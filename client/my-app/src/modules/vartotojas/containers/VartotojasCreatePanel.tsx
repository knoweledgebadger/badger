import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Vartotojas } from '../model';
import { createVartotojas } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import VartotojasForm from '../components/VartotojasForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
//import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  createVartotojas: (vartotojas: Vartotojas) => Promise<Vartotojas>;
}

export interface VartotojasCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & VartotojasCreatePanelProps;

class VartotojasCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.createVartotojas = this.createVartotojas.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  createVartotojas(vartotojas: Vartotojas) {
    return this.props.createVartotojas(vartotojas).then(() => {
      this.props.displaySnackbar('vartotojas.created', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('tema.createFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/vartotojas`;
  }

  render() {

    return (
      <div className="container">
        <h2> Registracija </h2>
        <BasicFormLayout
          body={

            <VartotojasForm onSubmit={this.createVartotojas} />


          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state: State, ownProps: VartotojasCreatePanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(createVartotojas, {
  props: (props) => {
    const { mutate } = props;
    return {
      createVartotojas: (vartotojas: Vartotojas) => mutate!({
        variables: { vartotojas },
      }),
    };
  },
});

const vartotojasCreatePanel: React.ComponentClass<VartotojasCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(VartotojasCreatePanel));

export default vartotojasCreatePanel;
