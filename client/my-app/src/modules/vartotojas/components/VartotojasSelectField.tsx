import * as React from 'react';
import { graphql } from 'react-apollo';
import { Validator } from 'redux-form';
import { rolesQuery } from '../graphql';
import FormSelectField from '../../../components/material/FormSelectField';
import { Role } from '../model';

export interface RoleSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
  colClassName?: string;
  onFieldSelect?: (value: Role) => void;
  hideLabel?: boolean;
  floatingLabelFixed?: boolean;
  valueField?: string;
  onlyProducts?: boolean;
  disabled?: boolean;
  style?: any;
}

interface GraphqlProps {
  role: Role[];
}

type ComponentProps = RoleSelectFieldProps & GraphqlProps;

class RoleSelectField extends React.Component<ComponentProps, {}> {
  static defaultProps = {
    onlyProducts: false,
    colClassName: 'col-sm-12',
    label: 'role',
  };
  render() {
    const {
      value, role, name, validate, colClassName, disabled,
      onFieldSelect, hideLabel, floatingLabelFixed, valueField } = this.props;
    const label = hideLabel ? undefined : 'Role'; console.log(role);
    return (
      <FormSelectField
        value={value}
        options={role}
        onFieldSelect={onFieldSelect}
        disabled={disabled}
        style={{ cursor: 'default' }}
        textField="role"
        valueField={valueField || 'id'}
        label={label}
        fullWidth={true}
        name={name}
        validate={validate}
        colClassName={colClassName}
        floatingLabelFixed={floatingLabelFixed}
      />
    );
  }
}

const withData = graphql<{ roles: Role[] }, RoleSelectFieldProps>(
  rolesQuery,
  {
    options: (props) => {
      return {
        variables: { onlyProducts: props.onlyProducts },
        fetchPolicy: 'network-only',
      };
    },
    props: ({ data: { role }, ownProps }: any) => {
      return {
        role,
      };
    },
  });

export default withData(RoleSelectField);
