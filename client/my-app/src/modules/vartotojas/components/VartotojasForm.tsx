import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { Vartotojas } from '../model';
import FormField from '../../../components/form/FormField';
import { required } from '../../../components/validation/ValidationUtils';
import { FlatButton } from 'material-ui';


export interface VartotojasFormProps {
  onSubmit: (tema: Vartotojas) => void;
  isViewOnly?: boolean;
}

export interface VartotojasFormModel {
  vardas?: string;
  pseudonimas?: string;
  slaptazodis?: string;
  el_pastas?: string;
  lytis?: string;
  sukurimo_data?: string;
  bendras_ivertis?: number;
  avataras?: string;
  role_id?: number;
}

type ComponentProps = VartotojasFormProps & FormProps<VartotojasFormModel, VartotojasFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isViewOnly } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormField name="pseudomimas" label="vartotojas.nick" validate={required} disabled={isViewOnly} />
        <FormField name="slaptazodis" label="vartotojas.password" validate={required} disabled={isViewOnly} />
        <FormField name="vardas" label="vartotojas.name" validate={required} disabled={isViewOnly} />
        <FormField name="el_pastas" label="vartotojas.email" validate={required} disabled={isViewOnly} />
        <FormField name="lytis" label="vartotojas.sex" disabled={isViewOnly} />
        {isViewOnly && <FormField name="sukurimo_data" label="vartotojas.creation_date" validate={required} disabled={true} />}
        {isViewOnly && <FormField name="bendras_ivertis" label="vartotojas.karma" validate={required} disabled={true} />}
        <FormField name="avataras" label="vartotojas.avatar" disabled={isViewOnly} />
        {isViewOnly && <FormField name="role_id" label="vartotojas.role" validate={required} disabled={true} />}


        <br /><br />
        <FlatButton
          label={'Registruotis'}
          primary={true}
          type="submit" />
      </form>
    );
  }
}

export default compose(
  reduxForm<VartotojasFormModel, VartotojasFormProps, State>({ form: 'VarototjasForm' }),
)(TransportForm);
