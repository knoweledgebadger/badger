import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { Vartotojas } from '../model';
import FormField from '../../../components/form/FormField';
import { Checkbox } from 'redux-form-material-ui';
//import { FormattedMessage } from 'react-intl';

import { required } from '../../../components/validation/ValidationUtils';
import VartotojasSelectField from './VartotojasSelectField';
//import { FlatButton } from 'material-ui';

export interface VartotojasFormProps {
  onSubmit: (vartotojas: Vartotojas) => void;
  isViewOnly?: boolean;
}

export interface VartotojasFormModel {
  vardas?: string;
  pseudonimas?: string;
  slaptazodis?: string;
  el_pastas?: string;
  lytis?: string;
  sukurimo_data?: string;
  bendras_ivertis?: number;
  avataras?: string;
  role_id?: number;
  privilegijos_id?: number;
}

type ComponentProps = VartotojasFormProps & FormProps<VartotojasFormModel, VartotojasFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isViewOnly } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit}>
        <FormField name="pseudonimas" label="vartotojas.nick" disabled={true} />
        <FormField name="slaptazodis" label="vartotojas.password" validate={required} disabled={isViewOnly} />
        <FormField name="vardas" label="vartotojas.name" validate={required} disabled={isViewOnly} />
        <FormField name="el_pastas" label="vartotojas.email" validate={required} disabled={isViewOnly} />
        <FormField name="lytis" label="vartotojas.sex" disabled={isViewOnly} />
        <FormField name="sukurimo_data" label="vartotojas.creation_date" validate={required} disabled={true} />
        <FormField name="bendras_ivertis" label="vartotojas.karma" validate={required} disabled={true} />
        <FormField name="avataras" label="vartotojas.avatar" disabled={isViewOnly} />
        <VartotojasSelectField
          disabled={isViewOnly}
          name="role_id" />


        <table>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.rasyti_temas" /></td>
            <td><br /><p>Rašyti temas</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.rasyti_straipsnius" /></td>
            <td><br /><p>Rašyti straipsnius</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.redaguoti_straipsnius" /></td>
            <td><br /><p>Redaguoti straipsnius</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.vertinti_vartotoja" /></td>
            <td><br /><p>Vertinti vartotoją</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.trinti_komentarus" /></td>
            <td><br /><p>Trinti komentarus</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.trinti_straipsnius" /></td>
            <td><br /><p>Trinti staipsnius</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.kurti_aprasymus" /></td>
            <td><br /><p>Kurti aprašymus</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.trinti_aprasymus" /></td>
            <td><br /><p>Trinti aprašymus</p></td>
          </tr>
          <tr>
            <td><FormField component={Checkbox} name="privilegijos.redaguoti_aprasymus" /></td>
            <td><br /><p>Redaguoti aprašymus</p></td>
          </tr>
        </table>


      </form >
    );
  }
}

export default compose(
  reduxForm<VartotojasFormModel, VartotojasFormProps, State>({ form: 'VartotojasForm' }),
)(TransportForm);
