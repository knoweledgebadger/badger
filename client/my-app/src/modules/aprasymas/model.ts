// fronte naudojamas modelis (TypeScript)

export interface Aprasymas {
  id?: number;
  pavadinimas?: string;
  sukurimo_data?: Date;
  redagavimo_data?: Date;
  aprasas?: string;
  vidutinis_ivertinimas?: number;
  redaguotas?: boolean;
  spec_kategorijos?: spec_kategorijos[];
  irenginio_kategorija_id?: number;
}

export interface spec_kategorijos {
  id?: number;
  kategorijos_pavadinimas?: string;
  aprasas?: string;
  paaiskinimas?: paaiskinimas[];
}

export interface paaiskinimas {
  id?: number;
  zodis?: string;
  tekstas?: string;
}

export interface Ivertinimas {
  id?: number;
  vartotojas?: string;
  zvaigzduciu_kiekis?: number;
  komentaras?: string;
  data?: Date;
}

export interface irenginio_kategorija {
  id?: number;
  pavadinimas?: string;
}
