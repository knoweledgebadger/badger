import { gql } from 'react-apollo';
// fronte naudojami queriai ir ką partraukti
const istrintiAprasyma = gql`
    mutation istrintiAprasyma($id: Int!) {
        istrintiAprasyma(id: $id) {
        id
      }
    }
`;

const kurtiAprasyma = gql`
    mutation kurtiAprasyma($aprasymas: AprasymoInput!) {
    kurtiAprasyma(aprasymas: $aprasymas) {
        id
        pavadinimas
        sukurimo_data
        redagavimo_data
        aprasas
        vidutinis_ivertinimas
        redaguotas   
        irenginio_kategorija_id 
        spec_kategorijos {
            id
            kategorijos_pavadinimas
            aprasas
            paaiskinimas{
                id
                zodis
                tekstas
            }
        }
      }
    }
`;

const aprasymoQuery = gql`
    query aprasymai {
        aprasymai {
            id
            pavadinimas
            sukurimo_data
            redagavimo_data
            aprasas
            vidutinis_ivertinimas
            redaguotas    
            irenginio_kategorija_id
            spec_kategorijos {
                id
                kategorijos_pavadinimas
                aprasas
                paaiskinimas{
                    id
                    zodis
                    tekstas
                }
            }  
        }
    }
`;
const getAprasymoData = gql`
query aprasymas($id: Int!) {
    aprasymas(id: $id) {
        id
        pavadinimas
        sukurimo_data
        redagavimo_data
        aprasas
        vidutinis_ivertinimas
        redaguotas    
        irenginio_kategorija_id
        spec_kategorijos {
            id
            kategorijos_pavadinimas
            aprasas
            paaiskinimas{
                id
                zodis
                tekstas
            }
        } 
  }
}
`;

const redaguotiAprasyma = gql`
mutation redaguotiAprasyma($aprasymas: AprasymoInput!) {
    redaguotiAprasyma(aprasymas: $aprasymas) {
    id
    pavadinimas
    sukurimo_data
    redagavimo_data
    aprasas
    vidutinis_ivertinimas
    redaguotas    
    irenginio_kategorija_id
    spec_kategorijos {
        id
        kategorijos_pavadinimas
        aprasas
        paaiskinimas{
            id
            zodis
            tekstas
        }
    }
  }
}
`;

const kategorijosQuery = gql`
query kategorijos {
    kategorijos {
        id
        pavadinimas
    }
}
`;

export {
    aprasymoQuery, redaguotiAprasyma, kurtiAprasyma, istrintiAprasyma, kategorijosQuery,
    getAprasymoData,
};
