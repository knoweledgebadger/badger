import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { paaiskinimas } from '../model';
import { WrappedFieldArrayProps } from 'redux-form';
import { IconButton } from 'material-ui';
import { default as ContentAdd } from 'material-ui/svg-icons/content/add';
import Paaiskinimas from './Paaiskinimas';

interface PaaiskinimasProps extends WrappedFieldArrayProps<paaiskinimas> {
}

const paaiskinimasList: React.StatelessComponent<PaaiskinimasProps> = (props) => {
  const { fields } = props;
  return (
    <div className="col-12">
      <div />
      <FormattedMessage id="paaiskinimas.paaisk" />
      <div />
      {fields && fields.map((name, index) => {
        return (
          <Paaiskinimas
            key={index}
            index={index}
            name={name}
          />
        );
      })}

      <IconButton
        tooltip={<FormattedMessage id="paaiskinimas.paaisk.add" />}
        tooltipPosition="top-left"
        onTouchTap={() => fields.push({})}
      >
        <ContentAdd />
      </IconButton>
    </div>
  );
};

export default paaiskinimasList;
