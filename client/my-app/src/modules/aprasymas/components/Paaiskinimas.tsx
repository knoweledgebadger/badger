import * as React from 'react';
import FormField from '../../../components/form/FormField';

interface SegmentProps {
  index: number;
  name: string;
}

const paaiskinimai: React.StatelessComponent<SegmentProps> = (props) => {
  const { index, name } = props;
  return (
    <div className="row">
      <div className="col-xl-5 col-lg-6 col-md-8 col-10">
        <div className="row">
          <FormField
            name={`${name}.zodis`}
            colClassName="col-5"
            label={index === 0 ? 'zodis.zodis' : undefined}
            floatingLabelFixed={true}
          />
          <FormField
            colClassName="col-8"
            name={`${name}.tekstas`}
            label={index === 0 ? 'zodis.tekstas' : undefined}
            floatingLabelFixed={true}
          />
        </div>
      </div>
    </div>
  );
};
export default paaiskinimai;
