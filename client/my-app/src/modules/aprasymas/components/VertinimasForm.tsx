import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import FormField from '../../../components/form/FormField';

export interface VertinimasFormProps {
  onSubmit: (event: any) => void;
  isViewOnly?: boolean;
}

export interface VertinimasFormModel {
  id?: number;
  vartotojas?: string;
  zvaigzduciu_kiekis?: number;
  komentaras?: string;
  data?: Date;
}

type ComponentProps = VertinimasFormProps & FormProps<VertinimasFormModel,
  VertinimasFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isViewOnly } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit}>
        <select name="zvaigzduciu_kiekis">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        <FormField name="komentaras" label="vertinimas.komentaras"
          disabled={isViewOnly}
          multiLine={true}
        />
      </form>
    );
  }
}

export default compose(
  reduxForm<VertinimasFormModel, VertinimasFormProps, State>({ form: 'VertinimasForm' }),
)(TransportForm);
