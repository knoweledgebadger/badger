import * as React from 'react';
import { FieldArray } from 'redux-form';
import SKategorijaList from './SKategorijaList';

export interface SegmentFieldsArrayProps {
  identificationNo?: string;
}

class SegmentFieldsArray extends React.Component<SegmentFieldsArrayProps, {}> {
  render() {
    const { identificationNo } = this.props;
    return (
      <FieldArray
        name="spec_kategorijos"
        component={SKategorijaList}
        identificationNo={identificationNo}
      />
    );
  }
}

export default SegmentFieldsArray;
