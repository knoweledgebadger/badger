import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { spec_kategorijos } from '../model';
import { WrappedFieldArrayProps } from 'redux-form';
import { IconButton } from 'material-ui';
import { default as ContentAdd } from 'material-ui/svg-icons/content/add';
import SKategorija from './SKategorija';

interface SKategorijaProps extends WrappedFieldArrayProps<spec_kategorijos> {
}

const specKategorijaList: React.StatelessComponent<SKategorijaProps> = (props) => {
  const { fields } = props;
  return (
    <div className="col-12">
      <div />
      <FormattedMessage id="kategorija.specKategorija" />
      <div />
      {fields && fields.map((name, index) => {
        return (
          <SKategorija
            key={index}
            index={index}
            name={name}
          />
        );
      })}

      <IconButton
        tooltip={<FormattedMessage id="kategorija.specKategorija.add" />}
        tooltipPosition="top-left"
        onTouchTap={() => fields.push({})}
      >
        <ContentAdd />
      </IconButton>
    </div>
  );
};

export default specKategorijaList;
