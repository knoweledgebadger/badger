import * as React from 'react';
import { graphql } from 'react-apollo';
import { Validator } from 'redux-form';
import { kategorijosQuery } from '../graphql';
import FormSelectField from '../../../components/material/FormSelectField';
import { irenginio_kategorija } from '../model';

export interface KategorijaSelectFieldProps {
  name: string;
  validate?: Validator | Validator[];
  value?: string;
  colClassName?: string;
  onFieldSelect?: (value: irenginio_kategorija) => void;
  hideLabel?: boolean;
  floatingLabelFixed?: boolean;
  valueField?: string;
  onlyProducts?: boolean;
  disabled?: boolean;
  style?: any;
}

interface GraphqlProps {
  kategorijos: irenginio_kategorija[];
}

type ComponentProps = KategorijaSelectFieldProps & GraphqlProps;

class KategorijaSelectField extends React.Component<ComponentProps, {}> {
  static defaultProps = {
    onlyProducts: false,
    colClassName: 'col-sm-12',
    label: 'irenginio_kategorija',
  };
  render() {
    const {
      value, kategorijos, name, validate, colClassName, disabled,
      onFieldSelect, hideLabel, floatingLabelFixed, valueField } = this.props;
    const label = hideLabel ? undefined : 'irenginio_kategorija.select';
    return (
      <FormSelectField
        value={value}
        options={kategorijos}
        onFieldSelect={onFieldSelect}
        disabled={disabled}
        style={{ cursor: 'default' }}
        textField="pavadinimas"
        valueField={valueField || 'id'}
        label={label}
        fullWidth={true}
        name={name}
        validate={validate}
        colClassName={colClassName}
        floatingLabelFixed={floatingLabelFixed}
      />
    );
  }
}

const withData = graphql<{ kategorijos: irenginio_kategorija[] }, KategorijaSelectFieldProps>(
  kategorijosQuery,
  {
    options: (props) => {
      return {
        variables: { onlyProducts: props.onlyProducts },
        fetchPolicy: 'network-only',
      };
    },
    props: ({ data: { kategorijos }, ownProps }: any) => {
      return {
        kategorijos,
      };
    },
  });

export default withData(KategorijaSelectField);
