import * as React from 'react';
import { compose } from 'react-apollo';
import { reduxForm, FormProps } from 'redux-form';
import { State } from '../../../rootReducer';
import { Aprasymas, spec_kategorijos } from '../model';
import SKategorijaArray from './SKategorijaArray';
import FormField from '../../../components/form/FormField';
import KategorijaSelectField from './KategorijaSelectField';

export interface AprasymasFormProps {
  onSubmit: (aprasymas: Aprasymas) => void;
  isViewOnly?: boolean;
}

export interface AprasymasFormModel {
  id?: number;
  pavadinimas?: string;
  sukurimo_data?: Date;
  redagavimo_data?: Date;
  aprasas?: string;
  vidutinis_ivertinimas?: number;
  redaguotas?: boolean;
  spec_kategorijos?: spec_kategorijos[];
  irenginio_kategorija_id?: number;
}

type ComponentProps = AprasymasFormProps & FormProps<AprasymasFormModel, AprasymasFormProps, State>;

class TransportForm extends React.Component<ComponentProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const { isViewOnly } = this.props;
    return (
      <form onSubmit={this.props.handleSubmit}>
        <KategorijaSelectField
          disabled={isViewOnly}
          name="irenginio_kategorija_id" />
        <FormField name="pavadinimas" label="aprasymas.pavadinimas" />
        <FormField name="aprasas" label="aprasymas.aprasas"
          disabled={isViewOnly}
          multiLine={true}
          rows={5}
        />
        <SKategorijaArray />
      </form>
    );
  }
}

export default compose(
  reduxForm<AprasymasFormModel, AprasymasFormProps, State>({ form: 'AprasymasForm' }),
)(TransportForm);
