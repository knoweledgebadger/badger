import * as React from 'react';
import { FieldArray } from 'redux-form';
import PaaiskinimasList from './PaaiskinimasList';

export interface SegmentFieldsArrayProps {
  identificationNo?: string;
  name?: string;
}

class SegmentFieldsArray extends React.Component<SegmentFieldsArrayProps, {}> {
  render() {
    const { identificationNo, name } = this.props;
    return (
      <FieldArray
        name={`${name}.paaiskinimas`}
        component={PaaiskinimasList}
        identificationNo={identificationNo}
      />
    );
  }
}

export default SegmentFieldsArray;
