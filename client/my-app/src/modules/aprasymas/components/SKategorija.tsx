import * as React from 'react';
import FormField from '../../../components/form/FormField';
import PaaiskinimasArray from './PaaiskinimasArray';

interface SegmentProps {
  index: number;
  name: string;
}

const specKategorijos: React.StatelessComponent<SegmentProps> = (props) => {
  const { index, name } = props;
  return (
    <div className="col-xl-5 col-lg-6 col-md-8 col-10">
      <div className="row">
        <FormField
          name={`${name}.kategorijos_pavadinimas`}
          colClassName="col-6"
          label={index === 0 ? 'kategorija.pav' : undefined}
          floatingLabelFixed={true}
        />
        <FormField
          colClassName="col-6"
          name={`${name}.aprasas`}
          label={index === 0 ? 'kategorija.ap' : undefined}
          floatingLabelFixed={true}
        />
        <PaaiskinimasArray name={name} />
      </div>
    </div>
  );
};
export default specKategorijos;
