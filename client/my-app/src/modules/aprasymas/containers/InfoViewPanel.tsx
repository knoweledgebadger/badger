import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Aprasymas, Ivertinimas } from '../model';
import { getAprasymoData } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicViewPanelTitle from '../../../components/form/BasicViewPanelTitle';
import VertinimasForm from '../components/VertinimasForm';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  redaguotiAprasyma: (aprasymas: Aprasymas) => Promise<Aprasymas>;
  vertintiAprasyma:
  (ivertinimas: Ivertinimas, aprasymoId: number,
  ) => Promise<Ivertinimas>; refetch: Function;
  aprasymas: Aprasymas;
}

export interface AprasymasEditPanelProps extends RouteComponentProps<{ id: number }> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & AprasymasEditPanelProps;

class InfoViewPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.redaguotiAprasyma = this.redaguotiAprasyma.bind(this);
    this.vertintiAprasyma = this.vertintiAprasyma.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  redaguotiAprasyma(aprasymas: Aprasymas) {
    const cleanObject = _.cloneDeep(aprasymas);
    delete (cleanObject as any).__typename;
    return this.props.redaguotiAprasyma(cleanObject).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('aprasymas.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('aprasymas.editFailed', 'danger') });
  }

  vertintiAprasyma(ivertinimas: Ivertinimas) {
    const cleanObject = _.cloneDeep(ivertinimas);
    delete (cleanObject as any).__typename;
    return this.props.vertintiAprasyma(cleanObject, this.props.aprasymas.id!).then(() => {
      this.props.refetch();
      this.props.displaySnackbar('vertintinimas.created', 'success');
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('vertintinimas.editFailed', 'danger') });
  }

  getPath() {
    return `/aprasymai`;
  }

  render() {
    const aprasymas = _.get<Aprasymas>(this.props.location.state, 'aprasymas');
    console.log(aprasymas);
    return (
      <BasicFormLayout
        title={
          <BasicViewPanelTitle
            usePlainMessage={true}
            onCancel={this.handleclose}
            title={aprasymas ? aprasymas.pavadinimas! : 'undefined'}
          />}
        body={!aprasymas ? <div /> :
          <div>
            {aprasymas ? aprasymas.aprasas : undefined}
            <div className="box-divider" style={{ marginTop: 15, marginBottom: 15 }} />
            Specifikacijos:
            {aprasymas.spec_kategorijos!.
              map((specifikacija: any, index: number) => {
                return (
                  <div>
                    {specifikacija.kategorijos_pavadinimas}: {specifikacija.aprasas}
                  </div>
                );
              })}
            <div className="box-divider" style={{ marginTop: 15, marginBottom: 15 }} />
            Vidutinis ivertinimas: {aprasymas ? aprasymas.vidutinis_ivertinimas : undefined}
            <VertinimasForm
              onSubmit={this.vertintiAprasyma}
              initialValues={_.get<Aprasymas>(this.props.location.state, 'aprasymas')}
            />
          </div>
        }
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: AprasymasEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withData = graphql<{ aprasymas: Aprasymas }, ComponentProps>(getAprasymoData, {
  options: (ownProps) => {
    return {
      fetchPolicy: 'network-only',
      variables: { id: ownProps.match.params.id },
    };
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, aprasymas } = data!;
    return {
      refetch,
      loading,
      aprasymas,
    };
  },
});

const aprasymasEditPanel: React.ComponentClass<AprasymasEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(InfoViewPanel));

export default aprasymasEditPanel;
