// paimportinti reikilingi dalykai
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import { aprasymoQuery, istrintiAprasyma } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Aprasymas } from '../model';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

// propsai routinimui ir žinutes atvaizdavimui
interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}
// propsai querinimui / mutationams
interface GraphqlProps {
  refetch: Function;
  istrintiAprasyma: (id: number) => Promise<Aprasymas>;
  aprasymai: Aprasymas[];
  loading: boolean;
}
// propsas padariantis kad komeponentas būtų route komponentas
export interface InfoPanelProps extends RouteComponentProps<{}> { }
// sujungiami i viena visi propsai
type ComponentProps = DispatchProps & InfoPanelProps & GraphqlProps;

//  temu peržiuros panel komponentas su sujungtais propsais
class InfoPanel extends React.Component<ComponentProps, {}> {
  // konstruktorius
  constructor() {
    super();
    // užbindimani viduje naudojami metodai, tam kad butu galima naudoti pačio komponento variablus
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }
  // kas vyks pasaudus temos delete mygtuka
  onDelete(aprasymas: Aprasymas) {
    // naudojamas iš graphql gautas metoda deleteTema
    this.props.istrintiAprasyma(aprasymas.id!).then(() => {
      // pasibaigus trinimui refetchinama
      this.props.refetch();
      // parodoma žinutė , kad tema sekmingai ištrinta
      this.props.displaySnackbar('aprasymas.removed', 'success');
      // jei nepavyksta ištrinti temos žinutė kad nepavyko
    }).catch(error => this.props.displaySnackbar('aprasymas.remove.failed', 'danger'));
  }
  // ka daryti jei paspaudžiamas edit mygtukas arba paspaudžiama ant temos elemento
  onEdit(aprasymas: Aprasymas) {
    // pushinamas edit panelis su  jam nurodyta tema, temos id
    this.props.push(`/settings/aprasymas/${aprasymas.id}/edit`, { aprasymas });
  }
  // render metodas kvieciamas kaskart pasikeitus propsas, pvz kai užkrauna temas
  render() {
    // tam kad visur nerašyti this.props.push arba thsi.props.temos tiesiog persimetama į konstatas
    const { push, aprasymai, loading } = this.props;
    // returninamas elementas

    return (
      <FilterFormLayout
        title={
          <ItemsListTitle
            onNewClick={() => push('/settings/aprasymas/new')}
            title="aprasymas.title"
            createTooltip="aprasymas.create.tooltip"
          />}
        filter={<div />}
        body={(
          <ItemsList
            items={aprasymai}
            onDelete={this.onDelete}
            onEdit={this.onEdit}
            onClick={this.onEdit}
            deleteMessage="aprasymas.delete.confirm"
            primaryTextProperty={['pavadinimas']}
            loading={loading}
          />
        )}
      />
    );
  }
}
// šiuo atveju nenaudojamas redux metodas naudingas sudetingesniuose psl
const mapStateToProps = (state: State, ownProps: InfoPanelProps) => {
  return {
  };
};
// sumapinami push ir žinutės atvaizdavimo metodai
const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};
// aprašomas mutationas su metodu, ir kas deletinama
const withMutations = graphql<{}, ComponentProps>(istrintiAprasyma, {
  props: (props) => {
    const { mutate } = props;
    return {
      istrintiAprasyma: (id: number) => mutate!({ variables: { id } }),
    };
  },
});
// aprašomas queris partraukiantis visas temas ir į ką viską sudėti , šiuo atveju
// return metodas į props numes fukciją refetch, boola loading, ir duomenis temos
// optionsai network-only reikalingi kad neužsicashintu listas ir pvz sukurus nauja tema 
// ji iskart atsivaizduotu liste
const withData = graphql<{ aprasymai: Aprasymas[] }, ComponentProps>(aprasymoQuery, {
  options: {
    fetchPolicy: 'network-only',
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, aprasymai } = data!;
    return {
      refetch,
      loading,
      aprasymai,
    };
  },
});

// viskas sujungiama į vieną komponentą
const infoPanel: React.ComponentClass<InfoPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(withMutations(InfoPanel)));
// exportinamas komponentas
export default infoPanel;
