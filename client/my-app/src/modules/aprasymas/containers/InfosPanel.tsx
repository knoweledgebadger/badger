// paimportinti reikilingi dalykai
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { connect, Dispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { graphql } from 'react-apollo';
import { aprasymoQuery } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import { Aprasymas } from '../model';
import FilterFormLayout from '../../../modules/main/components/FilterFormLayout';
import ItemsList from '../../../components/material/ItemsList';
import ItemsListTitle from '../../../components/material/ItemsListTitle';

// propsai routinimui ir žinutes atvaizdavimui
interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
}
// propsai querinimui / mutationams
interface GraphqlProps {
  refetch: Function;
  aprasymai: Aprasymas[];
  loading: boolean;
}
// propsas padariantis kad komeponentas būtų route komponentas
export interface InfosPanelProps extends RouteComponentProps<{}> { }
// sujungiami i viena visi propsai
type ComponentProps = DispatchProps & InfosPanelProps & GraphqlProps;

//  temu peržiuros panel komponentas su sujungtais propsais
class InfosPanel extends React.Component<ComponentProps, {}> {
  // konstruktorius
  constructor() {
    super();
    // užbindimani viduje naudojami metodai, tam kad butu galima naudoti pačio komponento variablus
    this.onView = this.onView.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }
  // kas vyks pasaudus aprasymai delete mygtuka

  onView(aprasymas: Aprasymas) {
    // pushinamas edit panelis su  jam nurodyta aprasymas, aprasymai id
    this.props.push(`/aprasymai/${aprasymas.id}/view`, { aprasymas });
  }
  onSearch(paieska: any) {
    this.props.refetch({ paieska: paieska.tekstas, fetchPolicy: 'network-only' });
  }
  // render metodas kvieciamas kaskart pasikeitus propsas, pvz kai užkrauna aprasymass
  render() {
    const { push, aprasymai, loading } = this.props;
    // returninamas elementas
    return (
      <FilterFormLayout
        title={
          <div className="col-12">
            <ItemsListTitle
              hideCreateButton={true}
              onNewClick={() => push('settings/aprasymas/new')}
              title="aprasymas.title"
              createTooltip="aprasymas.create.tooltip"
            />
          </div>}
        filter={<div />}
        body={(
          <ItemsList
            onEdit={() => { }}
            hideAllButtons={true}
            deleteMessage="aprasymas.delete.confirm"
            primaryTextProperty={['pavadinimas']}
            items={aprasymai}
            onClick={this.onView}
            loading={loading}
          />
        )}
      />
    );
  }
}
// šiuo atveju nenaudojamas redux metodas naudingas sudetingesniuose psl
const mapStateToProps = (state: State, ownProps: InfosPanelProps) => {
  return {
  };
};
// sumapinami push ir žinutės atvaizdavimo metodai
const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
  };
};
// aprašomas mutationas su metodu, ir kas deletinama

// aprašomas queris partraukiantis visas aprasymass ir į ką viską sudėti , šiuo atveju
// return metodas į props numes fukciją refetch, boola loading, ir duomenis aprasymai
// optionsai network-only reikalingi kad neužsicashintu listas ir pvz sukurus nauja aprasymas
// ji iskart atsivaizduotu liste
const withData = graphql<{ aprasymai: Aprasymas[] }, ComponentProps>(aprasymoQuery, {
  options: {
    fetchPolicy: 'network-only',
    variables: { search: undefined },
  },
  props: (props) => {
    const { data } = props;
    const { refetch, loading, aprasymai } = data!;
    return {
      refetch,
      loading,
      aprasymai,
    };
  },
});

// viskas sujungiama į vieną komponentą
const infosPanel: React.ComponentClass<InfosPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withData(InfosPanel));
// exportinamas komponentas
export default infosPanel;
