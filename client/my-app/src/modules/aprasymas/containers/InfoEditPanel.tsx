import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Aprasymas } from '../model';
import { redaguotiAprasyma } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import AprasymasForm from '../components/AprasymasForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  redaguotiAprasyma: (aprasymas: Aprasymas) => Promise<Aprasymas>;
  aprasymas: Aprasymas;
}

export interface InfoEditPanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & InfoEditPanelProps;

class InfoEditPanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.redaguotiAprasyma = this.redaguotiAprasyma.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  redaguotiAprasyma(aprasymas: Aprasymas) {
    const cleanObject = _.cloneDeep(aprasymas);
    delete (cleanObject as any).__typename;
    if (cleanObject.spec_kategorijos)
      cleanObject.spec_kategorijos.map((tais) => {
        delete (tais as any).__typename;
        return tais;
      });

    // console.log(cleanObject);
    return this.props.redaguotiAprasyma(cleanObject).then(() => {
      this.props.displaySnackbar('aprasymas.edited', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('aprasymas.editFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/aprasymas`;
  }

  render() {
    const { aprasymas } = this.props;
    console.log('edit panel', aprasymas);

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('AprasymasForm')}
            onCancel={this.handleclose}
            title="aprasymas.edit.title"
          />}
        body={
          <AprasymasForm
            onSubmit={this.redaguotiAprasyma}
            initialValues={_.get<Aprasymas>(this.props.location.state, 'aprasymas')}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: InfoEditPanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(redaguotiAprasyma, {
  props: (props) => {
    const { mutate } = props;
    return {
      redaguotiAprasyma: (aprasymas: Aprasymas) => mutate!({
        variables: { aprasymas },
      }),
    };
  },
});

const infoEditPanel: React.ComponentClass<InfoEditPanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(InfoEditPanel));

export default infoEditPanel;
