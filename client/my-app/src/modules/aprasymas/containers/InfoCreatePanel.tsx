import * as React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { push } from 'react-router-redux';
import { graphql } from 'react-apollo';
import { RouteComponentProps } from 'react-router';
import { submit } from 'redux-form';
import { Aprasymas } from '../model';
import { kurtiAprasyma } from '../graphql';
import { State } from '../../../rootReducer';
import { displaySnackbar, DisplaySnackbar } from '../../main/snackbarReducer';
import AprasymasForm from '../components/AprasymasForm';
import BasicFormLayout from '../../main/components/BasicFormLayout';
import BasicFormTitle from '../../../components/form/BasicFormTitle';

interface StateProps {
}

interface DispatchProps {
  push: typeof push;
  displaySnackbar: DisplaySnackbar;
  submit: typeof submit;
}

interface GraphqlProps {
  kurtiAprasyma: (aprasymas: Aprasymas) => Promise<Aprasymas>;
}

export interface InfoCreatePanelProps extends RouteComponentProps<{}> {
}

type ComponentProps = StateProps & DispatchProps & GraphqlProps & InfoCreatePanelProps;

class InfoCreatePanel extends React.Component<ComponentProps, {}> {

  constructor(props) {
    super(props);
    this.kurtiAprasyma = this.kurtiAprasyma.bind(this);
    this.getPath = this.getPath.bind(this);
    this.handleclose = this.handleclose.bind(this);
  }
  handleclose() {
    this.props.push(this.getPath());
  }
  kurtiAprasyma(aprasymas: Aprasymas) {
    return this.props.kurtiAprasyma(aprasymas).then(() => {
      this.props.displaySnackbar('aprasymas.created', 'success');
      this.props.push(this.getPath());
      // tslint:disable-next-line
    }).catch(error => { console.log(error); this.props.displaySnackbar('aprasymas.createFailed', 'danger') });
  }
  getPath() {
    const prevPath = _.get(this.props.location.state, 'prevPath');
    return prevPath ? prevPath : `/settings/aprasymas`;
  }

  render() {

    return (
      <BasicFormLayout
        title={
          <BasicFormTitle
            onSave={() => this.props.submit('AprasymasForm')}
            onCancel={this.handleclose}
            title="aprasymas.create.title"
          />}
        body={
          <AprasymasForm
            onSubmit={this.kurtiAprasyma}
          />}
      />
    );
  }
}

const mapStateToProps = (state: State, ownProps: InfoCreatePanelProps) => {
  return {
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps => {
  return {
    push: bindActionCreators(push, dispatch),
    displaySnackbar: bindActionCreators(displaySnackbar, dispatch),
    submit: bindActionCreators(submit, dispatch),
  };
};

const withMutations = graphql<{}, ComponentProps>(kurtiAprasyma, {
  props: (props) => {
    const { mutate } = props;
    return {
      kurtiAprasyma: (aprasymas: Aprasymas) => mutate!({
        variables: { aprasymas },
      }),
    };
  },
});

const infoCreatePanel: React.ComponentClass<InfoCreatePanelProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withMutations(InfoCreatePanel));

export default infoCreatePanel;
