import * as React from 'react';
import * as _ from 'lodash';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { toggleCollapsedNav, ToggleCollapsedNav } from '../sideNavReducer';
import { State } from '../../../rootReducer';
import SideNav from '../components/SideNav';
import Auth from '../../../Auth';
import BadgerSideNav from '../components/BadgerSideNav';

interface StateProps {
  navCollapsed: boolean;
}

interface DispatchProps {
  toggleCollapsedNav: ToggleCollapsedNav;
}

export interface SideBarProps {
  onBrand: () => void;
  path: string;
  brand: JSX.Element;
}

class SideBarComponent extends React.Component<StateProps & DispatchProps & SideBarProps, {}> {
  constructor(props) {
    super(props);
    this.getSideNavContent = this.getSideNavContent.bind(this);
  }

  componentDidUpdate() {
    const $body = $('#body');
    $body.removeClass('sidebar-mobile-open');
  }

  onToggleCollapsedNav() {
    const val = !this.props.navCollapsed;
    this.props.toggleCollapsedNav(val);
  }

  getSideNavContent() {
    const namespace = Auth.getJWT().namespace;
    const { path } = this.props;
    let sideNavContent;
    switch (_.lowerCase(namespace)) {
      default:
        sideNavContent = <BadgerSideNav path={path} />;
        break;
    }
    return sideNavContent;
  }

  render() {
    const { navCollapsed, onBrand, brand } = this.props;
    return (
      <SideNav
        about="about"
        brand={brand}
        onBrand={onBrand}
        navCollapsed={navCollapsed}
        toggleCollapsedNav={this.props.toggleCollapsedNav}
      >
        {this.getSideNavContent()}
      </SideNav>
    );
  }
}

const mapStateToProps = (state: State, ownProps: SideBarProps): StateProps => {
  return {
    navCollapsed: state.sideNavReducer.navCollapsed,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
  bindActionCreators({ toggleCollapsedNav }, dispatch);

const sideBar: React.ComponentClass<SideBarProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SideBarComponent);

export default sideBar;
