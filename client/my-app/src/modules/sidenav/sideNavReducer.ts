import { BasicAction } from '../../rootReducer';

// Actions
export const TOGGLE_COLLAPSED_NAV = 'TOGGLE_COLLAPSED_NAV';

// Reducer
interface SideNavAction extends BasicAction {
  type: string;
  isNavCollapsed?: boolean;
}

export interface SideNavReducerState {
  navCollapsed: boolean;
}

const initialState: SideNavReducerState = {
  navCollapsed: false,
};

export const sideNavReducer =
  (state: SideNavReducerState = initialState, action: SideNavAction): SideNavReducerState => {
    switch (action.type) {
      case TOGGLE_COLLAPSED_NAV:
        return {
          ...state,
          navCollapsed: action.isNavCollapsed!,
        };
      default:
        return state;
    }
  };

// Action Creators
export interface ToggleCollapsedNav {
  (isNavCollapsed: boolean): (dispatch: Function) => void;
}

export const toggleCollapsedNav: ToggleCollapsedNav = (isNavCollapsed) => {
  return (dispatch: Function) => {
    dispatch({ isNavCollapsed, type: TOGGLE_COLLAPSED_NAV });
  };
};
