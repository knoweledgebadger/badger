import * as React from 'react';
import SideNavItem from './SideNavItem';
import SideNavContent from './SideNavContent';
import Auth from '../../../Auth';

export interface BadgerSideNavProps {
  path: string;
}

const badgerSideNav: React.StatelessComponent<BadgerSideNavProps> = (props) => {
  const { path } = props;
  const isAdmin = Auth.isAdmin();
  return (
    <SideNavContent path={path}>
      <SideNavItem link="/forum" label="forum" />
      <SideNavItem link="/aprasymai" label="aprasymas" />
      {isAdmin && <li className="nav-divider" />}
      {isAdmin &&
        <SideNavItem label="administration" link="/settings" icon="settings">
          <SideNavItem link="/settings/users" label="users" />
          <SideNavItem link="/settings/tema" label="tema" />
          <SideNavItem link="/settings/aprasymas" label="aprasymas" />
          <SideNavItem link="/settings/vartotojas" label="users" />
        </SideNavItem>
      }
    </SideNavContent>
  );
};

export default badgerSideNav;
