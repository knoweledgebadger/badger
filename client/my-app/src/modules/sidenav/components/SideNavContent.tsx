import * as React from 'react';
import * as jquery from 'jquery';
window['$'] = window['jQuery'] = jquery; // tslint:disable-line
import 'jquery-slimscroll';

export interface SideNavContentProps {
  path: string;
}

class SideNavContent extends React.Component<SideNavContentProps, {}> {
  private nav: HTMLUListElement;

  componentDidMount() {
    const nav = this.nav;
    const $nav = $(nav);

    // scroll
    $nav.slimScroll({
      height: '100%',
    });

    // Append icon to submenu and append to child `div`
    // $nav.find('.prepend-icon').children('div')
    //   .prepend('<i class="material-icons">keyboard_arrow_right</i>');

    // AccordionNav
    const slideTime = 250;
    const $lists = $nav.find('ul').parent('li');
    $lists.append('<i class="material-icons icon-has-ul">arrow_drop_down</i>');
    const $As = $lists.children('a');

    // Disable A link that has ul
    $As.on('click', event => event.preventDefault());

    // Accordion nav
    $nav.on('click', (e) => {

      const target = e.target;
      // closest, insead of parent, so it still works when click on i icons
      const $parentLi = $(target).closest('li');
      // return if doesn't click on li
      if (!$parentLi.length) { return; }
      const $subUl = $parentLi.children('ul');

      // let depth = $subUl.parents().length; // but some li has no sub ul, so...
      const depth = $parentLi.parents().length + 1;

      // filter out all elements (except target) at current depth or greater
      const allAtDepth = $nav.find('ul').filter(function (this: {}) {
        if ($(this).parents().length >= depth && this !== $subUl.get(0)) {
          return true;
        }
        return false;
      });
      allAtDepth.slideUp(slideTime).closest('li').removeClass('open');

      // Toggle target
      if ($parentLi.has('ul').length) {
        $parentLi.toggleClass('open');
      }
      $subUl.stop().slideToggle(slideTime);

    });
    this.highlightActive(this.props.path);
  }

  componentDidUpdate() {
    this.highlightActive(this.props.path);
  }

  highlightActive(pathname: string) {
    const path = `${pathname}`;
    const nav = this.nav;
    const $nav = $(nav);
    const $links = $nav.find('a');
    $links.each((i, link) => {
      const $link = $(link);
      const $li = $link.parent('li');
      const href = $link.attr('href');

      if ($li.hasClass('active')) {
        $li.removeClass('active');
      }
      if (path.indexOf(href!) === 0) {
        $li.addClass('active');
      }
    });
  }

  render() {

    return (
      <ul className="nav" ref={(c: HTMLUListElement) => { this.nav = c; }}>
        {this.props.children}
      </ul>
    );
  }
}

export default SideNavContent;
