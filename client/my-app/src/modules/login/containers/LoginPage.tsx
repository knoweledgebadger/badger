import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RouteComponentProps } from 'react-router';
import { push } from 'react-router-redux';
import { login, Login } from '../sessionReducer';
import LoginLayout from '../components/LoginLayout';
import LoginForm from '../components/LoginForm';
import { State } from '../../../rootReducer';
import Auth from '../../../Auth';

interface StateProps {
  isError: boolean;
  isFetching: boolean;
  message: string;
  loggedOut: boolean;
  expired: boolean;
}

interface DispatchProps {
  login: Login;
  push: typeof push;
}

export interface LoginPageProps extends RouteComponentProps<{}> {
}

class LoginPageComponent extends React.Component<StateProps & DispatchProps & LoginPageProps, {}> {
  componentWillMount() {
    if (Auth.isUserAuthenticated()) {
      this.props.push('/');
    }
  }

  constructor(props: StateProps & DispatchProps & LoginPageProps) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
  }
  onRegisterClick = () => {
    this.props.push('register');
  }
  onSubmit(values: { email: string, password: string, remember: boolean }) {
    const username = values.email;
    const password = values.password;
    if (values.remember) {
      Auth.saveUsername(username);
      Auth.savePassword(password);
    } else {
      Auth.saveUsername('');
      Auth.savePassword('');
    }
    const locationState = this.props.location.state;
    const nextPath = locationState ? locationState.nextPathname : '/';
    this.props.login(username, password, nextPath);
  }

  render() {
    const { isFetching, isError, loggedOut, expired } = this.props;
    const initialValues = {
      email: Auth.getUsername(),
      password: Auth.getPassword(),
      remember: Auth.getPassword() !== '',
    };
    return (
      <LoginLayout>
        <LoginForm isFetching={isFetching} isError={isError}
          loggedOut={loggedOut} expired={expired}
          onRegister={this.onRegisterClick}
          initialValues={initialValues} onSubmit={this.onSubmit} />
      </LoginLayout>
    );
  }
}

const mapStateToProps = (state: State, ownProps: LoginPageProps): StateProps => {
  return {
    isError: state.sessionReducer.error,
    isFetching: state.sessionReducer.isFetching,
    message: state.sessionReducer.message,
    loggedOut: state.sessionReducer.loggedOut,
    expired: state.sessionReducer.expired,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<State>): DispatchProps =>
  bindActionCreators({ login, push }, dispatch);

const loginPage: React.ComponentClass<LoginPageProps> = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPageComponent);

export default loginPage;
