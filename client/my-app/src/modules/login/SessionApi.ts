import 'whatwg-fetch';

export interface SessionCreateResponse {
  id_token: string;
}

export class SessionApi {
  static login(username: string, password: string) {
    const options: RequestInit = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password }),
    };
    return fetch('/authenticate', options);
  }
}
