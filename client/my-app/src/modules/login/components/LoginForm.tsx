import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { reduxForm, Field, FormProps, FormComponentProps } from 'redux-form';
import { TextField, Checkbox } from 'redux-form-material-ui';
import { FlatButton } from 'material-ui';
import { required } from '../../../components/validation/ValidationUtils';
import { State } from '../../../rootReducer';

export interface LoginFormProps extends FormComponentProps {
  isError?: boolean;
  isFetching?: boolean;
  loggedOut?: boolean;
  expired?: boolean;
  onRegister: () => void;
}

export interface LoginFormModel {
  email: string;
  password: string;
  remember: boolean;

}

class LoginForm
  extends React.Component<LoginFormProps & FormProps<LoginFormModel, LoginFormProps, State>, {}> {
  constructor() {
    super();
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  onKeyUp(e: React.KeyboardEvent<{}>) {
    if (e.keyCode === 13) { // enter
      this.props.handleSubmit!(e);
    }
  }

  render() {
    return (
      <div className="body-inner">
        <div className="card bg-color-light">
          <div className="card-content theme-light">

            <section className="logo text-center">
              <h1><FormattedMessage id="appName" /></h1>
            </section>

            <form className="form-horizontal" onSubmit={this.props.handleSubmit}>

              <fieldset>
                <div className="form-group">
                  <Field
                    component={TextField}
                    floatingLabelText={<FormattedMessage id="user.username" />}
                    floatingLabelFixed={true}
                    name="email"
                    validate={required}
                    fullWidth={true}
                    onKeyUp={this.onKeyUp}
                  />
                </div>
                <div className="form-group">
                  <Field
                    component={TextField}
                    floatingLabelText={<FormattedMessage id="password" />}
                    floatingLabelFixed={true}
                    type="password"
                    name="password"
                    validate={required}
                    fullWidth={true}
                    onKeyUp={this.onKeyUp}
                  />
                </div>
              </fieldset>
              <Field
                component={Checkbox}
                name="remember"
                label={<FormattedMessage id="remember" />}
              />
            </form>

            {this.props.isError &&
              (
                <div className="callout callout-danger no-margin-bottom">
                  <p><FormattedMessage id="loginError" /></p>
                </div>
              )
            }

            {this.props.loggedOut &&
              (
                <div className="callout callout-success no-margin-bottom">
                  <p><FormattedMessage id="loggedOut" /></p>
                </div>
              )
            }

            {this.props.expired &&
              (
                <div className="callout callout-warning no-margin-bottom">
                  <p><FormattedMessage id="sessionExpired" /></p>
                </div>
              )
            }
          </div>
          <div className="row">
            <div className="card-action no-border text-right">
              <FlatButton
                label={this.props.isFetching ?
                  <FormattedMessage id="pleaseWait" /> : <FormattedMessage id="login" />}
                primary={true}
                onTouchTap={this.props.handleSubmit}
                disabled={this.props.isFetching} />
            </div>
            <div className="card-action no-border text-left">
              <FlatButton
                label={'Registracija'}
                primary={true}
                onTouchTap={this.props.onRegister}
                disabled={this.props.isFetching} />
            </div>
          </div>
        </div >

        <div className="additional-info" />
      </div >

    );
  }
}

export default reduxForm<LoginFormModel, LoginFormProps, State>({ form: 'LoginForm' })(LoginForm);
