import * as React from 'react';
import * as  QueueAnim from 'rc-queue-anim';

export interface LoginLayoutProps {
}

const loginLayout: React.StatelessComponent<LoginLayoutProps> = (props) => {
  return (
    <div className="page-login">
      <div className="main-body">
        <QueueAnim type="bottom" className="ui-animate">
          {React.Children.map(props.children, (child, index) => <div key={index}>{child}</div>)}
        </QueueAnim>
      </div>
    </div>
  );
};

export default loginLayout;
