import { push } from 'react-router-redux';
import { Action } from 'redux';
import { SessionApi, SessionCreateResponse } from './SessionApi';
import Auth from '../../Auth';

// Actions
const LOGIN_REQUEST = 'LOGIN_REQUEST';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILURE = 'LOGIN_FAILURE';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
const SESSION_EXPIRED = 'SESSION_EXPIRED';

// Reducer
interface SessionAction extends Action {
  message?: string;
  token?: string;
}

export interface SessionReducerState {
  token: string;
  isFetching: boolean;
  error: boolean;
  message: string;
  expired: boolean;
  loggedOut: boolean;
}

const initialState: SessionReducerState = {
  token: '',
  isFetching: false,
  error: false,
  expired: false,
  loggedOut: false,
  message: '',
};

export const sessionReducer =
  (state: SessionReducerState = initialState, action: SessionAction): SessionReducerState => {
    switch (action.type) {
      case LOGIN_REQUEST:
        return {
          ...state,
          isFetching: true,
        };
      case LOGIN_SUCCESS:
        return {
          ...state,
          isFetching: false,
          error: false,
          token: action.token!,
          loggedOut: false,
        };
      case LOGIN_FAILURE:
        return {
          ...state,
          isFetching: false,
          error: true,
          message: action.message!,
          loggedOut: false,
        };
      case LOGOUT_SUCCESS:
        return {
          ...state,
          loggedOut: true,
          token: '',
        };
      case SESSION_EXPIRED:
        return {
          ...state,
          expired: true,
          token: '',
          loggedOut: false,
        };
      default:
        return state;
    }
  };

// Action Creators
function requestLogin() {
  return { type: LOGIN_REQUEST };
}

function receiveLogin(token: string) {
  return {
    token,
    type: LOGIN_SUCCESS,
  };
}

function loginError(message: string) {
  return {
    message,
    type: LOGIN_FAILURE,
  };
}
export interface Login {
  (username: string, password: string, redirectUrl: string): (dispatch: Function) => void;
}

export const login: Login = (username, password, redirectUrl) => {
  return (dispatch: Function) => {
    dispatch(requestLogin());
    return SessionApi.login(username, password).then((response: Response) => {
      if (response.ok) {
        response.json().then((json: SessionCreateResponse) => {
          Auth.authenticateUser(json.id_token);
          dispatch(receiveLogin(json.id_token));
          dispatch(push(redirectUrl));
        });
      } else {
        dispatch(loginError(response.statusText));
      }
    }).catch((error: Error) => {
      throw error;
    });
  };
};

function receiveLogout() {
  return { type: LOGOUT_SUCCESS };
}

export interface Logout {
  (): (dispatch: Function) => void;
}

export const logout: Logout =
  () => {
    return (dispatch: Function) => {
      Auth.deauthenticateUser();
      dispatch(receiveLogout());
      dispatch(push({ pathname: '/login', state: { nextPathname: '/' } }));
    };
  };

export interface SessionExpired {
  (): (dispatch: Function) => void;
}

export const sessionExpired: SessionExpired = () => {
  return (dispatch: Function) => {
    dispatch(push('/login'));
    dispatch({ type: SESSION_EXPIRED });
  };
};
