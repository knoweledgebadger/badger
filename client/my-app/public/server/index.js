const express = require('express');
const proxy = require('http-proxy-middleware');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;



app.use(express.static(path.resolve(__dirname, '..', 'build')));

app.use('/api', proxy({
    target: process.env.POSTGRES_API,
    changeOrigin: true
}));
app.use('/authenticate', proxy({
    target: process.env.POSTGRES_API,
    changeOrigin: true
}));

app.get('/*', function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', 'build', 'index.html'));
});


app.listen(PORT, function () {});